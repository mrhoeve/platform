/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.cluster.ClusterService;
import org.pidome.server.services.cluster.HostInformation;
import org.pidome.server.system.config.SystemConfig;

/**
 * Cluster tests
 *
 * @author johns
 */
@DisplayName("Cluster integration tests")
public class ClusterApiControllerTest {

    /**
     * Api controller status.
     */
    private static ClusterApiController instance;

    /**
     * The service handler.
     */
    private static ServiceHandler handler;

    @BeforeAll
    public static void setUpClass() throws Exception {
        SystemConfig.initialize();
        handler = ServiceHandler.getInstance();
        handler.startServicesBlocking(
                PiDomeService.CLUSTER
        );
        instance = new ClusterApiController();
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
        handler.stopServicesBlocking();
        handler = null;
        instance = null;
    }

    /**
     * Test of gethostInfo method, of class ClusterApiController.
     */
    @Test
    @DisplayName("Test cluster host info")
    public void testGethostInfo() throws Exception {
        HostInformation expResult = handler.getService(ClusterService.class).get().getHostInformation();
        HostInformation result = instance.gethostInfo();
        assertEquals(expResult, result);
    }

}
