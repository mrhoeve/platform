/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import java.util.List;
import java.util.Map;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.peripherals.Peripheral;
import org.pidome.server.system.hardware.peripherals.PeripheralDriverCandidate;
import org.pidome.server.system.hardware.usb.USBDevices;

/**
 *
 * @author johns
 */
public class HardwareApiControllerTest {

    /**
     * Api controller status.
     */
    private static HardwareApiController instance;

    /**
     * The service handler.
     */
    private static ServiceHandler handler;

    /**
     * The peripheral we test with using windows style key.
     */
    private static final String PERIPHERAL_TEST_KEY_WINDOWS = "\\\\?\\USB#VID_2458&PID_0001#1#{5f5b9aac-4db5-5aba-89f0-a13651d848c8}";

    /**
     * The peripheral we test with using *nix style key.
     */
    private static final String PERIPHERAL_TEST_KEY_NIX = "/devices/pci0000:00/0000:00:06.0/usb2/2-2/2-2:1.0/tty/ttyACM0";

    @BeforeAll
    public static void setUpClass() throws Exception {
        SystemConfig.initialize();
        PlatformOs.setPlatform();
        handler = ServiceHandler.getInstance();
        handler.startServicesBlocking(
                PiDomeService.HARDWARESERVICE
        );
        instance = new HardwareApiController();
    }

    @AfterAll
    public static void tearDownClass() throws InterruptedException {
        handler.stopServicesBlocking();
        handler = null;
        instance = null;
    }

    /**
     * Test of getHardware method, of class HardwareApiController.
     */
    @Test
    public void testGetHardwareComponents() {
        Map<HardwareComponent.Interface, HardwareComponent> result = instance.getHardware();
        assertThat("libudev-dev expected on linux, and reachable registry on windows", result, hasKey(HardwareComponent.Interface.USB));
        List<Peripheral<? extends HardwareDriver>> peripherals = result.get(HardwareComponent.Interface.USB).getConnectedPeripherals();
        assertThat(peripherals.isEmpty(), is(false));
        /// LPT is not implemented, make sure no implementation is for whatever reason created.
        assertThat(result, not(hasKey(HardwareComponent.Interface.LPT)));
    }

    /**
     * Test of getHardwareComponent method, of class HardwareApiController.
     */
    @Test
    public void testGetHardwareComponent() throws Exception {
        HardwareComponent.Interface interfaceKey = HardwareComponent.Interface.USB;
        assertThat(instance.getHardwareComponent(interfaceKey), is(instanceOf(USBDevices.class)));
    }

    /**
     * Test of getPeripheral method, of class HardwareApiController.
     */
    @Test
    public void testGetPeripheral() throws Exception {
        HardwareComponent.Interface interfaceKey = HardwareComponent.Interface.USB;
        HardwareComponent component = instance.getHardwareComponent(interfaceKey);
        Peripheral peripheral = component.getPeripheralByKey(getHardwareDeviceTestKey())
                .orElseThrow(() -> new Exception("Peripheral missing"));
        assertThat(getHardwareDeviceTestKey(), is(equalTo(peripheral.getDeviceKey())));
    }

    /**
     * Test of getPeripheralDriverCandidates method, of class
     * HardwareApiController.
     */
    @Test
    public void testGetPeripheralDriverCandidates() throws Exception {
        HardwareComponent.Interface interfaceKey = HardwareComponent.Interface.USB;
        List<PeripheralDriverCandidate> candidates = instance.getPeripheralDriverCandidates(interfaceKey, getHardwareDeviceTestKey());
        assertThat(candidates.isEmpty(), is(false));
        assertThat("org.pidome.platform.hardware.driver.serial.PiDomeSerialDriver", is(equalTo(candidates.get(0).getDriver())));
    }

    /**
     * Test of getPeripheralSettings method, of class HardwareApiController.
     */
    @Disabled("Reading driver settings not yet finalized")
    public void testGetPeripheralSettings() throws Exception {
        List<PeripheralDriverCandidate> candidates
                = instance.getPeripheralDriverCandidates(HardwareComponent.Interface.USB, getHardwareDeviceTestKey());
        assertThat(candidates.isEmpty(), is(false));
        PeripheralDriverCandidate driver = candidates.get(0);
        assertThat(driver, is(notNullValue()));
    }

    private String getHardwareDeviceTestKey() {
        if (PlatformOs.isOs(PlatformOs.OS.WINDOWS)) {
            return PERIPHERAL_TEST_KEY_WINDOWS;
        } else {
            return PERIPHERAL_TEST_KEY_NIX;
        }
    }

}
