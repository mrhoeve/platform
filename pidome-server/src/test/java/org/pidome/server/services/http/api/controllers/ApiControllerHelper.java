/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.impl.headers.VertxHttpHeaders;
import io.vertx.ext.web.RoutingContext;
import java.util.Optional;
import org.mockito.Mockito;
import org.pidome.server.services.authentication.PidomeAuthTokenReference;

/**
 * Helper class for the api controllers.
 *
 * @author johns
 */
public class ApiControllerHelper {

    /**
     * Mocks an authentication token.
     *
     * @return The mocked token.
     */
    protected static PidomeAuthTokenReference mockAuthTokenReference() {
        PidomeAuthTokenReference reference = new PidomeAuthTokenReference();
        reference.setBrowserName("Java test");
        reference.setBrowserType("Local system");
        reference.setBrowserVersion("1.0");
        reference.setDeviceType("Computer");
        reference.setOsName(System.getProperty("os.name"));
        reference.setRemoteIp("127.0.0.1");
        return reference;
    }

    /**
     * Mocks a http request.
     *
     * @return The mocked http request.
     */
    protected static HttpServerRequest mockHttpServerRequest() {
        RoutingContext mockContext = Mockito.mock(RoutingContext.class);
        HttpServerRequest mockRequest = Mockito.mock(HttpServerRequest.class);
        VertxHttpHeaders headers = new VertxHttpHeaders();

        Mockito.when(mockContext.request()).thenReturn(mockRequest);
        Mockito.when(mockRequest.headers()).thenReturn(headers);

        return mockRequest;
    }

    /**
     * Mocks a response object.
     *
     * @return the mocked response object.
     */
    protected static HttpServerResponse mockHttpServerResponse() {
        HttpServerResponse mockResponseObject = Mockito.mock(HttpServerResponse.class);
        return mockResponseObject;
    }

    /**
     * Class which can be set final to be used for results in lambdas
     *
     * @param <T> Type to be expected;
     */
    protected static class ReferenceHolder<T> {

        /**
         * the object set.
         */
        private T object;

        /**
         * @return the object
         */
        public Optional<T> getObject() {
            return Optional.of(object);
        }

        /**
         * @param object the object to set
         */
        public void setObject(T object) {
            this.object = object;
        }

    }

}
