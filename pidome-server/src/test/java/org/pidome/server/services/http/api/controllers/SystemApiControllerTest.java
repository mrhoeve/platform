/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.pidome.server.entities.system.SystemLocale;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.broadcast.DiscoveryBroadcastMessage;
import org.pidome.server.services.network.broadcast.DiscoveryBroadcastService;
import org.pidome.server.services.network.broadcast.DiscoveryBroadcastServiceException;
import org.pidome.server.services.network.broadcast.ServicesConfig;
import org.pidome.server.system.config.SystemConfig;

/**
 *
 * @author johns
 */
@DisplayName("System API")
public class SystemApiControllerTest {

    /**
     * Api controller status.
     */
    private static SystemApiController instance;

    private static ServiceHandler handler;

    @BeforeAll
    public static void setUpClass() throws Exception {
        SystemConfig.initialize();
        instance = new SystemApiController();
        handler = ServiceHandler.getInstance();
        handler.startServicesBlocking(PiDomeService.NETWORK, PiDomeService.DISCOVERYSERVICE);
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
        instance = null;
        handler.stopServicesBlocking();
        handler = null;
    }

    /**
     * Test of setLocale method, of class SystemApiController.
     */
    @Test
    @DisplayName("Test locale set")
    public void testSetLocale() {
        SystemLocale locale = new SystemLocale();
        locale.setCountry("US");
        locale.setTag("en_US");
        Locale result = instance.setLocale(locale);
        assertEquals(Locale.US, result);
    }

    /**
     * Test of getLocale method, of class SystemApiController.
     */
    @Test
    @DisplayName("Test locale get")
    public void testGetLocale() {
        System.out.println("getLocale");
        SystemLocale result = instance.getLocale();
        assertEquals(Locale.getDefault().toString(), result.getTag());
    }

    /**
     * Test of getLocales method, of class SystemApiController.
     */
    @Test
    @DisplayName("Test get all possible locales")
    public void testGetLocales() {
        System.out.println("getLocales");
        List<SystemLocale> expResult = Stream.of(Locale.getAvailableLocales())
                .map(locale -> {
                    SystemLocale local = new SystemLocale();
                    local.setTag(locale.toString());
                    local.setCountry(locale.getDisplayName());
                    return local;
                }).collect(Collectors.toList());
        expResult.sort((c1, c2) -> c1.getCountry().compareTo(c2.getCountry()));
        List<SystemLocale> result = instance.getLocales();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNetworkInterfaces method, of class SystemApiController.
     */
    @Test
    @DisplayName("Test network interfaces availability")
    public void testGetNetworkInterfaces() {
        assertFalse(instance.getNetworkInterfaces().isEmpty());
    }

    /**
     * Test of serviceDiscovery method, of class SystemApiController.
     */
    @Test
    @DisplayName("Test available service providers endpoints")
    public void testServiceDiscovery() throws Exception {
        System.out.println("serviceDiscovery");
        DiscoveryBroadcastMessage message = ServiceHandler.getInstance().getService(DiscoveryBroadcastService.class)
                .orElseThrow(() -> new Exception("Broadcast service not available for testing"))
                .getBroadcastMessage();
        ServicesConfig result = instance.serviceDiscovery(message);

        assertThat(result.getApiLocation(), is(equalTo(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.rest", "/api"))));

        assertThat(result.getServerAddressAsString(), is(equalTo(ServiceHandler.getInstance().getService(NetworkService.class)
                .orElseThrow(() -> new DiscoveryBroadcastServiceException("No network service present"))
                .getInterfaceInUse()
                .getIpAddress().getHostAddress())));

        assertThat(result.getHttpPort(), is(equalTo(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.port", 0))));
    }

}
