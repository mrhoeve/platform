/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.users.person;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The current GEO Location.
 *
 * @author John Sirach
 */
public final class GeoLocation implements Serializable {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Current latitude.
     */
    private double latitude;

    /**
     * Current longitude.
     */
    private double longitude;

    /**
     * The current distance in meters.
     */
    private double distance;

    /**
     * The last update of the current location.
     */
    private LocalDateTime lastUpdate;

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param lat the latitude to set
     */
    public void setLatitude(final double lat) {
        this.latitude = lat;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longt the longitude to set
     */
    public void setLongitude(final double longt) {
        this.longitude = longt;
    }

    /**
     * @return the distance
     */
    public double getDistance() {
        return distance;
    }

    /**
     * @param dist the distance to set
     */
    public void setDistance(final double dist) {
        this.distance = dist;
    }

    /**
     * @return the lastUpdate
     */
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param update the lastUpdate to set
     */
    public void setLastUpdate(final LocalDateTime update) {
        this.lastUpdate = update;
    }
}
