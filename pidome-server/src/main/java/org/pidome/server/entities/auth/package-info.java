/**
 * Contains entities used for authentication and authorization.
 * <p>
 * These classes provide POJO entities for authorization and authentication.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.auth;
