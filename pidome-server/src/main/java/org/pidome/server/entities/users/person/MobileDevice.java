/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.users.person;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A mobile device bound to an user.
 *
 * @author John Sirach
 */
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(name = "mobile_id_unique", columnNames = {"uniqueId"})})
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MobileDevice implements Serializable {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * The database id of this device.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    /**
     * The description of this device when shown in an interface.
     */
    private String mobileDescription;

    /**
     * Unique mobile identifier to identify the device.
     */
    private String uniqueId;

    /**
     * If the device is connection throttled or not.
     */
    private boolean throttled;

    /**
     * If the device has geofencing enabled or not.
     */
    private boolean geofence;

    /**
     * When created.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    /**
     * Execute when creating.
     */
    @PrePersist
    protected void onCreate() {
        Date date = new Date();
        setCreated(date);
        setModified(date);
    }

    /**
     * The last login date and time.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    /**
     * Execute when updating.
     */
    @PreUpdate
    protected void onUpdate() {
        setModified(new Date());
    }

    /**
     * @return the mobileId
     */
    public int getId() {
        return id;
    }

    /**
     * @param mobileId the mobileId to set
     */
    public void setId(final int mobileId) {
        this.id = mobileId;
    }

    /**
     * @return the mobileDescription
     */
    public String getMobileDescription() {
        return mobileDescription;
    }

    /**
     * @param description the mobileDescription to set
     */
    public void setMobileDescription(final String description) {
        this.mobileDescription = description;
    }

    /**
     * @return the uniqueId
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * @param unique the uniqueId to set
     */
    public void setUniqueId(final String unique) {
        this.uniqueId = unique;
    }

    /**
     * @return the throttled
     */
    public boolean isThrottled() {
        return throttled;
    }

    /**
     * @param throttle the throttled to set
     */
    public void setThrottled(final boolean throttle) {
        this.throttled = throttle;
    }

    /**
     * @return the geofence
     */
    public boolean isGeofence() {
        return geofence;
    }

    /**
     * @param fence the geofence to set
     */
    public void setGeofence(final boolean fence) {
        this.geofence = fence;
    }

    /**
     * @return the created.
     */
    public Date getCreated() {
        return (Date) created.clone();
    }

    /**
     * @param created the created to set
     */
    public void setCreated(final Date created) {
        this.created = (Date) created.clone();
    }

    /**
     * @return the modified
     */
    public Date getModified() {
        return (Date) modified.clone();
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(final Date modified) {
        this.modified = (Date) modified.clone();
    }

}
