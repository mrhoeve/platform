/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.users;

/**
 * The user roles.
 *
 * @author John Sirach
 */
public enum Role {

    /**
     * Administrator role. An administrator is able to perform system level
     * tasks.
     */
    SERVICE(0, "SERVICE", "System services (for example used within clustering)"),
    /**
     * Administrator role. An administrator is able to perform system level
     * tasks.
     */
    ADMIN(1, "ADMIN", "Administrator"),
    /**
     * Power user. A power user is able to apply application level
     * modifications.
     */
    POWER(2, "POWER", "Power user"),
    /**
     * Regular user. A system user.
     */
    USER(3, "USER", "Default user."),
    /**
     * Used when unknown.
     */
    UNKNOWN(9999, "UNKNOWN", "Unknown role");

    /**
     * Readable representation of the role.
     */
    private final String roleName;

    /**
     * The short name for this enum. Must be EXACTLY as the enum name.
     */
    private final String shortName;

    /**
     * Numeric representation of the role level.
     */
    private final int roleLevel;

    /**
     * Private constructor for the enum.
     *
     * @param level The numeric role level.
     * @param shName the short name.
     * @param rName The role representation.
     */
    Role(final int level, final String shName, final String rName) {
        this.roleLevel = level;
        this.roleName = shName;
        this.shortName = rName;
    }

    /**
     * @return the roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Returns the int role level. This method is not adviced to use unless for
     * special cases, please use the named methods to check uquality on role
     * level.
     *
     * @return the roleLevel
     */
    public int getRoleLevel() {
        return roleLevel;
    }

    /**
     * Returns if the current role is higher then the given role.
     *
     * @param role The role to check against.
     * @return true when the current role is higher then the given role.
     */
    public final boolean isHigherLevelThan(final Role role) {
        if (role == null) {
            return false;
        }
        return this.getRoleLevel() > role.getRoleLevel();
    }

    /**
     * Returns if the current role is higher then the given role.
     *
     * @param role The role to check against.
     * @return true when the current role is higher then the given role.
     */
    public final boolean isHigherOrEqualLevelThan(final Role role) {
        if (role == null) {
            return false;
        }
        return isEqualLevelWith(role) || isHigherLevelThan(role);
    }

    /**
     * Returns if the current role is lower then the given role.
     *
     * @param role The role to check against.
     * @return true when the current role is lower then the given role.
     */
    public final boolean isLowerLevelThan(final Role role) {
        if (role == null) {
            return false;
        }
        return this.getRoleLevel() < role.getRoleLevel();
    }

    /**
     * Returns if the given role is on equal level with the given role.
     *
     * @param role The role to check against.
     * @return true when the roles are equal.
     */
    public final boolean isEqualLevelWith(final Role role) {
        if (role == null) {
            return false;
        }
        return this.getRoleLevel() == role.getRoleLevel();
    }

    /**
     * Returns the enum short name.
     *
     * @return The enum shortname.
     */
    public final String getShortName() {
        return this.shortName;
    }

    /**
     * Returns the role from the string representation.
     *
     * @param roleString The string representation of the Role.
     * @return The Role found, UNKNOWN role is returned when not found.
     */
    public static Role fromString(final String roleString) {
        if (roleString == null) {
            return Role.UNKNOWN;
        }
        for (Role role : Role.values()) {
            if (role.name().equals(roleString)) {
                return role;
            }
        }
        return Role.UNKNOWN;
    }

}
