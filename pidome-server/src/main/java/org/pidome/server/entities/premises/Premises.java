/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.pidome.server.entities.users.person.GeoLocation;

/**
 * The premises.
 *
 * @author John Sirach
 */
@Entity
@SuppressWarnings("CPD-START")
public class Premises implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The premises id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * The premises name.
     */
    private String name;

    /**
     * The properties on the premises.
     */
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Property> properties;

    /**
     * property sections on the premises.
     */
    @OneToMany(fetch = FetchType.EAGER)
    private Set<PropertySection> sections;

    /**
     * Icon used to represent the premises.
     */
    @OneToOne
    private PremisesIcon icon;

    /**
     * The location of the premises.
     */
    private GeoLocation location;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the properties
     */
    public Set<Property> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(final Set<Property> properties) {
        this.properties = properties;
    }

    /**
     * @return the sections
     */
    public Set<PropertySection> getSections() {
        return sections;
    }

    /**
     * @param sections the sections to set
     */
    public void setSections(final Set<PropertySection> sections) {
        this.sections = sections;
    }

    /**
     * @return the location
     */
    public GeoLocation getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(final GeoLocation location) {
        this.location = location;
    }

    /**
     * @return the icon
     */
    public PremisesIcon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(final PremisesIcon icon) {
        this.icon = icon;
    }

}
