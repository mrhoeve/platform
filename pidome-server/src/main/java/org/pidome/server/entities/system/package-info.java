/**
 * System level entities.
 * <p>
 * Providing the system entities for performing system actions.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.system;
