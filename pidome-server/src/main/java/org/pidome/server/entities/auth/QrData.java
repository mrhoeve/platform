/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.auth;

/**
 * The object holding the QR data.
 *
 * Object is required for completing the authorization process.
 *
 * @author John Sirach
 */
public final class QrData {

    /**
     * Constructor.
     */
    public QrData() {
    }

    /**
     * The user's id.
     */
    private long uid;
    /**
     * The ip of the remote device.
     */
    private String remoteIp;
    /**
     * The unique phone id.
     */
    private String phoneUniqueId;
    /**
     * The server ip.
     */
    private String serverIp;
    /**
     * The unique id.
     */
    private String uuId;

    /**
     * The possibility to set an on behalf id.
     */
    private Integer onBehalf;

    /**
     * @return the remoteIp
     */
    public String getRemoteIp() {
        return remoteIp;
    }

    /**
     * @param remote the remoteIp to set
     */
    public void setRemoteIp(final String remote) {
        this.remoteIp = remote;
    }

    /**
     * @return the phoneUniqueId
     */
    public String getPhoneUniqueId() {
        return phoneUniqueId;
    }

    /**
     * @param phoneUnique the phoneUniqueId to set
     */
    public void setPhoneUniqueId(final String phoneUnique) {
        this.phoneUniqueId = phoneUnique;
    }

    /**
     * @return the serverIp
     */
    public String getServerIp() {
        return serverIp;
    }

    /**
     * @param server the serverIp to set
     */
    public void setServerIp(final String server) {
        this.serverIp = server;
    }

    /**
     * @return the Uuid
     */
    public String getUuId() {
        return uuId;
    }

    /**
     * @param uuid the Uuid to set
     */
    public void setUuId(final String uuid) {
        this.uuId = uuid;
    }

    /**
     * @return the onBehalf
     */
    public Integer getOnBehalf() {
        return onBehalf;
    }

    /**
     * @param onBehalfOf the onBehalf to set
     */
    public void setOnBehalf(final Integer onBehalfOf) {
        this.onBehalf = onBehalfOf;
    }

    /**
     * @return the uid
     */
    public long getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(final long uid) {
        this.uid = uid;
    }

}
