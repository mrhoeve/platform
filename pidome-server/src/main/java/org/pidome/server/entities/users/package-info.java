/**
 * User based entities.
 * <p>
 * User entity classes and objects providing user information and roles.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.users;
