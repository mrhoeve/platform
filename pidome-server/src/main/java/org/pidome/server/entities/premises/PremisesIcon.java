/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.pidome.platform.presentation.icon.IconType;

/**
 * An icon to represent a premises object.
 *
 * @author John Sirach
 */
@Entity
public class PremisesIcon implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The icon id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;

    /**
     * The icon type.
     */
    @Enumerated(EnumType.STRING)
    private IconType iconType;

    /**
     * The icon.
     */
    private String icon;

    /**
     * Returns the id.
     *
     * @return The id.
     */
    public long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id The id to set.
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * The icon type.
     *
     * @return The icon type.
     */
    public IconType getIconType() {
        return iconType;
    }

    /**
     * Sets the icon type.
     *
     * @param iconType The type of icon.
     */
    public void setIconType(final IconType iconType) {
        this.iconType = iconType;
    }

    /**
     * The icon available in type.
     *
     * @return The icon.
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * Sets the icon available in icon type.
     *
     * @param icon the icon.
     */
    public void setIcon(final String icon) {
        this.icon = icon;
    }

}
