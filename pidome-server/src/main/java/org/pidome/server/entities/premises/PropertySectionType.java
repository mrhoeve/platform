/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

/**
 * The type of section identifying where it is located.
 *
 * @author John Sirach
 */
public enum PropertySectionType {

    /**
     * The section is located on the premises.
     */
    PREMISES,
    /**
     * The section is inside a property.
     */
    PROPERTY

}
