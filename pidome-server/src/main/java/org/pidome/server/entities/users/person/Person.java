/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.users.person;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Identifying a person.
 *
 * @author John Sirach
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Person implements Serializable {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * The id of the person.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * First name.
     */
    private String firstName;
    /**
     * Last name.
     */
    private String lastName;

    /**
     * The PiDome connected mobile devices for this person.
     */
    @OneToMany(fetch = FetchType.EAGER)
    private Collection<MobileDevice> mobiles;

    /**
     * Current location. null when disabled.
     */
    private GeoLocation geoLocation;

    /**
     * @return the mobiles
     */
    public Collection<MobileDevice> getMobiles() {
        return mobiles;
    }

    /**
     * @param devices the mobiles to set
     */
    public void setMobiles(final Collection<MobileDevice> devices) {
        this.mobiles = devices;
    }

    /**
     * Adds a mobile device to the person.
     *
     * @param mobile The mobile to add.
     */
    public void addMobile(final MobileDevice mobile) {
        this.mobiles.add(mobile);
    }

    /**
     * Removes a mobile from this person.
     *
     * @param mobile The mobile to be removed.
     */
    public void removeMobile(final MobileDevice mobile) {
        this.mobiles.remove(mobile);
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param personId the id to set
     */
    public void setId(final Integer personId) {
        this.id = personId;
    }

    /**
     * @return the geoLocation
     */
    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    /**
     * @param location the geoLocation to set
     */
    public void setGeoLocation(final GeoLocation location) {
        this.geoLocation = location;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param personFirstName the firstName to set
     */
    public void setFirstName(final String personFirstName) {
        this.firstName = personFirstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param personLastName the lastName to set
     */
    public void setLastName(final String personLastName) {
        this.lastName = personLastName;
    }
}
