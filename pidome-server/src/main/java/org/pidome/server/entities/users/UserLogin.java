/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.pidome.server.entities.users.person.Person;
import org.pidome.server.system.security.HashUtils;
import org.pidome.tools.utilities.NumberUtils;

/**
 * User model.
 *
 * (Database annotations)
 *
 * @author John
 */
@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(name = "user_unique", columnNames = {"userName"})})
@NamedQueries({
    @NamedQuery(name = "FROM_USERLOGIN", query = "from UserLogin WHERE userName = :userName")})
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserLogin implements Serializable {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * User logger.
     */
    private static final Logger LOG = LogManager.getLogger(UserLogin.class);

    /**
     * User id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * The user's username.
     */
    @NotNull
    private String username;

    /**
     * The user password.
     */
    @NotNull
    @JsonProperty(access = Access.WRITE_ONLY)
    private byte[] password;

    /**
     * The salt.
     */
    @NotNull
    @JsonIgnore
    private byte[] salt;

    /**
     * Boolean indicating if this is an initial login or not.
     */
    private boolean initialLogin;

    /**
     * When the user was created.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    /**
     * Execute when creating.
     */
    @PrePersist
    protected void onCreate() {
        Date date = new Date();
        setCreated(date);
        setModified(date);
        LOG.info("User create date [{}]", date);
    }

    /**
     * The last login date and time.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    /**
     * Execute when updating.
     */
    @PreUpdate
    protected void onUpdate() {
        setModified(new Date());
    }

    /**
     * The last login date and time.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;

    /**
     * The user role.
     */
    //@OneToOne(fetch = FetchType.EAGER)
    @Enumerated
    private Role role;

    /**
     * The person mapped to this user.
     */
    @OneToOne(fetch = FetchType.EAGER)
    private Person person;

    /**
     * @return the id.
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the userId to set.
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @return the username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * The user password.
     *
     * @param password The password for the user.
     */
    public void setPassword(final String password) {
        this.setSalt(HashUtils.generateRandomSalt());
        this.setPassword(HashUtils.hashPassword(password.toCharArray(), this.getSalt(), HashUtils.DEFAULT_HASH_ITERATIONS, HashUtils.DEFAULT_HASH_KEY_LENGTH));
    }

    /**
     * Check if the password equals.
     *
     * @param givenPassword The given password.
     * @return true if equals.
     */
    public boolean passwordEquals(final String givenPassword) {
        return Arrays.equals(HashUtils.hashPassword(givenPassword.toCharArray(), this.getSalt(), HashUtils.DEFAULT_HASH_ITERATIONS, HashUtils.DEFAULT_HASH_KEY_LENGTH), this.getPassword());
    }

    /**
     * @return the lastLogin.
     */
    public Date getLastLogin() {
        if (lastLogin == null) {
            lastLogin = new Date(NumberUtils.LONG_ONE);
        }
        return (Date) lastLogin.clone();
    }

    /**
     * @return the role.
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param userRole the role to set.
     */
    public void setRole(final Role userRole) {
        this.role = userRole;
    }

    /**
     * Returns the person bound to this user.
     *
     * @return The person bound.
     */
    public Person getPerson() {
        return this.person;
    }

    /**
     * Sets the person associated with this login.
     *
     * @param person The person to associate.
     */
    public void setPerson(final Person person) {
        this.person = person;
    }

    /**
     * @param lastLogin the lastLogin to set.
     */
    public void setLastLogin(final Date lastLogin) {
        this.lastLogin = (Date) lastLogin.clone();
    }

    /**
     * @return the initialLogin.
     */
    public boolean isInitialLogin() {
        return initialLogin;
    }

    /**
     * @param initialLogin the initialLogin to set.
     */
    public void setInitialLogin(final boolean initialLogin) {
        this.initialLogin = initialLogin;
    }

    /**
     * @return the modified.
     */
    public Date getModified() {
        return (Date) modified.clone();
    }

    /**
     * @return the password.
     */
    public byte[] getPassword() {
        byte[] returnPassword = new byte[this.password.length];
        System.arraycopy(this.password, 0, returnPassword, 0, this.password.length);
        return returnPassword;
    }

    /**
     * @param setPassword the password to set.
     */
    public void setPassword(final byte[] setPassword) {
        this.password = new byte[setPassword.length];
        System.arraycopy(setPassword, 0, this.password, 0, setPassword.length);
    }

    /**
     * @return the salt.
     */
    public byte[] getSalt() {
        byte[] returnSalt = new byte[this.salt.length];
        System.arraycopy(this.salt, 0, returnSalt, 0, this.salt.length);
        return returnSalt;
    }

    /**
     * @param setSalt the salt to set.
     */
    public void setSalt(final byte[] setSalt) {
        this.salt = new byte[setSalt.length];
        System.arraycopy(setSalt, 0, this.salt, 0, salt.length);
    }

    /**
     * @return the created.
     */
    public Date getCreated() {
        return (Date) created.clone();
    }

    /**
     * @param created the created to set.
     */
    public void setCreated(final Date created) {
        this.created = (Date) created.clone();
    }

    /**
     * @param modified the modified to set.
     */
    public void setModified(final Date modified) {
        this.modified = (Date) modified.clone();
    }

}
