/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Type;

/**
 * A region on a premises level.
 *
 * From rooms, attics, toilets to a region in a garden, driveway or parking lot
 * etc.
 *
 * @author John Sirach
 */
@Entity
@SuppressWarnings("CPD-START")
public class PropertySection implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The region id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * The region name.
     */
    private String name;

    /**
     * The type of property section.
     *
     * This is set automatically based on the object on which it is added to,
     * readonly.
     */
    @Enumerated
    @JsonProperty(access = Access.READ_ONLY)
    private PropertySectionType sectionType;

    /**
     * The region's X position on the map.
     */
    private int posX;

    /**
     * The region's Y position on the map.
     */
    private int posY;

    /**
     * The visual representation of the reqion.
     *
     * An SVG based object for visual representation.
     */
    @Type(type = "text")
    private String visualRepresentation;

    /**
     * Icon used to represent the property.
     */
    @OneToOne
    private PremisesIcon icon;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the posX
     */
    public int getPosX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosX(final int posX) {
        this.posX = posX;
    }

    /**
     * @return the posY
     */
    public int getPosY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(final int posY) {
        this.posY = posY;
    }

    /**
     * @return the visualRepresentation
     */
    public String getVisualRepresentation() {
        return visualRepresentation;
    }

    /**
     * @param visualRepresentation the visualRepresentation to set
     */
    public void setVisualRepresentation(final String visualRepresentation) {
        this.visualRepresentation = visualRepresentation;
    }

    /**
     * @return the sectionType
     */
    public PropertySectionType getSectionType() {
        return sectionType;
    }

    /**
     * @param sectionType the sectionType to set
     */
    public void setSectionType(final PropertySectionType sectionType) {
        this.sectionType = sectionType;
    }

    /**
     * @return the icon
     */
    public PremisesIcon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(final PremisesIcon icon) {
        this.icon = icon;
    }

}
