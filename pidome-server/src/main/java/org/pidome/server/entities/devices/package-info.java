/**
 * Provides device based classes and routines.
 * <p>
 * Package for supplying devices, interfaces and methods to be able to handle
 * devices within the pidome system.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.devices;
