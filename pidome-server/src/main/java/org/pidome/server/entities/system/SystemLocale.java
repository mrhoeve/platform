/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.system;

import java.util.Objects;

/**
 * A single system locale object.
 *
 * @author John Sirach
 */
public final class SystemLocale {

    /**
     * The language tag.
     *
     * This parameter is also used for setting a locale.
     */
    private String tag;

    /**
     * The visual country representation of the locale tag.
     *
     * This parameter is only for visual reference. Not used to set a Locale.
     */
    private String country;

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param langTag the tag to set
     */
    public void setTag(final String langTag) {
        this.tag = langTag;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param langCountry the country to set
     */
    public void setCountry(final String langCountry) {
        this.country = langCountry;
    }

    /**
     * Generate hash code.
     *
     * @return The object hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.tag);
        hash = 37 * hash + Objects.hashCode(this.country);
        return hash;
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SystemLocale other = (SystemLocale) obj;
        return Objects.equals(this.tag, other.tag);
    }

}
