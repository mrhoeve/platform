/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * A level in the premises.
 *
 * From basement to top floor/attic location.
 *
 * @author John Sirach
 */
@Entity
public class PropertyLevel implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The level id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * The level name.
     */
    private String name;

    /**
     * The level index.
     */
    private int index;

    /**
     * The level image url if present.
     */
    private String image;

    /**
     * The sections on the property level.
     */
    @OneToMany(fetch = FetchType.EAGER)
    private Set<PropertySection> sections;

    /**
     * Icon used to represent the property level.
     */
    @OneToOne
    private PremisesIcon icon;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(final int index) {
        this.index = index;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(final String image) {
        this.image = image;
    }

    /**
     * @param id the levelId to set
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @return the sections
     */
    public Set<PropertySection> getSections() {
        return sections;
    }

    /**
     * @param sections the regions to set
     */
    public void setSections(final Set<PropertySection> sections) {
        this.sections = sections;
    }

    /**
     * @return the icon
     */
    public PremisesIcon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(final PremisesIcon icon) {
        this.icon = icon;
    }

}
