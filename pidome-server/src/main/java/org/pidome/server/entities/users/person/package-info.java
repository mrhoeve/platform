/**
 * Person entities.
 * <p>
 * All entities supporting a person model in the system.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.users.person;
