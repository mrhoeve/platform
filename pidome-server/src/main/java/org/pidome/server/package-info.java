/**
 * Root package for server boot purposes.
 * <p>
 * Provides server boot and boot related classes.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server;
