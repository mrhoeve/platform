/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import io.vertx.ext.auth.User;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import org.pidome.server.entities.premises.Premises;
import org.pidome.server.entities.premises.PropertyLevel;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.premises.PremisesNotFoundException;
import org.pidome.server.services.premises.PremisesService;

/**
 * The controller for premises and properties.
 *
 * @author John Sirach
 */
@Path("premises")
@Produces("application/json")
@Consumes("application/json")
public class PremisesController extends ApiControllerResource {

    /**
     * Returns a list of premises.
     *
     * @param user The logged in user to select the allowed premises.
     * @return The premises for the given user.
     * @throws HttpStatusCodeException When an error occurs.
     */
    @GET
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "With the premises returned."),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When the service is not available.")
    })
    public List<Premises> getPremises(final @Context User user) throws HttpStatusCodeException {
        PremisesService service = getService();
        return service.getPremises();
    }

    /**
     * Returns the given premises.
     *
     * @param id The id of the premises requested to return.
     * @param user The user which should be allowed to request the premises.
     * @return The premises if the given user is allowed to fetch this.
     * @throws HttpStatusCodeException when an error occurs.
     */
    @GET
    @RolesAllowed("USER")
    @Path("{id}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "With the premises returned."),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When the service is not available."),
        @ResponseCode(code = ApiResponseCode.HTTP_403, condition = "When the requested premises is not allowed to be requested by the user.")
    })
    public Premises getPremises(final @PathParam("id") Long id, final @Context User user) throws HttpStatusCodeException {
        PremisesService service = getService();
        Premises premises = service.getPremises(id);
        RestTools.assertNotNull(premises);
        return premises;
    }

    /**
     * Returns the given premises.
     *
     * @param id The id of the premises requested to return.
     * @param user The user which should be allowed to request the premises.
     * @return The premises if the given user is allowed to fetch this.
     * @throws HttpStatusCodeException when an error occurs.
     */
    @GET
    @RolesAllowed("USER")
    @Path("property/level/{id}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "With the premises returned."),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When the service is not available."),
        @ResponseCode(code = ApiResponseCode.HTTP_403, condition = "When the requested premises is not allowed to be requested by the user.")
    })
    public PropertyLevel getPropertyLevel(final @PathParam("id") Long id, final @Context User user) throws HttpStatusCodeException {
        PremisesService service = getService();
        PropertyLevel propertyLevel;
        try {
            propertyLevel = service.getPropertyLevel(id);
        } catch (PremisesNotFoundException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Not found", ex);
        }
        return propertyLevel;
    }

    /**
     * Returns the premises service.
     *
     * @return The premises service
     * @throws HttpStatusCodeException When the service is not available with a
     * code 500.
     */
    private PremisesService getService() throws HttpStatusCodeException {
        if (ServiceHandler.getInstance().getService(PremisesService.class).isPresent()) {
            return ServiceHandler.getInstance().getService(PremisesService.class).get();
        } else {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Premises service not available");
        }
    }

}
