/**
 * Service providers.
 * <p>
 * Package supplying all the available services within the environment.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services;
