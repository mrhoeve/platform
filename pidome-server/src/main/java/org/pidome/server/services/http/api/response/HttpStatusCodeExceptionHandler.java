/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

import com.zandero.rest.exception.ExceptionHandler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import org.apache.logging.log4j.LogManager;

/**
 * Base class formatting the exposure of the exceptions thrown.
 *
 * @author John Sirach
 */
public class HttpStatusCodeExceptionHandler implements ExceptionHandler<HttpStatusCodeException> {

    /**
     * Writes the exception back to the response.
     *
     * @param t The HttpStatusCodeException thrown.
     * @param request The request done by a client.
     * @param response The response object used for providing the exception
     * response.
     * @throws Throwable When it is not possible to return to the requester or
     * write the exception to a log
     */
    @Override
    public void write(final HttpStatusCodeException t, final HttpServerRequest request, final HttpServerResponse response) throws Throwable {
        response.setStatusCode(t.getStatusCode());
        response.end(GenericExceptionHandler.exceptionFormatter(t, response.getStatusCode(), LogManager.getLogger().isDebugEnabled()));
    }

}
