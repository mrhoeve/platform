/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.hardware;

import io.vertx.core.Future;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.hardware.HardwareRoot;

/**
 * Service for server attached peripherals.
 *
 * @author John Sirach
 * @since 1.0
 */
public class HardwareService extends AbstractService {

    /**
     * The peripherals store where all connected peripherals are registered.
     */
    private HardwareRoot hardware;

    /**
     * Starts the peripherals service.
     *
     * @param startFuture The future used to detect success or failure.
     */
    @Override
    public final void start(final Future<Void> startFuture) {
        hardware = new HardwareRoot();
        Future<Void> future = Future.future();
        hardware.start(future.setHandler(result -> {
            startFuture.complete();
        }));
    }

    /**
     * Stops the peripheral service.
     *
     * @param stopFuture The future supplied to determine when the service is
     * stopped.
     */
    @Override
    public final void stop(final Future<Void> stopFuture) {
        if (hardware != null) {
            hardware.stop();
        }
        stopFuture.complete();
    }

    /**
     * Returns the hardware root entry.
     *
     * @return the hardware root.
     */
    public final HardwareRoot getRoot() {
        return hardware;
    }

}
