/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.VertXHandler;

/**
 * The event bus is responsible to send messages around in the system.
 *
 * The event bus is the heart of the system. Without the event bus the server
 * would not be able to communicate with all registered components in the
 * server.
 *
 * The event bus makes use of addresses and codecs. The event bus distinguishes
 * public and private events. All private events are shielded for use. As an
 * implementor there is no need to perform any special actions to register on
 * the event bus as this will be done automatically when an implementation is
 * registered on the server.
 *
 * @author John Sirach
 * @since 1.0
 * @todo finalize implementation of the event bus service, will be part of the
 * cluster epic.
 */
public final class EventBusService extends AbstractService {

    /**
     * The class logger.
     */
    private static final Logger LOG = LogManager.getLogger(EventBusService.class);

    /**
     * The eventbus used.
     */
    private EventBus eventBus;

    /**
     * Initializes the event bus.
     *
     * @param startFuture The future to determine the bus is available.
     */
    @Override
    public void start(final Future<Void> startFuture) {
        eventBus = VertXHandler.getInstance().getVertX().eventBus();
        startFuture.complete();
    }

    /**
     * Releases the eventbus locally.
     *
     * @param stopFuture The future used to determine if the bus is shut down.
     */
    @Override
    public void stop(final Future<Void> stopFuture) {
        stopFuture.complete();
    }

    /**
     * Registers a data consumer on the event bus.
     *
     * @param registerConsumer The consumer to consume messages.
     */
    public void registerConsumer(final EventConsumer<?> registerConsumer) {
        try {
            registerConsumer.createEventHandler(eventBus, something -> {
                LOG.info("Something to be printed");
            });
        } catch (ConsumerMissingException ex) {
            LOG.error("Could not register event consumer [{}], {}", registerConsumer, ex.getMessage(), ex);
        }
    }

    /**
     * Registers a data consumer on the event bus.
     *
     * @param unregisterConsumer The consumer to consume messages.
     */
    public void unRegisterConsumer(final EventConsumer<?> unregisterConsumer) {
        try {
            unregisterConsumer.unregister();
        } catch (ConsumerMissingException ex) {
            LOG.error("Could not unRegister event consumer [{}], {}", unregisterConsumer, ex.getMessage(), ex);
        }
    }

    /**
     * Registers a producer on the event bus.
     *
     * @param registerProducer The producer to produce messages.
     * @return True or false depending on the success of registering as a
     * producer
     */
    public boolean registerProducer(final EventProducer<?> registerProducer) {
        return false;
    }

    /**
     * Registers a producer on the event bus.
     *
     * @param consumer The consumer to consume messages.
     * @param address The address to listen to.
     * @return True or false depending on the success of registering as a
     * producer
     */
    public boolean unRegisterProducer(final MessageConsumer<String> consumer, final EventAddress address) {
        return false;
    }

}
