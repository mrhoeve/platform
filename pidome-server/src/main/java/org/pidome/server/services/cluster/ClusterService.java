/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.cluster;

import io.vertx.core.Future;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.config.SystemConfig;

/**
 * The service for cluster management.
 *
 * @author John Sirach
 */
public class ClusterService extends AbstractService {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(ClusterService.class);

    /**
     * Information about this cluster host.
     */
    private HostInformation hostInformation = new HostInformation();

    /**
     * Starts the cluster service.
     *
     * @param startFuture The future to identify server start.
     */
    @Override
    public void start(final Future<Void> startFuture) {
        HostInformation.ServerVersion version = new HostInformation.ServerVersion();
        version.setBuild(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_BUILD", 0));
        version.setDate(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_RELEASEDATE", "Unknown"));
        version.setMajor(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_MAJOR", 0));
        version.setMinor(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_MINOR", 0));
        version.setName(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_RELEASENAME", "Unknown"));
        try {
            version.setPatch(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_PATCH", 0));
            version.setSnapshot(false);
        } catch (NumberFormatException ex) {
            version.setPatch(0);
            version.setSnapshot(true);
        }
        version.setType(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_RELEASETYPE", "Local"));

        HostInformation.PlatformInfo info = new HostInformation.PlatformInfo();
        info.setArch(PlatformOs.getReportedArch());
        info.setOs(PlatformOs.getReportedOs());
        info.setPi(PlatformOs.isRaspberryPi());
        info.setJavaVersionInfo(System.getProperty("java.version"));
        info.setJavaVendorInfo(System.getProperty("java.vendor"));

        String localHost = "Unknown";
        try {
            localHost = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            LOG.warn("Could not get local hostname information [{}]", ex.getMessage());
        }

        hostInformation.setServerName(localHost);
        hostInformation.setPlatform(info);
        hostInformation.setVersion(version);

        startFuture.complete();
    }

    /**
     * Stops the cluster service.
     *
     * @param stopFuture The future to identify server stop.
     */
    @Override
    public void stop(final Future<Void> stopFuture) {
        stopFuture.complete();
    }

    /**
     * Returns the cluster host information.
     *
     * @return Host information.
     */
    public HostInformation getHostInformation() {
        return this.hostInformation;
    }

}
