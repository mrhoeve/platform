/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services;

/**
 * Exception used when a service fails.
 *
 * @author John
 */
public class ServiceException extends Exception {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>ServiceException</code> without detail
     * message.
     */
    public ServiceException() {
        //Default constructor.
    }

    /**
     * Constructs an instance of <code>ServiceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ServiceException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>CertGenException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     * @param ex The throwable cause.
     */
    public ServiceException(final String msg, final Throwable ex) {
        super(msg, ex);
    }

}
