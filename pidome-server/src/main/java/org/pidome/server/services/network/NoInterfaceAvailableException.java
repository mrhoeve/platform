/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network;

/**
 * Exception used when there is not network interface available.
 *
 * @author John Sirach
 */
public class NoInterfaceAvailableException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>NoInterfaceAvailebleException</code>
     * without detail message.
     */
    public NoInterfaceAvailableException() {
    }

    /**
     * Constructs an instance of <code>NoInterfaceAvailebleException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public NoInterfaceAvailableException(final String msg) {
        super(msg);
    }
}
