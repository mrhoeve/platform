/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network.broadcast;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.net.InetAddress;

/**
 * Class containing the configuration of the available services suitable for
 * consuming.
 *
 * @author John Sirach
 */
public class ServicesConfig {

    /**
     * The address of the server.
     */
    private InetAddress serverAddress;

    /**
     * The port of the HTTP services.
     */
    private int httpPort;

    /**
     * The address for the websocket services.
     */
    private int websocketAddress;

    /**
     * The location of the API.
     */
    private String apiLocation;

    /**
     * The address for the MQTT services.
     */
    @JsonIgnore
    private int mqttPort = 0;

    /**
     * The port used for debugging features.
     */
    @JsonIgnore
    private int debugPort = 0;

    /**
     * @return the serverAddress
     */
    @JsonIgnore
    public InetAddress getServerAddress() {
        return serverAddress;
    }

    /**
     * @return the ipAddress as String.
     */
    @JsonGetter("serverAddress")
    public String getServerAddressAsString() {
        return serverAddress.getHostAddress();
    }

    /**
     * @param serverAddress the serverAddress to set
     */
    public void setServerAddress(final InetAddress serverAddress) {
        this.serverAddress = serverAddress;
    }

    /**
     * @return the httpPort
     */
    public int getHttpPort() {
        return httpPort;
    }

    /**
     * @param httpPort the httpPort to set
     */
    public void setHttpPort(final int httpPort) {
        this.httpPort = httpPort;
    }

    /**
     * @return the websocketAddress
     */
    public int getWebsocketAddress() {
        return websocketAddress;
    }

    /**
     * @param websocketAddress the websocketAddress to set
     */
    public void setWebsocketAddress(final int websocketAddress) {
        this.websocketAddress = websocketAddress;
    }

    /**
     * @return the MQTTPort
     */
    public int getMQTTPort() {
        return mqttPort;
    }

    /**
     * @param mqttPort the MQTT Port to set
     */
    public void setMqttPort(final int mqttPort) {
        this.mqttPort = mqttPort;
    }

    /**
     * @return the debugPort
     */
    public int getDebugPort() {
        return debugPort;
    }

    /**
     * @param debugPort the debugPort to set
     */
    public void setDebugPort(final int debugPort) {
        this.debugPort = debugPort;
    }

    /**
     * @return the apiLocation
     */
    public String getApiLocation() {
        return apiLocation;
    }

    /**
     * @param apiLocation the apiLocation to set
     */
    public void setApiLocation(final String apiLocation) {
        this.apiLocation = apiLocation;
    }

}
