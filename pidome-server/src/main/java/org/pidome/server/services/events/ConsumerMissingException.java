/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

/**
 * If this exception occurs we are in trouble on the event bus.
 *
 * @author John Sirach
 */
public class ConsumerMissingException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>ConsumerMissingException</code> without
     * detail message.
     */
    public ConsumerMissingException() {
    }

    /**
     * Constructs an instance of <code>ConsumerMissingException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ConsumerMissingException(final String msg) {
        super(msg);
    }
}
