/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.auth;

import java.io.ByteArrayOutputStream;
import net.glxn.qrgen.javase.QRCode;
import org.pidome.server.entities.auth.QrData;

/**
 * QR code proxy for generating QR codes.
 *
 * @author John Sirach
 */
public final class QrProxy {

    /**
     * The QR data to be used.
     */
    private final QrData qrData;

    /**
     * The size to be used for the QR code.
     *
     * The size is square.
     */
    private static final int SIZE = 250;

    /**
     * Constructor which sets the QR data.
     *
     * @param data The QR data the proxy needs to handle.
     */
    public QrProxy(final QrData data) {
        this.qrData = data;
    }

    /**
     * Generates the image.
     *
     * @return Byte array with the QR image.
     */
    public ByteArrayOutputStream generate() {
        PiDomeQRSchema schema = new PiDomeQRSchema(this.qrData);
        return QRCode.from(schema.generateString()).withSize(SIZE, SIZE).stream();
    }

}
