/**
 * PiDome Cluster integration.
 * <p>
 * Provides services to have PiDome operate in a clustered environment.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.cluster;
