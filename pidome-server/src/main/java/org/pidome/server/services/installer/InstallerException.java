/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.installer;

/**
 * Exception used by the installer service when service specific actions fail.
 *
 * @author John
 */
public class InstallerException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>InstallerException</code> without detail
     * message.
     */
    public InstallerException() {
    }

    /**
     * Constructs an instance of <code>InstallerException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InstallerException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>InstallerException</code> with the
     * specified detail message and cause.
     *
     * @param msg the detail message.
     * @param cause the original exception.
     */
    public InstallerException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

}
