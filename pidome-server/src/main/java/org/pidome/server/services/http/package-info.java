/**
 * HTTP service package.
 * <p>
 * Providing all the HTTP based services.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.http;
