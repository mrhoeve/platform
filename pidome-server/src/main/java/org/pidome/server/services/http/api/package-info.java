/**
 * API resources.
 * <p>
 * Providing all the needed resources to be able to supply API's
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.http.api;
