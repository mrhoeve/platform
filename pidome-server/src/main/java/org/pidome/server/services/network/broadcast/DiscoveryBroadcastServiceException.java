/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network.broadcast;

/**
 * Exception when the broadcast service encounters a core exception.
 *
 * @author John Sirach
 */
public class DiscoveryBroadcastServiceException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor without message.
     */
    public DiscoveryBroadcastServiceException() {
    }

    /**
     * Constructor with message.
     *
     * @param message The message used in the exception.
     */
    public DiscoveryBroadcastServiceException(final String message) {
        super(message);
    }

    /**
     * Constructor used with original Throwable exception.
     *
     * @param cause The original Throwable exception.
     */
    public DiscoveryBroadcastServiceException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor with message and Throwable exception.
     *
     * @param message The message.
     * @param cause The Throwable exception.
     */
    public DiscoveryBroadcastServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
