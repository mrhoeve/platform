/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

/**
 * An event consumer which only receives events.
 *
 * @author John Sirach
 * @param <T> The type to be consumed.
 */
public class EventConsumer<T> extends EventBusMessageHandler<T> {

    /**
     * Constructor for the event consumer.
     *
     * @param address The address where the consumer should listen to.
     */
    public EventConsumer(final EventAddress address) {
        super(address);
    }

    /**
     * Handle the passed item.
     *
     * @param item The item to pass.
     */
    @Override
    public void handle(final T item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
