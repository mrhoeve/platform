/**
 * Authentication services.
 * <p>
 * Provides services and integration to apply authentication to users and or other systems.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.authentication;
