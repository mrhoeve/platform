/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

/**
 * Abstract class for implementing services.
 *
 * @author John
 */
public abstract class AbstractService extends AbstractVerticle {

    /**
     * Starts a service.
     *
     * @param startFuture Future for determining if a service has started or
     * not.
     */
    @Override
    public abstract void start(Future<Void> startFuture);

    /**
     * Stops a service.
     *
     * @param stopFuture Future for determining if a service has stopped or not.
     */
    @Override
    public abstract void stop(Future<Void> stopFuture);

    /**
     * Illeal start method as we only accept future based services.
     *
     * @throws Exception Always
     */
    @Override
    public final void start() throws Exception {
        throw new Exception("Illegal service start, implement your service with a future");
    }

    /**
     * Illeal stop method as we only accept future based services.
     *
     * @throws Exception Always
     */
    @Override
    public final void stop() throws Exception {
        throw new Exception("Illegal service stop, implement your service with a future");
    }

}
