/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

/**
 * Default exception for REST exceptions.
 *
 * @author John Sirach
 */
public class DefaultRestException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The response status in the exception.
     */
    private final int status;

    /**
     * Default REST exception constructor.
     *
     * @param message The message of the exception.
     * @param code The code involved in the thrown exception.
     */
    public DefaultRestException(final String message, final int code) {
        super(message);
        status = code;
    }

    /**
     * The error in the default exception.
     *
     * @return The error.
     */
    public String getError() {
        return getMessage();
    }

    /**
     * The response status in the exception.
     *
     * @return The status.
     */
    public int getStatus() {
        return status;
    }
}
