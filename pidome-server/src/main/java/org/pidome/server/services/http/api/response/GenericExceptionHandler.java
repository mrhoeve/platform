/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zandero.rest.exception.ExceptionHandler;
import com.zandero.rest.exception.ExecuteException;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import org.apache.logging.log4j.LogManager;
import org.pidome.tools.utilities.Serialization;

/**
 * Base class formatting the exposure of the exceptions thrown.
 *
 * @author John Sirach
 */
public class GenericExceptionHandler implements ExceptionHandler<Throwable> {

    /**
     * Writes the exception back to the response.
     *
     * @param t The exception thrown.
     * @param request The request done by a client.
     * @param response The response object used for providing the exception
     * response.
     * @throws Throwable When it is not possible to return to the requester or
     * write the exception to a log
     */
    @Override
    public void write(final Throwable t, final HttpServerRequest request, final HttpServerResponse response) throws Throwable {
        if (t instanceof ExecuteException) {
            ExecuteException execExcept = (ExecuteException) t;
            response.setStatusCode(execExcept.getStatusCode());
        } else {
            response.setStatusCode(ApiResponseCode.HTTP_500);
        }
        response.end(exceptionFormatter(t, response.getStatusCode(), LogManager.getLogger().isDebugEnabled()));
    }

    /**
     * Formats an exception to representable format.
     *
     * @param thr The thrown exception.
     * @param isDebug If debug is enabled or not.
     * @param statusCode The status code to report.
     * @return Representable exception string.
     */
    protected static String exceptionFormatter(final Throwable thr, final int statusCode, final boolean isDebug) {
        StringBuilder builder = new StringBuilder("{");
        builder.append("\"code\":").append(statusCode).append(",");
        builder.append("\"message\":\"").append(thr.getMessage().replaceAll("\"", "'")).append("\"");
        if (isDebug) {
            try {
                String trace = Serialization.getDefaultObjectMapper().writeValueAsString(thr);
                builder.append(",\"details\":").append(trace);
            } catch (JsonProcessingException ex) {
                LogManager.getLogger().error("Could not serialize stacktrace, so here it is: [{}]", thr);
            }
        }
        return builder.append("}").toString();
    }

}
