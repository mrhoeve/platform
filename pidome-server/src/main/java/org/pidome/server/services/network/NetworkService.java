/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network;

import io.vertx.core.Future;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.config.SystemConfig;

/**
 * The network service.
 *
 * @author John Sirach
 */
public class NetworkService extends AbstractService {

    /**
     * The logger.
     */
    private static final Logger LOG = LogManager.getLogger(NetworkService.class);

    /**
     * Constant for naming auto discovery.
     */
    private static final String AUTO_DISCOVERY = "network.autodiscovery";

    /**
     * List of available network interfaces we can interact with.
     */
    private final List<NetInterface> interfaces = new ArrayList<>();

    /**
     * Network availability listeners.
     */
    private static List<NetworkEventListener> listeners = new ArrayList<>();

    /**
     * Starts the network service.
     *
     * @param startFuture Determination if the service has started or not.
     */
    @Override
    public void start(final Future<Void> startFuture) {
        try {
            discover();
            startFuture.complete();
        } catch (NoInterfaceAvailableException | SocketException | UnknownHostException ex) {
            LOG.error("Unable to determine network interfaces [{}]", ex.getMessage(), ex);
            startFuture.fail(ex);
        }
    }

    /**
     * Stops the network service.
     *
     * @param stopFuture Determination if the service has stopped or not.
     */
    @Override
    public void stop(final Future<Void> stopFuture) {
        stopFuture.complete();
    }

    /**
     * Returns the list of interfaces.
     *
     * @return The interfaces list.
     */
    public List<NetInterface> getInterfaces() {
        return this.interfaces;
    }

    /**
     * Returns the current active interface from the available interfaces list.
     *
     * @return network interface to interact with.
     * @throws NoInterfaceAvailableException When there is no active network
     * interface.
     */
    public NetInterface getInterfaceInUse() throws NoInterfaceAvailableException {
        List<NetInterface> knownInterfaces = getInterfaces();
        for (NetInterface netInterface : knownInterfaces) {
            if (netInterface.isInUse()) {
                return netInterface;
            }
        }
        throw new NoInterfaceAvailableException("No active interface available in interface list");
    }

    /**
     * Looks for the networking devices and sets the addresses. When searching
     * for addresses it used the first found interface and only ip 4 as of this
     * moment. If you use multiple ip addresses you should set the addresses you
     * want to use for the services in the configuration file.
     *
     * @throws UnknownHostException When the host can not be found.
     * @throws SocketException When access to interfaces fails.
     * @throws NoInterfaceAvailableException When no interfaces are present
     * which are suitable for use.
     */
    private void discover() throws UnknownHostException, SocketException, NoInterfaceAvailableException {
        LOG.info("Autodiscovery of network interfaces");
        Enumeration<NetworkInterface> list;
        list = NetworkInterface.getNetworkInterfaces();
        while (list.hasMoreElements()) {
            NetworkInterface iface = list.nextElement();
            try {
                if (iface != null) {
                    NetInterface netFace = new NetInterface(iface);
                    if (netFace.getIpAddress() != null) {
                        interfaces.add(netFace);
                    }
                }
            } catch (NullInterfaceException ex) {
                LOG.trace("Interface [{}] refused for use", iface, ex);
            } catch (SocketException ex) {
                LOG.error("Unable to correct set network interface [{}]", iface, ex);
            }
        }
        if (SystemConfig.getSetting(SystemConfig.Type.SYSTEM, AUTO_DISCOVERY, false)) {
            NetInterface iface = this.interfaces.stream().filter((net) -> (!net.isLoopBack() && net.isUp()))
                    .findFirst()
                    .orElseThrow(() -> new NoInterfaceAvailableException("No interface found to activate"));
            iface.setInUse(true);
            LOG.info("Using network interface [{}]", iface);
        } else {
            InetAddress ip = InetAddress.getByName(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "network.ip", AUTO_DISCOVERY));
            InetAddress brdc = InetAddress.getByName(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "network.broadcastip", AUTO_DISCOVERY));
            this.interfaces.stream().filter((net) -> (!net.isLoopBack() && net.getIpAddress().equals(ip)
                    && net.getBroadcastAddress().equals(brdc)))
                    .findFirst().orElseThrow(() -> new NoInterfaceAvailableException("No interface found to activate"))
                    .setInUse(true);
        }
    }

    /**
     * Helper to set string to inetaddress.
     *
     * @param address The string version of an ip address.
     * @return The inet address version of the given ip address.
     * @throws UnknownHostException When the given ip address can not be
     * converted to an <code>InetAddress</code>
     */
    public static InetAddress toIp(final String address) throws UnknownHostException {
        return InetAddress.getByName(address);
    }

    /**
     * Adds a network event listener.
     *
     * @param l The listener for listening to network events.
     */
    public static synchronized void addEventListener(final NetworkEventListener l) {
        if (!listeners.contains(l)) {
            listeners.add(l);
        }
    }

    /**
     * Removes a network event listener.
     *
     * @param l The listener for removing.
     */
    public static synchronized void removeEventListener(final NetworkEventListener l) {
        listeners.remove(l);
    }

    /**
     * Fires a network event.
     *
     * Up/Down/Unavailable.
     *
     * @param eventType The event type to be fired.
     */
    synchronized void fireNetworkEvent(final String eventType) {
        try {
            LOG.debug("fireNetworkEvent: {}", eventType);
            //// Befor any network services are started create a new certificate.
            switch (eventType) {
                case NetworkEvent.AVAILABLE:
                    break;
                default:
                    LOG.warn("Unknown event type [{}]", eventType);
                    break;
            }
            NetworkEvent networkEvent = new NetworkEvent(getInterfaceInUse(), eventType);
            Iterator<NetworkEventListener> listenersIterator = listeners.iterator();
            while (listenersIterator.hasNext()) {
                listenersIterator.next().handleNetworkEvent(networkEvent);
            }
        } catch (NoInterfaceAvailableException ex) {
            LOG.error("No interface available to notify about", ex);
        }
    }

}
