/**
 * Event service.
 * <p>
 * Package providing everything for the system to be able to provide events.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.events;
