/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network.broadcast;

import io.vertx.core.Future;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.config.ConfigPropertiesException;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.tools.utilities.Serialization;

/**
 * Service responsible to enable discovery for clients.
 *
 * @author John Sirach
 */
public final class DiscoveryBroadcastService extends AbstractService {

    /**
     * The multicast socket.
     */
    private MulticastSocket broadcastSocket;
    /**
     * The package to broadcast.
     */
    private DatagramPacket broadcastMessage;
    /**
     * The message to broadcast.
     */
    private DiscoveryBroadcastMessage broadcastMessageObject = new DiscoveryBroadcastMessage();

    /**
     * Thread name.
     */
    private static final String THREAD_NAME = "SERVICE:BroadcastServer";
    /**
     * The logger.
     */
    private static final Logger LOG = LogManager.getLogger(DiscoveryBroadcastService.class);

    /**
     * The broadcast service thread.
     */
    private Thread broadcastServiceThread;

    /**
     * Object used to wait for the actual broadcast start.
     */
    private final Object blockWaitForStart = new Object();

    /**
     * Constructor.
     */
    public DiscoveryBroadcastService() {

    }

    /**
     * Starts the broadcast service.
     *
     * @param startFuture The future to complete when starting succeeded.
     */
    @Override
    public void start(final Future<Void> startFuture) {
        try {
            if (SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "network.enablebroadcast", false)) {
                broadcastSocket = new MulticastSocket();
                if (broadcastServiceThread == null || !broadcastServiceThread.isAlive()) {
                    createMessage();
                    createThread();
                    broadcastServiceThread.start();
                    try {
                        synchronized (blockWaitForStart) {
                            LOG.info("Waiting for first send");
                            blockWaitForStart.wait();
                        }
                    } catch (InterruptedException ex) {
                        LOG.error("Could not wait for start of the broadcast service broadcaster", ex);
                    }
                }
            } else {
                LOG.info("Broadcast service disabled, clients will not be able to do auto discovery");
            }
            startFuture.complete();
        } catch (DiscoveryBroadcastServiceException | NoInterfaceAvailableException | ConfigPropertiesException | IOException ex) {
            startFuture.fail(ex);
        }
    }

    /**
     * Stopping the broadcast service.
     *
     * @param stopFuture The future to complete after stopping.
     */
    @Override
    public void stop(final Future<Void> stopFuture) {
        stopBroadcast();
        stopFuture.complete();
    }

    /**
     * Stops the broadcast.
     */
    private void stopBroadcast() {
        if (broadcastServiceThread != null && broadcastServiceThread.isAlive()) {
            broadcastServiceThread.interrupt();
        }
    }

    /**
     * Creates the message to be broadcasted.
     *
     * @throws ConfigPropertiesException When the configuration is insufficient.
     * @throws IOException When serialization of the broadcast object fails.
     * @throws DiscoveryBroadcastServiceException When the broadcast service is
     * unable to contact the network service.
     * @throws NoInterfaceAvailableException When there is a network service but
     * no network interface is compatible/present
     */
    private void createMessage() throws ConfigPropertiesException, IOException, DiscoveryBroadcastServiceException, NoInterfaceAvailableException {
        final int broadcastPort = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "network.broadcastport", 10000);

        final InetAddress broadcastAddress = ServiceHandler.getInstance().getService(NetworkService.class)
                .orElseThrow(() -> new DiscoveryBroadcastServiceException("No network service present"))
                .getInterfaceInUse()
                .getBroadcastAddress();

        broadcastMessageObject.setS(ServiceHandler.getInstance().getService(NetworkService.class)
                .orElseThrow(() -> new DiscoveryBroadcastServiceException("No network service present"))
                .getInterfaceInUse()
                .getIpAddress().getHostAddress());
        broadcastMessageObject.setP(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.port", 0));
        broadcastMessageObject.setL(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.rest", "/api"));

        byte[] message = Serialization.getDefaultObjectMapper().writeValueAsBytes(broadcastMessageObject);
        broadcastMessage = new DatagramPacket(message, message.length, broadcastAddress, broadcastPort);
        LOG.info("Discovery service sends on [{}:{}] with [{}]", broadcastAddress.getHostAddress(), broadcastPort, Serialization.getDefaultObjectMapper().writeValueAsString(broadcastMessageObject));
    }

    /**
     * Returns the broadcast message.
     *
     * @return The message object.
     */
    public DiscoveryBroadcastMessage getBroadcastMessage() {
        return this.broadcastMessageObject;
    }

    /**
     * Creates the broadcast thread.
     *
     * @todo Change to thread executor, remove suppresswarning and remove thread
     * name.
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    private void createThread() {
        stopBroadcast();
        broadcastServiceThread = new Thread() {
            @Override
            public void run() {
                if (broadcastMessage != null) {
                    LOG.info("Send service started, sending every [{}] seconds", 7500 / 1000);
                    try {
                        synchronized (blockWaitForStart) {
                            blockWaitForStart.notify();
                        }
                        while (true) {
                            try {
                                broadcastSocket.send(broadcastMessage);
                            } catch (IOException e) {
                                broadcastSocket.close();
                            }
                            Thread.sleep(7500);
                        }
                    } catch (InterruptedException e) {
                        if (broadcastSocket.isBound()) {
                            broadcastSocket.close();
                        }
                        LOG.info("Broadcasting stopped.");
                    }
                } else {
                    LOG.info("Not running, there is no broadcast message (check config)");
                    synchronized (blockWaitForStart) {
                        blockWaitForStart.notify();
                    }
                }
            }
        };
        broadcastServiceThread.setName(THREAD_NAME);
    }

}
