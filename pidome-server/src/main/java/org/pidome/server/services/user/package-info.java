/**
 * User service.
 * <p>
 * Provides the ability to interact with user related actions as add/remove and edited user related information.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.user;
