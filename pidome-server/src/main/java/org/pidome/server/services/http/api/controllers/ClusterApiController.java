/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.cluster.ClusterService;
import org.pidome.server.services.cluster.HostInformation;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;

/**
 * Controller for cluster actions.
 *
 * @author John Sirach
 */
@Path("cluster")
@Produces("application/json")
@Consumes("application/json")
@RolesAllowed("POWER")
public class ClusterApiController extends ApiControllerResource {

    /**
     * Returns the information of this host on the cluster.
     *
     * @return Host information.
     * @throws HttpStatusCodeException On error.
     */
    @GET
    @Path("host")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "Returns host specific information."),
        @ResponseCode(code = ApiResponseCode.HTTP_401, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.HTTP_503, condition = "When the cluster service is not available")
    })
    public final HostInformation gethostInfo() throws HttpStatusCodeException {
        return ServiceHandler.getInstance().getService(ClusterService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Not available."))
                .getHostInformation();
    }

}
