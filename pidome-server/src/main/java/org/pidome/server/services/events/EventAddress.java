/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

/**
 * The event addresses are used by the event bus to determine the system paths
 * to send messages between system and service components.
 *
 * @author John Sirach
 * @since 1.0
 */
public enum EventAddress {

    /**
     * Address for the websockets.
     *
     * The websocket service reads from this address.
     */
    WEBSOCKET("org.pidome.server.service.http.websocket"),
    /**
     * Base address path for devices.
     */
    DEVICES("org.pidome.server.service.devices");

    /**
     * The address of the events.
     */
    private final String address;

    /**
     * Event address enum constructor.
     *
     * @param eventAddress The textual representation of the address.
     */
    EventAddress(final String eventAddress) {
        this.address = eventAddress;
    }

    /**
     * Returns the address of the events.
     *
     * @return The events address.
     */
    public final String getAddress() {
        return this.address;
    }

}
