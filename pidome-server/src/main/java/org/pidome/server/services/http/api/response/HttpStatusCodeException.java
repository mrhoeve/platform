/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

/**
 * Exception used to throw HTTP status code errors.
 *
 * @author John Sirach
 */
public class HttpStatusCodeException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The status code to return with this exception.
     */
    private final int statusCode;

    /**
     * Creates a new instance of <code>RestResourceNotFoundException</code>
     * without detail message.
     */
    public HttpStatusCodeException() {
        this(ApiResponseCode.HTTP_500, "Server error");
    }

    /**
     * Constructs an instance of <code>RestResourceNotFoundException</code> with
     * the specified detail message.
     *
     * @param httpStatusCode The http response code.
     * @param msg the detail message.
     */
    public HttpStatusCodeException(final int httpStatusCode, final String msg) {
        super(msg);
        this.statusCode = httpStatusCode;
    }

    /**
     * Constructs an instance of <code>RestResourceNotFoundException</code> with
     * the specified detail message.
     *
     * @param httpStatusCode The http response code.
     * @param msg the detail message.
     * @param cause The cause of the exception.
     */
    public HttpStatusCodeException(final int httpStatusCode, final String msg, final Throwable cause) {
        super(msg, cause);
        this.statusCode = httpStatusCode;
    }

    /**
     * Returns the status code given in th exception constructor.
     *
     * @return The status code.
     */
    public int getStatusCode() {
        return this.statusCode;
    }

}
