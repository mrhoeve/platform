/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

/**
 * The event direction determines if the event is flowing into the component
 * address or from the component.
 *
 * For example when an event is triggered by the HTTP API to PATCH a device
 * value it would be an IN direction targeting the device. The device would
 * notify the system of the update and sends a message with the OUT direction.
 *
 * @author John Sirach
 * @since 1.0
 */
public enum EventDirection {

    /**
     * A message flows into a component.
     */
    IN,
    /**
     * A message flows out a component.
     */
    OUT;

}
