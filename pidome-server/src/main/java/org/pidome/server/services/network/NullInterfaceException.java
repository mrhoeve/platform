/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network;

/**
 * Thrown when the selected interface does not provide the data suitable to be
 * used.
 *
 * @author John Sirach
 */
public class NullInterfaceException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>NullInterfaceException</code> without
     * detail message.
     */
    public NullInterfaceException() {
    }

    /**
     * Constructs an instance of <code>NullInterfaceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NullInterfaceException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>NullInterfaceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     * @param thr The throwable thrown set as cause.
     */
    public NullInterfaceException(final String msg, final Throwable thr) {
        super(msg, thr);
    }
}
