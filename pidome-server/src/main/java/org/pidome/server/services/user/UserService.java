/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.user;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import java.util.List;
import java.util.UUID;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.users.Role;
import org.pidome.server.entities.users.UserLogin;
import org.pidome.server.entities.users.person.Person;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.authentication.PidomeAuthUser;
import org.pidome.server.services.authentication.UserNotFoundException;
import org.pidome.server.services.http.api.auth.PassChangeObject;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.Database.AutoClosableEntityManager;
import org.pidome.server.system.database.DatabaseUtils;
import org.pidome.tools.utilities.NumberUtils;

/**
 * Service responsible for all user related actions with exception of
 * authentication and authorisation.
 *
 * @author John Sirach
 */
public class UserService extends AbstractService {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(UserService.class);

    /**
     * Returns a list of users.
     *
     * @return Users list
     * @throws Exception thrown when user retrieval fails.
     */
    public List<UserLogin> getUsers() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            TypedQuery<UserLogin> typedQuery = createGenericUserSelectTypedQuery(autoManager);
            typedQuery.setFirstResult(NumberUtils.INTEGER_ONE);
            return typedQuery.getResultList();

        }
    }

    /**
     * Returns a limited list of users.
     *
     * @param startIndex start index for the list.
     * @param amount The amount tot maximum retrieve.
     * @return Users list
     * @throws Exception thrown when user retrieval fails.
     */
    public List<UserLogin> getUsers(final int startIndex, final int amount) throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            TypedQuery<UserLogin> typedQuery = createGenericUserSelectTypedQuery(autoManager);
            typedQuery.setFirstResult(NumberUtils.INTEGER_ONE + 1);
            typedQuery.setMaxResults(amount);
            return typedQuery.getResultList();
        }
    }

    /**
     * Creates a generic typed query to fetch a users list.
     *
     * @param autoManager the auto closable manager.
     * @return The typed <code>User</code> query.
     */
    private static TypedQuery<UserLogin> createGenericUserSelectTypedQuery(final AutoClosableEntityManager autoManager) {
        CriteriaQuery<UserLogin> criteriaQuery = autoManager.getManager().getCriteriaBuilder().createQuery(UserLogin.class);
        Root<UserLogin> from = criteriaQuery.from(UserLogin.class);
        CriteriaQuery<UserLogin> select = criteriaQuery.select(from);
        return autoManager.getManager().createQuery(select);
    }

    /**
     * Returns the total list size of the users.
     *
     * @return The amount of users in total.
     * @throws Exception When the users list can not be retrieved.
     */
    public long getUserListSize() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), UserLogin.class) - 1;
        }
    }

    /**
     * Returns the user of the token.
     *
     * @param authUser The session user.
     * @return The user of the given token.
     * @throws UserNotFoundException When the given user is not found.
     */
    @SuppressWarnings("PMD.PreserveStackTrace")
    public UserLogin getUser(final PidomeAuthUser authUser) throws UserNotFoundException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(UserLogin.class, authUser.getUid());
        } catch (NoResultException ex) {
            LOG.error("Token passed, but no user found for id [{}]", authUser.getUid(), ex);
            throw new UserNotFoundException();
        }
    }

    /**
     * Updates a user password.
     *
     * @param passChange Object required to be able to change a password.
     * @param authUser The authenticated user.
     * @param resultHandler The handler to supply the result to.
     */
    public final void updatePassword(final PassChangeObject passChange, final PidomeAuthUser authUser, final Handler<AsyncResult<Boolean>> resultHandler) {
        if (isSystemUser(authUser)) {
            resultHandler.handle(Future.failedFuture("Not allowed to change system user"));
            return;
        }
        if (!passChange.check()) {
            resultHandler.handle(Future.failedFuture("New password and new password check are not equal"));
            return;
        }
        try {
            UserLogin user = getUser(authUser);
            if (user.passwordEquals(passChange.getOldPassword())) {
                if (user.passwordEquals(passChange.getRetypeNewPassword())) {
                    resultHandler.handle(Future.failedFuture("New password equals current password"));
                    return;
                }
                if (user.passwordEquals(passChange.getOldPassword())) {
                    user.setPassword(passChange.getRetypeNewPassword());
                    user.setInitialLogin(false);
                    try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
                        autoManager.getManager().getTransaction().begin();
                        autoManager.getManager().merge(user);
                        autoManager.getManager().getTransaction().commit();
                        resultHandler.handle(Future.succeededFuture(true));
                    } catch (Exception ex) {
                        LOG.error("Password update error", ex);
                        resultHandler.handle(Future.failedFuture(new Exception("Password change failed due to system error")));
                    }
                } else {
                    resultHandler.handle(Future.succeededFuture(true));
                }
            } else {
                resultHandler.handle(Future.failedFuture("Current password incorrect"));
            }
        } catch (UserNotFoundException ex) {
            resultHandler.handle(Future.failedFuture(ex));
        }
    }

    /**
     * Starts the service.
     *
     * @param startFuture The future to report start success or failure.
     */
    @Override
    public void start(final Future<Void> startFuture) {
        try {
            createDefaultUsers();
            startFuture.complete();
        } catch (Exception ex) {
            startFuture.fail(ex);
        }
    }

    /**
     * Starts the service.
     *
     * @param stopFuture The future to report stop success or failure.
     */
    @Override
    public void stop(final Future<Void> stopFuture) {
        stopFuture.complete();
    }

    /**
     * Creates default users.
     *
     * @throws Exception When the creation of default users fails.
     */
    private void createDefaultUsers() throws Exception {
        LOG.info("Checking for default users existence");
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            UserLogin serviceLogin = null;
            UserLogin login = null;
            Person person = null;
            UserLogin systemService = autoManager.getManager().find(UserLogin.class, NumberUtils.LONG_ONE);
            if (systemService == null) {
                LOG.warn("System service does not exist, creating");
                serviceLogin = new UserLogin();
                serviceLogin.setUsername("service");
                serviceLogin.setPassword(UUID.randomUUID().toString());
                serviceLogin.setInitialLogin(false);
                serviceLogin.setRole(Role.SERVICE);
            }
            UserLogin checkLogin = autoManager.getManager().find(UserLogin.class, NumberUtils.LONG_TWO);
            if (checkLogin == null) {
                LOG.warn("Primary PiDome admin user does not exist, creating");
                login = new UserLogin();
                login.setUsername("pidome");
                login.setPassword("pidome");
                login.setInitialLogin(true);
                login.setRole(Role.ADMIN);
                person = new Person();
                person.setFirstName("PiDome");
            }
            if (serviceLogin != null || login != null) {
                if (serviceLogin != null) {
                    autoManager.getManager().persist(serviceLogin);
                }
                if (login != null) {
                    if (person != null) {
                        try {
                            autoManager.getManager().persist(person);
                        } catch (Exception ex) {
                            LOG.error("Could not create pidome default user: [{}]", ex.getMessage());
                        }
                        login.setPerson(person);
                    }
                    autoManager.getManager().persist(login);
                }
            }
            autoManager.getManager().getTransaction().commit();
        } catch (Exception ex) {
            LOG.error("Problem in default users creation [{}]", ex.getMessage(), ex);
            throw ex;
        }
    }

    /**
     * Check if the current user is the system user.
     *
     * @param authUser check if the given user is the System user.
     * @return true when system user.
     */
    private boolean isSystemUser(final PidomeAuthUser authUser) {
        return authUser.getUid() == NumberUtils.LONG_ONE;
    }

}
