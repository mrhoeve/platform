/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.premises;

import io.vertx.core.Future;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.presentation.icon.IconType;
import org.pidome.server.entities.premises.Premises;
import org.pidome.server.entities.premises.PremisesIcon;
import org.pidome.server.entities.premises.Property;
import org.pidome.server.entities.premises.PropertyLevel;
import org.pidome.server.entities.premises.PropertySection;
import org.pidome.server.entities.premises.PropertySectionType;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.Database.AutoClosableEntityManager;
import org.pidome.server.system.database.DatabaseUtils;
import org.pidome.tools.utilities.NumberUtils;

/**
 * Premises service.
 *
 * @author John Sirach
 */
public final class PremisesService extends AbstractService {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(PremisesService.class);

    /**
     * Returns all premises levels.
     *
     * @return All levels known on the premises.
     */
    public List<Premises> getPremises() {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaQuery<Premises> criteriaQuery = autoManager.getManager().getCriteriaBuilder().createQuery(Premises.class);
            Root<Premises> from = criteriaQuery.from(Premises.class);
            CriteriaQuery<Premises> select = criteriaQuery.select(from);

            TypedQuery<Premises> typedQuery = autoManager.getManager().createQuery(select);
            return typedQuery.getResultList();
        }
    }

    /**
     * Returns a premises based on the given id.
     *
     * @param id The id of the premises to etch.
     * @return the selected premises.
     */
    public Premises getPremises(final long id) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(Premises.class, id);
        }
    }

    /**
     * Returns all premises levels.
     *
     * @return All levels known on the premises.
     */
    public List<PropertyLevel> getPropertyLevels() {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaQuery<PropertyLevel> criteriaQuery = autoManager.getManager().getCriteriaBuilder().createQuery(PropertyLevel.class);
            Root<PropertyLevel> from = criteriaQuery.from(PropertyLevel.class);
            CriteriaQuery<PropertyLevel> select = criteriaQuery.select(from);

            TypedQuery<PropertyLevel> typedQuery = autoManager.getManager().createQuery(select);
            typedQuery.setFirstResult(1);
            return typedQuery.getResultList();
        }
    }

    /**
     * Returns a single on premises level.
     *
     * @param id The id of the premises level to return.
     * @return A premises level.
     * @throws PremisesNotFoundException When no premises level is found with
     * the given id.
     */
    @SuppressWarnings("PMD.PreserveStackTrace")
    public PropertyLevel getPropertyLevel(final long id) throws PremisesNotFoundException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(PropertyLevel.class, id);
        } catch (NoResultException ex) {
            LOG.error("No premises level found with given id [{}]", id, ex);
            throw new PremisesNotFoundException();
        }
    }

    /**
     * Returns a list of premises regions on a given premises level.
     *
     * @param premisesLevelId The id of the premises level.
     * @return a list of premises regions.
     */
    public List<PropertySection> getPremisesSections(final long premisesLevelId) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PropertySection> criteriaQuery = builder.createQuery(PropertySection.class);
            Root<PropertyLevel> fromRoot = criteriaQuery.from(PropertyLevel.class);
            Root<PropertySection> joinRoot = criteriaQuery.from(PropertySection.class);

            Join<PropertyLevel, PropertySection> joinRegions = fromRoot.join("regions", JoinType.INNER);
            joinRegions.on(
                    builder.equal(fromRoot.get("regions"), joinRoot.get("id"))
            );

            criteriaQuery.select(joinRoot);

            criteriaQuery.where(builder.equal(fromRoot.get("id"), premisesLevelId));

            return autoManager.getManager()
                    .createQuery(criteriaQuery)
                    .getResultList();

        } catch (Exception ex) {
            LOG.error("Problem gathering token references", ex);
            throw ex;
        }
    }

    /**
     * Returns the amount of premises available.
     *
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPremisesListSize() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), Premises.class);
        }
    }

    /**
     * Returns the amount of property levels available.
     *
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertiesListSize() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), Property.class);
        }
    }

    /**
     * Returns the amount of premises levels available.
     *
     * @param premises the premises to get the amount of properties on.
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertiesListSize(final Premises premises) throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), Property.class);
        }
    }

    /**
     * Returns the amount of premises levels available.
     *
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertLevelsListSize() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), PropertyLevel.class);
        }
    }

    /**
     * Returns the amount of premises levels available.
     *
     * @param property The propty to get the amount of levels from.
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertLevelsListSize(final Property property) throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), PropertyLevel.class);
        }
    }

    /**
     * Start the premises service.
     *
     * @param startFuture Future to determine a correct or failed start.
     */
    @Override
    public void start(final Future<Void> startFuture) {
        prefillData();
        startFuture.complete();
    }

    /**
     * Prefills the data when minimal data is not present.
     *
     * Does a check if minimal
     *
     * 1 premises is present, 1 property on a premises, 1 property level in a
     * property. 1 property section on a property level.
     *
     */
    private void prefillData() {
        try (AutoClosableEntityManager closableManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = closableManager.getManager();
            Premises defaultPremises = manager.find(Premises.class, NumberUtils.LONG_ONE);
            if (defaultPremises == null) {
                Premises premises = new Premises();
                premises.setName("My premises");
                PremisesIcon premisesIcon = new PremisesIcon();
                premisesIcon.setIconType(IconType.FONT_AWESOME);
                premisesIcon.setIcon("map");
                premises.setIcon(premisesIcon);

                PropertySection premisesSection = new PropertySection();
                premisesSection.setName("Driveway");
                premisesSection.setSectionType(PropertySectionType.PREMISES);
                PremisesIcon premisesSectionIcon = new PremisesIcon();
                premisesSectionIcon.setIconType(IconType.FONT_AWESOME);
                premisesSectionIcon.setIcon("road");
                premisesSection.setIcon(premisesSectionIcon);
                premises.setSections(Set.of(premisesSection));

                Property property = new Property();
                property.setName("My property");
                PremisesIcon propertyIcon = new PremisesIcon();
                propertyIcon.setIconType(IconType.FONT_AWESOME);
                propertyIcon.setIcon("home");
                property.setIcon(propertyIcon);
                premises.setProperties(Set.of(property));

                PropertyLevel level = new PropertyLevel();
                level.setName("Ground floor");
                PremisesIcon levelIcon = new PremisesIcon();
                levelIcon.setIconType(IconType.FONT_AWESOME);
                levelIcon.setIcon("minus");
                level.setIcon(levelIcon);
                property.setPropertyLevels(Set.of(level));

                PropertySection section = new PropertySection();
                section.setName("Living room");
                section.setSectionType(PropertySectionType.PROPERTY);
                PremisesIcon sectionIcon = new PremisesIcon();
                sectionIcon.setIconType(IconType.FONT_AWESOME);
                sectionIcon.setIcon("couch");
                section.setIcon(sectionIcon);
                level.setSections(Set.of(section));

                manager.getTransaction().begin();
                manager.persist(sectionIcon);
                manager.persist(section);
                manager.persist(levelIcon);
                manager.persist(level);
                manager.persist(propertyIcon);
                manager.persist(property);
                manager.persist(premisesSectionIcon);
                manager.persist(premisesSection);
                manager.persist(premisesIcon);
                manager.persist(premises);
                manager.getTransaction().commit();
            }
        }
    }

    /**
     * Stops the service.
     *
     * @param stopFuture Future to determine the service stoppped correctly or
     * not.
     */
    @Override
    public void stop(final Future<Void> stopFuture) {
        stopFuture.complete();
    }

}
