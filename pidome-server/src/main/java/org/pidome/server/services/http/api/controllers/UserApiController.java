/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.auth.User;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.users.UserLogin;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.authentication.PidomeAuthUser;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.auth.PassChangeObject;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.user.UserService;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.Database.AutoClosableEntityManager;

/**
 * API Controller for the {@link org.pidome.server.entities.users.UserLogin}
 * object.
 *
 * @author John
 * @since 1.0
 */
@Path("users")
@Produces("application/json")
@Consumes("application/json")
public class UserApiController extends ApiControllerResource {

    /**
     * User API loger.
     */
    private static final Logger LOG = LogManager.getLogger(UserApiController.class);

    /**
     * Returns the user logged in.The user is put in the
     * <code>HttpServerResponse</code> object.
     *
     *
     * @param user The user logged in, if logged in.
     * @return The current logged in user.
     * @throws HttpStatusCodeException When not allowed to request
     */
    @GET
    @Path("me")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When self is returned."),
        @ResponseCode(code = ApiResponseCode.HTTP_401, condition = "When not logged in."),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When there is a server error")
    })
    public final UserLogin getMe(final @Context User user) throws HttpStatusCodeException {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(UserLogin.class, ((PidomeAuthUser) user).getUid());
        } catch (Exception ex) {
            LOG.error("Unable get self user from [{}]", user, ex);
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Server error", ex);
        }
    }

    /**
     * Change the password for the logged in user.
     *
     * @param passChange The password change object.
     * @param response The response object to return.
     * @param user The user injected for extra checks.
     */
    @PATCH
    @Path("me/password")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When password change succeeds."),
        @ResponseCode(code = ApiResponseCode.HTTP_409, condition = "When password change conflicts."),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When there is a server error")
    })
    public final void changePass(final PassChangeObject passChange, final @Context HttpServerResponse response, final @Context User user) {
        ServiceHandler.getInstance().getService(UserService.class).ifPresentOrElse(service -> {
            service.updatePassword(passChange, (PidomeAuthUser) user, handler -> {
                if (handler.succeeded()) {
                    response.setStatusCode(ApiResponseCode.HTTP_200);
                } else {
                    response.setStatusCode(ApiResponseCode.HTTP_409);
                    response.setStatusMessage(handler.cause().getMessage());
                }
                response.end();
            });
        }, () -> {
            response.setStatusCode(ApiResponseCode.HTTP_500);
            response.setStatusMessage("Authentification service not present");
            response.end();
        });
    }

    /**
     * Adds a {@link org.pidome.server.entities.users.UserLogin} to the system.
     *
     * @param user The User to add.
     * @return The {@link org.pidome.server.entities.users.UserLogin} id of the
     * added user.
     */
    @POST
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When user is added."),
        @ResponseCode(code = ApiResponseCode.HTTP_403, condition = "When not authorized.")
    })
    public final Integer addUser(final UserLogin user) {
        LOG.info("Adding user [{}]", user);
        return 1;
    }

    /**
     * Returns a list of users.
     *
     * This method applies pagination.
     *
     * @param response The response object.
     * @param request The request object.
     * @param page The pagination page, starts with 1.
     * @param pageSize The amount to return.
     * @return List of logins.
     * @throws HttpStatusCodeException When user service is unavailable.
     */
    @GET
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "Returns user list"),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When user service not available.")
    })
    public final List<UserLogin> getUsers(final @Context HttpServerResponse response,
            final @Context HttpServerRequest request,
            final @QueryParam("page") @DefaultValue("1") int page,
            final @QueryParam("pageSize") @DefaultValue("10") int pageSize) throws HttpStatusCodeException {
        try {
            if (ServiceHandler.getInstance().getService(UserService.class).isPresent()) {
                UserService service = ServiceHandler.getInstance().getService(UserService.class).get();
                int index = (page - 1) * pageSize;

                RestTools.addListHeaders(response,
                        service.getUserListSize(),
                        index,
                        pageSize);
                return service.getUsers(index, pageSize);
            } else {
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "User service not available");
            }
        } catch (Exception ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Server error", ex);
        }
    }

    /**
     * Updates a user.
     *
     * @param user The User to update.
     * @return The updated {@link org.pidome.server.entities.users.UserLogin}.
     */
    @PUT
    @Path("user/{id}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When user is updated."),
        @ResponseCode(code = ApiResponseCode.HTTP_403, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.HTTP_404, condition = "When not found.")
    })
    public final UserLogin updateUser(final UserLogin user) {
        return new UserLogin();
    }

    /**
     * Returns a User by id.
     *
     * @param id The id of the User to retrieve.
     * @return The User requested.
     * @throws HttpStatusCodeException On error.
     */
    @GET
    @Path("user/{id}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "On return of the given user."),
        @ResponseCode(code = ApiResponseCode.HTTP_403, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.HTTP_404, condition = "When not found.")
    })
    @SuppressWarnings("PMD.PreserveStackTrace")
    public final UserLogin getUser(final @PathParam("id") long id) throws HttpStatusCodeException {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            UserLogin login = autoManager.getManager().find(UserLogin.class, id);
            if (login == null) {
                throw new NoResultException();
            }
            return login;
        } catch (NoResultException ex) {
            LOG.warn("User with given id [{}] not found", id);
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Not found");
        } catch (Exception ex) {
            LOG.error("Unable get user for id [{}]: [{}]", id, ex.getMessage());
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Server error", ex);
        }
    }

    /**
     * Deletes a User by id.
     *
     * @param id The id of the User to delete.
     * @return true when deleted.
     */
    @DELETE
    @Path("user/{id}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When user is deleted."),
        @ResponseCode(code = ApiResponseCode.HTTP_403, condition = "When not authorized.")
    })
    public final boolean deleteUser(final @PathParam("id") Integer id) {
        return true;
    }

}
