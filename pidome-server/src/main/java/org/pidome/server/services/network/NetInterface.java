/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.Transport.SubSystem;
import org.pidome.server.system.hardware.peripherals.Peripheral;

/**
 * A network interface.
 *
 * @author John Sirach
 */
public class NetInterface extends Peripheral<HardwareDriver> {

    /**
     * Logger for the network interface.
     */
    private static final Logger LOG = LogManager.getLogger(NetInterface.class);

    /**
     * Used for 32 bit shift.
     */
    private static final int SHIFT_32_BIT = 31;
    /**
     * 24.
     */
    private static final int SHIFT_24_BIT = 24;
    /**
     * 16.
     */
    private static final int SHIFT_16_BIT = 16;
    /**
     * 8.
     */
    private static final int SHIFT_8_BIT = 8;
    /**
     * 255.
     */
    private static final int BIT_255 = 255;

    /**
     * the ip address of the interface.
     */
    @JsonIgnore
    private InetAddress ipAddress;

    /**
     * the subnet address.
     */
    @JsonIgnore
    private InetAddress subnetAddress;

    /**
     * The broadcast address.
     */
    @JsonIgnore
    private InetAddress broadcastAddress;

    /**
     * The network device.
     *
     * Ignored by unmarshalling as it ain't serializable. Use the method members
     * to retrieve the values.
     */
    @JsonIgnore
    private final NetworkInterface networkInterface;

    /**
     * Constructor setting type to NETWORK.
     *
     * @param netInterface The network interface for this network device.
     * @throws java.net.SocketException When ip data can not be extracted.
     * @throws org.pidome.server.services.network.NullInterfaceException
     * Interfaces which are completely unusable.
     */
    public NetInterface(final NetworkInterface netInterface) throws SocketException, NullInterfaceException {
        super(SubSystem.NETWORK);
        this.networkInterface = netInterface;
        setIpData();
        this.setName(this.networkInterface.getName());
        this.setFriendlyName(this.networkInterface.getDisplayName());
        this.setDeviceKey(this.networkInterface.getName() + ":" + this.networkInterface.getIndex());
    }

    /**
     * @return the ipAddress
     */
    @JsonIgnore
    public InetAddress getIpAddress() {
        return ipAddress;
    }

    /**
     * @return the ipAddress as String.
     */
    @JsonGetter("ipAddress")
    public String getIpAddressAsString() {
        return ipAddress.getHostAddress();
    }

    /**
     * @return the broadcastAddress
     */
    @JsonIgnore
    public InetAddress getBroadcastAddress() {
        return broadcastAddress;
    }

    /**
     * @return the ipAddress as String.
     */
    @JsonGetter("broadcastAddress")
    public String getBroadcastAddressAsString() {
        return broadcastAddress.getHostAddress();
    }

    /**
     * @return the ipSubnet
     */
    @JsonIgnore
    public InetAddress getSubnetAddress() {
        return subnetAddress;
    }

    /**
     * @return the ipSubnet as String
     */
    @JsonGetter("subnetAddress")
    public String getSubnetAddressAsString() {
        return subnetAddress.getHostAddress();
    }

    /**
     * If the interface is active. synonym for isUp();
     *
     * @return true when active.
     */
    @JsonGetter("active")
    @Override
    public boolean isActive() {
        return isUp();
    }

    /**
     * If the interface is up or not.
     *
     * @return If up.
     */
    @JsonGetter("up")
    public boolean isUp() {
        try {
            return this.networkInterface.isUp();
        } catch (SocketException ex) {
            LOG.error("Error during interface up check", ex);
            return false;
        }
    }

    /**
     * If the interface is a loopback interface.
     *
     * @return true hwn loopback interface.
     */
    @JsonGetter("loopback")
    public boolean isLoopBack() {
        try {
            return this.networkInterface.isLoopback();
        } catch (SocketException ex) {
            LOG.error("Error during check if interface is local loopback", ex);
            return false;
        }
    }

    /**
     * Extracts the ip address.
     *
     * @throws SocketException When it is not possible to extract data from the
     * interface.
     * @throws NullInterfaceException when the interface is nulled.
     */
    private void setIpData() throws SocketException, NullInterfaceException {
        Iterator<InterfaceAddress> it = networkInterface.getInterfaceAddresses().iterator();
        if (it.hasNext()) {
            InterfaceAddress address = it.next();
            if (address != null && address.getAddress() != null) {
                ipAddress = address.getAddress();
                subnetAddress = generateIPv4LocalNetMask(address.getNetworkPrefixLength());
                broadcastAddress = address.getBroadcast();
            } else {
                throw new NullInterfaceException("Interface has no address");
            }
        }
    }

    /**
     * Get the netmask from the given ip address and prefix length.
     *
     * @param netPrefixLength The prefix length
     * @return The subnet mask.
     */
    private static InetAddress generateIPv4LocalNetMask(final int netPrefixLength) {
        try {
            int shiftby = (1 << SHIFT_32_BIT);
            for (int i = netPrefixLength - 1; i > 0; i--) {
                shiftby = (shiftby >> 1);
            }
            String maskString = Integer.toString((shiftby >> SHIFT_24_BIT) & BIT_255) + "." + Integer.toString((shiftby >> SHIFT_16_BIT) & BIT_255) + "." + Integer.toString((shiftby >> SHIFT_8_BIT) & BIT_255) + "." + Integer.toString(shiftby & BIT_255);
            return InetAddress.getByName(maskString);
        } catch (UnknownHostException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        // Something went wrong here...
        return null;
    }

    /**
     * Returns the network hardware interface.
     *
     * @return The hardware interface.
     */
    @Override
    public final HardwareComponent.Interface getHardwareInterface() {
        return HardwareComponent.Interface.NETWORK;
    }

    /**
     * String representation of this object.
     *
     * @return This object as string.
     */
    @Override
    public String toString() {
        return new StringBuilder(super.toString()).append(",Ip:[").append(this.ipAddress).append("],")
                .append("Subnet:[").append(this.subnetAddress).append("],")
                .append("Broadcast:[").append(this.broadcastAddress).append("]")
                .toString();
    }

}
