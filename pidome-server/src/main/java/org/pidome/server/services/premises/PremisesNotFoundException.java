/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.premises;

/**
 * When a premises is not found.
 *
 * @author John Sirach
 */
public class PremisesNotFoundException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>PremisesNotFoundException</code> without
     * detail message.
     */
    public PremisesNotFoundException() {
        // Default constructor.
    }

    /**
     * Constructs an instance of <code>PremisesNotFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public PremisesNotFoundException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>PremisesNotFoundException</code> with the
     * specified Throwable.
     *
     * @param thr The Throwable.
     */
    public PremisesNotFoundException(final Throwable thr) {
        super(thr);
    }

}
