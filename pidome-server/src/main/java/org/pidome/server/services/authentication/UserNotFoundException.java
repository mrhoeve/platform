/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.authentication;

/**
 * Exception used when a user is not found.
 *
 * @author John Sirach
 */
public class UserNotFoundException extends Exception {

    /**
     * Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>UserNotFoundException</code> without
     * detail message.
     */
    public UserNotFoundException() {
    }

    /**
     * Constructs an instance of <code>UserNotFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public UserNotFoundException(final String msg) {
        super(msg);
    }
}
