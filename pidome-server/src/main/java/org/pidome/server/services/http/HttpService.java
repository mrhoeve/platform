/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http;

import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.PfxOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceException;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.authentication.AuthenticationService;
import org.pidome.server.services.http.api.ApiResources;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.VertXHandler;
import org.pidome.server.system.config.SystemConfig;

/**
 * Main class responsible for maintaining http based services. These services
 * comprehend: Web service: Regular UI, API Service: The service for API
 * requests. Websocket service: Service for supplying the API over websockets.
 *
 * @author John
 */
public final class HttpService extends AbstractService {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(HttpService.class);

    /**
     * Server options.
     */
    private HttpServerOptions httpServerOptions;

    /**
     * The Vert.x webservice router instance.
     */
    private Router router;

    /**
     * The http server serving the clients.
     */
    private HttpServer httpServer;

    /**
     * Default http port.
     */
    public static final int HTTP_PORT = 8080;

    /**
     * Maximum age of items transfered over tls.
     */
    private static final String TRANSPORT_SECURITY_MAX_AGE = "15768000";

    /**
     * Constructor.
     */
    public HttpService() {

    }

    /**
     * Starts the http service.
     *
     * @param startFuture The future given to start the service.
     */
    @Override
    public void start(final Future<Void> startFuture) {
        try {
            setOpts();
            setupRouters();
            httpServer = VertXHandler.getInstance().getVertX().createHttpServer(httpServerOptions)
                    .requestHandler(router)
                    .listen(res -> {
                        if (res.succeeded()) {
                            try {
                                LOG.info("Webservices started on [{}:{}]", ServiceHandler.getInstance().getService(NetworkService.class).get().getInterfaceInUse().getIpAddressAsString(), res.result().actualPort());
                            } catch (NoInterfaceAvailableException ex) {
                                LOG.info("Webservices started on port [{}]", res.result().actualPort());
                            }
                            startFuture.complete();
                        } else {
                            startFuture.fail(new ServiceException("Failed to start webservices", res.cause()));
                        }
                    });
        } catch (NoInterfaceAvailableException ex) {
            startFuture.fail(new ServiceException("Unable to determine the host", ex));
        }
    }

    /**
     * Stops the http service.
     *
     * @param stopFuture The future given to detect stopping was succesful.
     */
    @Override
    public void stop(final Future<Void> stopFuture) {
        httpServer.close(res -> {
            if (res.succeeded()) {
                LOG.info("Webservices stopped");
                stopFuture.complete();
            } else {
                stopFuture.fail(new ServiceException("Failed to stop webservices", res.cause()));
            }
        });
    }

    /**
     * Returns the host bound to.
     *
     * @return The host as string, this can be either a name or ip addres.
     */
    public String getBoundHostAsString() {
        return httpServerOptions.getHost();
    }

    /**
     * Install all the routes to be able to service web service users.
     */
    private void setupRouters() {
        final String apiLocation = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.rest", "/api");

        router = Router.router(VertXHandler.getInstance().getVertX());
        LOG.trace("Creating body handler");
        router.route().handler(BodyHandler.create());

        LOG.info("Mounting [{}] as the API entry point for all REST calls", apiLocation);
        Router apiRouter = ApiResources.getInstance().build();
        ServiceHandler.getInstance().getService(AuthenticationService.class).ifPresent(authService -> {
            authService.addToRoute(apiRouter, "/api/auth/service/");
        });
        router.mountSubRouter(apiLocation, apiRouter);

        String staticPath = SystemConfig.getResourcePath("resources/http/" + SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.interface", "default/"));
        LOG.info("Serving static files from: [{}, {}]", staticPath, new File(staticPath).exists());
        StaticHandler staticHandler
                = StaticHandler.create(staticPath)
                        .setCachingEnabled(true)
                        .setDefaultContentEncoding("UTF-8")
                        .setIndexPage("index.html")
                        .setIncludeHidden(false);
        router.route("/site").handler(staticHandler);
        appendSecurityHeaders();
    }

    /**
     * Adds headers to improve security a bit.
     */
    private void appendSecurityHeaders() {
        router.route().handler(ctx -> {
            ctx.response()
                    // prevents Internet Explorer from MIME - sniffing a
                    // response away from the declared content-type
                    .putHeader("X-Content-Type-Options", "nosniff")
                    // Strict HTTPS (for about ~6Months)
                    .putHeader("Strict-Transport-Security", "max-age=" + TRANSPORT_SECURITY_MAX_AGE)
                    // IE8+ do not allow opening of attachments in the context of this resource
                    .putHeader("X-Download-Options", "noopen")
                    // enable XSS for IE
                    .putHeader("X-XSS-Protection", "1; mode=block")
                    // deny frames
                    .putHeader("X-FRAME-OPTIONS", "DENY");
            ctx.next();
        });
    }

    /**
     * Set's the options for the http service.
     *
     * @throws NoInterfaceAvailableException When there is no interface
     * available to bind to.
     */
    private void setOpts() throws NoInterfaceAvailableException {
        final String hostIp;
        final NetworkService networkService = ServiceHandler.getInstance().getService(NetworkService.class).get();
        if (SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.ip", "network.ip").equals("network.ip")) {
            hostIp = networkService.getInterfaceInUse().getIpAddressAsString();
        } else {
            hostIp = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.ip", networkService.getInterfaceInUse().getIpAddressAsString());
        }

        httpServerOptions = new HttpServerOptions()
                .setUseAlpn(true)
                .setSsl(true)
                .setPort(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.port", HTTP_PORT))
                .setHost(hostIp)
                .setLogActivity(true)
                .setPfxKeyCertOptions(
                        new PfxOptions()
                                .setPath(
                                        SystemConfig.getResourcePath(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.keystore", ""))
                                )
                                .setPassword(
                                        SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.certpass", "")
                                )
                );
    }

}
