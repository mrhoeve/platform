/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.HardwareDriverDiscovery;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.server.services.hardware.HardwareService;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.HardwareDriverStore;
import org.pidome.server.system.hardware.peripherals.Peripheral;
import org.pidome.server.system.hardware.peripherals.PeripheralDriverCandidate;

/**
 * Controller for controlling and maintaining the configurations, adding and
 * removing of peripherals to the system.
 *
 * @author John Sirach
 * @since 1.0
 */
@Path("hardware")
@Produces("application/json")
@Consumes("application/json")
@SuppressWarnings("CPD-START")
public final class HardwareApiController extends ApiControllerResource {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(HardwareApiController.class);

    /**
     * Not found literal.
     */
    private static final String NOT_FOUND = "Not found";

    /**
     * Returns a list of connected peripherals.
     *
     * @return List of connected peripherals.
     */
    @GET
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "Succesfully returns the hardware.")
    })
    public Map<HardwareComponent.Interface, HardwareComponent> getHardware() {
        return getService(HardwareService.class).get().getRoot().getHardware();
    }

    /**
     * Returns a list of connected peripherals.
     *
     * @param interfaceKey The key of the hardware component to fetch.
     * @return The hardware component.
     * @throws HttpStatusCodeException When the component is not found.
     */
    @GET
    @Path("{interface}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "On returning existing interface."),
        @ResponseCode(code = ApiResponseCode.HTTP_404, condition = "When the interface is not found.")
    })
    public HardwareComponent getHardwareComponent(final @PathParam("interface") HardwareComponent.Interface interfaceKey) throws HttpStatusCodeException {
        return getService(HardwareService.class).get().getRoot().getHardware(interfaceKey).orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND));
    }

    /**
     * Returns a piece of hardware attached to the given port.
     *
     * PiDome uses custom mapping to hardware ports. This way there is a generic
     * access to any connected device. Before this method is called you need to
     * find out on which virtual port a device is connected by calling
     * <code>getPeripherals</code>
     *
     * @param interfaceKey The interface to retrieve the hardware device from.
     * @param key The key of the device to get the information from.
     * @return The piece of hardware
     * @throws HttpStatusCodeException When there is no piece of hardware
     * connected on that port.
     */
    @GET
    @Path("{interface}/{key}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "On returning existing peripheral on the given interface."),
        @ResponseCode(code = ApiResponseCode.HTTP_404, condition = "When the peripheral or interface is not found.")
    })
    public Peripheral<? extends HardwareDriver> getPeripheral(final @PathParam("interface") HardwareComponent.Interface interfaceKey,
            final @PathParam("key") String key) throws HttpStatusCodeException {
        Optional<HardwareComponent> component = getService(HardwareService.class).get().getRoot().getHardware(interfaceKey);
        return component.orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND))
                .getPeripheralByKey(key).orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND));
    }

    /**
     * Returns the list of hardware peripheral driver candidates.
     *
     * @param interfaceKey The key of the interface where the peripheral is
     * connected.
     * @param key The unique device key.
     * @return List if hardware drivers capable to service the given key.
     * @throws HttpStatusCodeException When the interface or peripheral is
     * unknown.
     */
    @GET
    @Path("{interface}/{key}/drivers")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "On returning a list of drivers capapable of driving the peripheral"),
        @ResponseCode(code = ApiResponseCode.HTTP_404, condition = "When the interface, peripheral is not found.")
    })
    public List<PeripheralDriverCandidate> getPeripheralDriverCandidates(final @PathParam("interface") HardwareComponent.Interface interfaceKey,
            final @PathParam("key") String key) throws HttpStatusCodeException {
        Optional<HardwareComponent> component = getService(HardwareService.class).get().getRoot().getHardware(interfaceKey);
        Peripheral peripheral = component.orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND))
                .getPeripheralByKey(key).orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND));
        Set<Class<? extends HardwareDriver>> drivers = HardwareDriverStore.getTransportDrivers(peripheral.getSubSystem());
        List<PeripheralDriverCandidate> collection = new ArrayList<>();
        Iterator<Class<? extends HardwareDriver>> iter = drivers.iterator();
        while (iter.hasNext()) {
            Class<? extends HardwareDriver> driver = iter.next();
            List<Annotation> annotations = Arrays.asList(driver.getAnnotations());
            annotations.stream().forEach(annotation -> {
                if (annotation instanceof HardwareDriverDiscovery) {
                    LOG.info("found candidate for [{}:{}]: {}", interfaceKey, key, ((HardwareDriverDiscovery) annotation).name());
                    PeripheralDriverCandidate candidate = new PeripheralDriverCandidate();
                    candidate.setName(((HardwareDriverDiscovery) annotation).name());
                    candidate.setDescription(((HardwareDriverDiscovery) annotation).description());
                    candidate.setDriver(driver.getCanonicalName());
                    collection.add(candidate);
                }
            });
        }
        return collection;
    }

    /**
     * Returns the settings for the given interface with driver candidate.
     *
     * A driver candidate is a driver which would possibly be used as the driver
     * to drive the attached hardware.
     *
     * @param interfaceKey The hardware interface.
     * @param key The hardware key.
     * @param driverCandidate Get the settings for the given driver candidate.
     * @return The settings for the given candidate as <code>InputForm</code>.
     * @throws HttpStatusCodeException On server error.
     */
    @GET
    @Path("{interface}/{key}/settings")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "On returning a presentation of type InputForm for settings purposes."),
        @ResponseCode(code = ApiResponseCode.HTTP_404, condition = "When the interface and/or peripheral is not found."),
        @ResponseCode(code = ApiResponseCode.HTTP_400, condition = "When the given driver is not suitable for the peripheral targeted")
    })
    public InputForm getPeripheralSettings(final @PathParam("interface") HardwareComponent.Interface interfaceKey,
            final @PathParam("key") String key, final @PathParam("driver") String driverCandidate) throws HttpStatusCodeException {
        Optional<HardwareComponent> component = getService(HardwareService.class).get().getRoot().getHardware(interfaceKey);
        Peripheral peripheral = component.orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND))
                .getPeripheralByKey(key).orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND));
        return peripheral.getDriver().getConfiguration();
    }

}
