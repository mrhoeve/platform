/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.cluster;

import java.util.UUID;

/**
 * Class providing host information.
 *
 * @author John Sirach
 */
public class HostInformation {

    /**
     * Temporary restart changing UUID until cluster services are in effect.
     */
    private static final UUID TEMP_UUID = UUID.randomUUID();

    /**
     * The host information of the master host.
     */
    private final HostInformation clusterMaster = null;

    /**
     * The id of the node.
     */
    private final UUID nodeId = TEMP_UUID;

    /**
     * The name of the server.
     */
    private String serverName;

    /**
     * The server version.
     */
    private ServerVersion version;

    /**
     * The platform info.
     */
    private PlatformInfo platform;

    /**
     * @return the serverName
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @param serverName the serverName to set.
     */
    public void setServerName(final String serverName) {
        this.serverName = serverName;
    }

    /**
     * @return the version
     */
    public HostInformation.ServerVersion getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(final HostInformation.ServerVersion version) {
        this.version = version;
    }

    /**
     * @return the platform
     */
    public PlatformInfo getPlatform() {
        return platform;
    }

    /**
     * @param platform the platform to set
     */
    public void setPlatform(final PlatformInfo platform) {
        this.platform = platform;
    }

    /**
     * The server version.
     */
    public static class ServerVersion {

        /**
         * Major version.
         */
        private int major = 1;
        /**
         * Minor version.
         */
        private int minor = 0;
        /**
         * Patch version.
         */
        private int patch = 0;
        /**
         * Build version.
         */
        private int build = 0;
        /**
         * Release name.
         */
        private String name = "octopi";
        /**
         * Release type.
         */
        private String type = "local";
        /**
         * Build date.
         */
        private String date = "0000-00-00";

        /**
         * If the current version is a snapshot version or not.
         */
        private boolean snapshot = false;

        /**
         * @return the major
         */
        public int getMajor() {
            return major;
        }

        /**
         * @param major the major to set
         */
        public void setMajor(final int major) {
            this.major = major;
        }

        /**
         * @return the minor
         */
        public int getMinor() {
            return minor;
        }

        /**
         * @param minor the minor to set
         */
        public void setMinor(final int minor) {
            this.minor = minor;
        }

        /**
         * @return the patch
         */
        public int getPatch() {
            return patch;
        }

        /**
         * @param patch the patch to set
         */
        public void setPatch(final int patch) {
            this.patch = patch;
        }

        /**
         * @return the build
         */
        public int getBuild() {
            return build;
        }

        /**
         * @param build the build to set
         */
        public void setBuild(final int build) {
            this.build = build;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(final String name) {
            this.name = name;
        }

        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(final String type) {
            this.type = type;
        }

        /**
         * @return the date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date the date to set
         */
        public void setDate(final String date) {
            this.date = date;
        }

        /**
         * @return the snapshot
         */
        public boolean isSnapshot() {
            return snapshot;
        }

        /**
         * @param snapshot the snapshot to set
         */
        public void setSnapshot(final boolean snapshot) {
            this.snapshot = snapshot;
        }

    }

    /**
     * Identifying the platform running on.
     */
    public static class PlatformInfo {

        /**
         * The reported arch.
         */
        private String arch;

        /**
         * The reported operating system.
         */
        private String os;

        /**
         * If it is a raspberry pi or not.
         */
        private boolean pi;

        /**
         * The java vendor info.
         */
        private String javaVendorInfo;
        /**
         * The java version info.
         */
        private String javaVersionInfo;

        /**
         * @return the arch
         */
        public String getArch() {
            return arch;
        }

        /**
         * @return the os
         */
        public String getOs() {
            return os;
        }

        /**
         * @return the pi
         */
        public boolean isPi() {
            return pi;
        }

        /**
         * @param arch the arch to set
         */
        public void setArch(final String arch) {
            this.arch = arch;
        }

        /**
         * @param os the os to set
         */
        public void setOs(final String os) {
            this.os = os;
        }

        /**
         * @param pi the pi to set
         */
        public void setPi(final boolean pi) {
            this.pi = pi;
        }

        /**
         * @return the javaBuildInfo
         */
        public String getJavaVendorInfo() {
            return javaVendorInfo;
        }

        /**
         * @param javaVendorInfo the javaBuildInfo to set
         */
        public void setJavaVendorInfo(final String javaVendorInfo) {
            this.javaVendorInfo = javaVendorInfo;
        }

        /**
         * @return the javaVersionInfo
         */
        public String getJavaVersionInfo() {
            return javaVersionInfo;
        }

        /**
         * @param javaVersionInfo the javaVersionInfo to set
         */
        public void setJavaVersionInfo(final String javaVersionInfo) {
            this.javaVersionInfo = javaVersionInfo;
        }

    }

    /**
     * @return the clusterMaster
     */
    public HostInformation getClusterMaster() {
        return clusterMaster;
    }

    /**
     * @return the nodeId
     */
    public UUID getNodeId() {
        return nodeId;
    }

}
