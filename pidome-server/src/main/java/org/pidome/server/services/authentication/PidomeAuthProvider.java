/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.authentication;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWK;
import io.vertx.ext.jwt.JWT;
import io.vertx.ext.jwt.JWTOptions;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.ServiceHandler;

/**
 * Authentication provider.
 *
 * @author John Sirach
 */
public final class PidomeAuthProvider implements JWTAuth {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(PidomeAuthProvider.class);

    /**
     * The JWTR object.
     */
    private final transient JWT jwt;

    /**
     * The options set.
     */
    private final transient JWTOptions jwtOptions;

    /**
     * JWT literal string.
     */
    private static final String JWT = "jwt";

    /**
     * Constructor.
     *
     * @param config The configuration for the tokens.
     */
    public PidomeAuthProvider(final JWTAuthOptions config) {
        this.jwtOptions = config.getJWTOptions();
        this.jwt = new JWT();
        final List<PubSecKeyOptions> keys = config.getPubSecKeys();
        if (keys != null) {
            config.getPubSecKeys().forEach((pubSecKey) -> {
                if (pubSecKey.isSymmetric()) {
                    jwt.addJWK(new JWK(pubSecKey.getAlgorithm(), pubSecKey.getPublicKey()));
                } else {
                    jwt.addJWK(new JWK(pubSecKey.getAlgorithm(), pubSecKey.isCertificate(), pubSecKey.getPublicKey(), pubSecKey.getSecretKey()));
                }
            });
        }

        final List<JsonObject> jwks = config.getJwks();

        if (jwks != null) {
            jwks.forEach((jwk) -> {
                this.jwt.addJWK(new JWK(jwk));
            });
        }

    }

    /**
     * Authenticate the given token information.
     *
     * @param authInfo The information containing the token.
     * @param resultHandler Handler used to report back success or failure.
     */
    @Override
    public void authenticate(final JsonObject authInfo, final Handler<AsyncResult<User>> resultHandler) {
        try {
            if (authInfo.getString(JWT) == null || authInfo.getString(JWT).isBlank()) {
                resultHandler.handle(Future.failedFuture("No token present"));
                return;
            }

            AuthenticationService service = ServiceHandler.getInstance().getService(AuthenticationService.class).orElseThrow(() -> new RuntimeException());
            if (!service.isTokenRevoked(authInfo.getString(JWT))) {
                final JsonObject payload = jwt.decode(authInfo.getString(JWT));
                if (jwt.isExpired(payload, jwtOptions)) {
                    resultHandler.handle(Future.failedFuture("Expired JWT token."));
                    return;
                }
                if (jwtOptions.getAudience() != null) {
                    JsonArray target;
                    if (payload.getValue("aud") instanceof String) {
                        target = new JsonArray().add(payload.getValue("aud", ""));
                    } else {
                        target = payload.getJsonArray("aud", new JsonArray());
                    }

                    if (Collections.disjoint(jwtOptions.getAudience(), target.getList())) {
                        resultHandler.handle(Future.failedFuture("Invalid JWT audient. expected: " + Json.encode(jwtOptions.getAudience())));
                        return;
                    }
                }
                if (jwtOptions.getIssuer() != null) {
                    if (!jwtOptions.getIssuer().equals(payload.getString("iss"))) {
                        resultHandler.handle(Future.failedFuture("Invalid JWT issuer"));
                        return;
                    }
                }
                resultHandler.handle(Future.succeededFuture(new PidomeAuthUser(payload)));
            } else {
                resultHandler.handle(Future.failedFuture("Invalid token"));
            }
        } catch (RuntimeException e) {
            LOG.error("Unable to verify token", e);
            resultHandler.handle(Future.failedFuture(e));
        }
    }

    /**
     * Generates a token with the given options.
     *
     * @param claims The claims supplied.
     * @param options The options given.
     * @return The signed token generated with the claims and options.
     */
    @Override
    public String generateToken(final JsonObject claims, final JWTOptions options) {
        final JsonObject claimsCopy = claims.copy(); // prevent insight and modifications.

        // we do some "enhancement" of the claims to support roles and permissions
        if (options.getPermissions() != null && !claimsCopy.containsKey("role")) {
            claimsCopy.put("role", new JsonArray(options.getPermissions()));
        }

        return jwt.sign(claimsCopy, options);
    }
}
