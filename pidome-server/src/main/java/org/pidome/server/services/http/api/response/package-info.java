/**
 * Default responses.
 * <p>
 * Provides default exception and response code to be used within the REST controllers.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.http.api.response;
