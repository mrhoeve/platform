/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.auth;

import org.pidome.server.services.authentication.PidomeAuthTokenReference;

/**
 * A simple login object which can be used to authenticate a user.
 *
 * @author John Sirach
 */
public class LoginObject {

    /**
     * The username.
     */
    private String username;

    /**
     * The password.
     */
    private String password;

    /**
     * The source where the login originates from.
     */
    private PidomeAuthTokenReference loginSource;

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the loginSource
     */
    public PidomeAuthTokenReference getLoginSource() {
        return loginSource;
    }

    /**
     * @param loginSource the loginSource to set
     */
    public void setLoginSource(final PidomeAuthTokenReference loginSource) {
        this.loginSource = loginSource;
    }

}
