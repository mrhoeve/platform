/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.authentication;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.pidome.platform.presentation.PresentationPlatform;

/**
 * A login resource information object which will be bound to a token.
 *
 * The login resource information object adds information to a token to help an
 * end user identifying where the token has been issued. It is loosely based on
 * information which would be available in an http user agent header.
 *
 * The combination of all parameters MUST help an user in identifying where a
 * token has been issued. It is highly advisable all the parameters are present.
 * If the object is not supplied the server will not attempt to read an user
 * agent header. It is the responsibility of the app creator to supply this
 * information.
 *
 * The server will not rely on the user agent information as this is not always
 * available. In app API's do often not supply these and if these are supplied
 * it is unknown how this has been setup and make a proper guess.
 *
 * @author John Sirach
 * @since 1.0
 */
@Entity
public class PidomeAuthTokenReference implements Serializable {

    /**
     * Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * the UUID of the token.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    /**
     * The browser type.
     *
     * The type of browser can be a browser, application or other identifyer
     * understandable for an end user a specific type of application is being
     * used. Example: Google chrome is a browser, the PiDome app is mobile-app.
     */
    private String browserType;
    /**
     * The name of the browser.
     *
     * The name of the browser helps an end user the name of the application
     * type mentioned in <code>browserType</code>. For example the name Chrome
     * is from the Chrome browser, PiDome is from the PiDome mobile-app.
     */
    private String browserName;
    /**
     * The version of the browser used.
     */
    private String browserVersion;
    /**
     * The device type being used.
     *
     * This is identifyable for the end user. Chrome can either be on a desktop
     * or mobile device.
     */
    private String deviceType;
    /**
     * The name of the operating system the browser is running on.
     */
    private String osName;
    /**
     * The version of the OS the browser is running on.
     */
    private String osVersion;
    /**
     * The ip address of the login resource.
     */
    private String remoteIp;

    /**
     * If the session viewed is the current session.
     *
     * This is only applied when the user requesting it is the logged in user.
     * Otherwise always false.
     */
    @Transient
    private boolean currentSession;
    /**
     * When the token has been issued.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date issuedAt;

    /**
     * the presentation platform the token is issued on.
     */
    @Enumerated(EnumType.STRING)
    private PresentationPlatform issuedPlatform;

    /**
     * Execute when creating.
     */
    @PrePersist
    protected void onCreate() {
        this.issuedAt = new Date();
    }

    /**
     * @return the browserType
     */
    public String getBrowserType() {
        return browserType;
    }

    /**
     * @param browserType the browserType to set
     */
    public void setBrowserType(final String browserType) {
        this.browserType = browserType;
    }

    /**
     * @return the browserName
     */
    public String getBrowserName() {
        return browserName;
    }

    /**
     * @param browserName the browserName to set
     */
    public void setBrowserName(final String browserName) {
        this.browserName = browserName;
    }

    /**
     * @return the browserVersion
     */
    public String getBrowserVersion() {
        return browserVersion;
    }

    /**
     * @param browserVersion the browserVersion to set
     */
    public void setBrowserVersion(final String browserVersion) {
        this.browserVersion = browserVersion;
    }

    /**
     * @return the deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType the deviceType to set
     */
    public void setDeviceType(final String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the osName
     */
    public String getOsName() {
        return osName;
    }

    /**
     * @param osName the osName to set
     */
    public void setOsName(final String osName) {
        this.osName = osName;
    }

    /**
     * @return the osVersion
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * @param osVersion the osVersion to set
     */
    public void setOsVersion(final String osVersion) {
        this.osVersion = osVersion;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    /**
     * @param currentSession the currentSession to set
     */
    public void setCurrentSession(final boolean currentSession) {
        this.currentSession = currentSession;
    }

    /**
     * @return the issuedAt
     */
    public Date getIssuedAt() {
        return (Date) issuedAt.clone();
    }

    /**
     * @return the currentSession
     */
    public boolean isCurrentSession() {
        return currentSession;
    }

    /**
     * @param issuedPlatform the issuedPlatform to set
     */
    public void setIssuedPlatform(final PresentationPlatform issuedPlatform) {
        this.issuedPlatform = issuedPlatform;
    }

    /**
     * @return the issuedPlatform
     */
    public PresentationPlatform getIssuedPlatform() {
        return issuedPlatform;
    }

    /**
     * @return the remoteIp
     */
    public String getRemoteIp() {
        return remoteIp;
    }

    /**
     * @param remoteIp the remoteIp to set
     */
    public void setRemoteIp(final String remoteIp) {
        this.remoteIp = remoteIp;
    }

    /**
     * Generate hash code.
     *
     * @return The object hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    @SuppressWarnings("CPD-END")
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PidomeAuthTokenReference other = (PidomeAuthTokenReference) obj;
        return Objects.equals(this.id, other.id);
    }

}
