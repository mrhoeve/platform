/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.authentication;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.users.Role;
import org.pidome.tools.utilities.NumberUtils;

/**
 * User mapping for jwt token auth.
 *
 * @author John Sirach
 */
public class PidomeAuthUser extends AbstractUser {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(PidomeAuthUser.class);

    /**
     * The token.
     */
    private JsonObject jwtToken;
    /**
     * the roles.
     */
    private Set<Role> roles = new HashSet<>();

    /**
     * Empty constructor.
     */
    public PidomeAuthUser() {
        // required if the object is serialized, however this is not a good idea
        // because JWT are supposed to be used in stateless environments
        LOG.debug("You are probably serializing the PidomeAuthUser (JWT) User, JWT are supposed to be used in stateless servers!");
    }

    /**
     * Construct the JWT user.
     *
     * @param jwtToken The token set.
     */
    public PidomeAuthUser(final JsonObject jwtToken) {
        this.jwtToken = jwtToken;
        JsonArray stringRoles = jwtToken.getJsonArray("role");
        if (stringRoles != null) {
            stringRoles.forEach(role -> {
                this.roles.add(Role.fromString((String) role));
            });
        } else {
            this.roles.add(Role.UNKNOWN);
        }
    }

    /**
     * Returns the JWT user principal.
     *
     * @return JsonObject The token as principal.
     */
    @Override
    public JsonObject principal() {
        return jwtToken;
    }

    /**
     * Sets the auth provider.
     *
     * Noop, JWT tokens are self contained.
     *
     * @param authProvider The provider to set.
     */
    @Override
    public void setAuthProvider(final AuthProvider authProvider) {
        /// Override without implementation.
    }

    /**
     * Check if this user's role is permitted to perform an action checked
     * against the given role.
     *
     * In this stage we do not distinguish roles and authorities. this method
     * proxies to <code>isAuthorized</code>
     *
     * @param role The role to check as string.
     * @param resultHandler The handler to handle the role check.
     */
    @Override
    protected void doIsPermitted(final String role, final Handler<AsyncResult<Boolean>> resultHandler) {
        LOG.trace("Checking role [{} in [{}]]", role, this.roles);
        checkRole(role, resultHandler);
    }

    /**
     * Returns if a user is authorized on the checked authority.
     *
     * @param authority The authority to check this user on.
     * @param resultHandler The result handler for the permission check result.
     * @return The UserLogin object.
     */
    @Override
    public User isAuthorized(final String authority, final Handler<AsyncResult<Boolean>> resultHandler) {
        LOG.trace("Checking authority [{}, {}]", authority, this.roles);
        return checkRole(authority, resultHandler);
    }

    /**
     * The check if the given role/permission checks out.
     *
     * @param roleCheck The role/permission to check.
     * @param resultHandler The handler to post the result to.
     * @return The self object.
     */
    private PidomeAuthUser checkRole(final String roleCheck, final Handler<AsyncResult<Boolean>> resultHandler) {
        for (Role role : this.roles) {
            if (roleCheck != null && Role.fromString(roleCheck).isHigherOrEqualLevelThan(role)) {
                resultHandler.handle(Future.succeededFuture(true));
                return this;
            }
        }
        LOG.debug("User does not have or does not has the required level for role [{}]", roleCheck);
        resultHandler.handle(Future.failedFuture("User role not sufficient"));
        return this;
    }

    /**
     * Checks if the given role is higher or equal to any user given role.
     *
     * @param role The rol to check against.
     * @return true when the user has a role which is higher or equal to te
     * given role.
     */
    public boolean hasHigherOrEqualLevelThan(final Role role) {
        return this.roles.stream().anyMatch((myRole) -> (myRole.isHigherOrEqualLevelThan(role)));
    }

    /**
     * Checks if the given role is lower then any role the user has.
     *
     * @param role The rol to check against.
     * @return true when the user has a role which is lower than te given role.
     */
    public boolean hasLowerLevelThan(final Role role) {
        return this.roles.stream().anyMatch((myRole) -> (myRole.isLowerLevelThan(role)));
    }

    /**
     * Returns the user's UID.
     *
     * @return The uid.
     */
    public final long getUid() {
        return jwtToken.getLong("uid", NumberUtils.LONG_ZERO);
    }

}
