/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import io.vertx.core.http.HttpServerResponse;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;

/**
 * Helpers for rest.
 *
 * @author John Sirach
 */
public final class RestTools {

    /**
     * Private utility class constructor.
     */
    private RestTools() {
        /// Default utility constructor.
    }

    /**
     * Header for total amounts.
     */
    private static final String AMOUNT_HEADER = "X-ACME-AMOUNT-TOTAL";
    /**
     * Header for the index in the total list.
     */
    private static final String AMOUNT_INDEX = "X-ACME-AMOUNT-INDEX";
    /**
     * The size of the list to return.
     */
    private static final String AMOUNT_SIZE = "X-ACME-AMOUNT-SIZE";

    /**
     * Adds a header with an number.
     *
     * Should only be used when returing lists.
     *
     * @param response The response object.
     * @param total The amount to register.
     * @param index The start index of the list.
     * @param size The size of the list from index to end.
     */
    public static void addListHeaders(final HttpServerResponse response, final long total, final long index, final long size) {
        response.putHeader(AMOUNT_HEADER, String.valueOf(total));
        response.putHeader(AMOUNT_INDEX, String.valueOf(total));
        response.putHeader(AMOUNT_SIZE, String.valueOf(total));
    }

    /**
     * Check if the given object is not null.
     *
     * @param object the object to check if null.
     * @throws HttpStatusCodeException throws a 404 marked exception if null.
     */
    public static void assertNotNull(final Object object) throws HttpStatusCodeException {
        if (object == null) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Not found");
        }
    }

}
