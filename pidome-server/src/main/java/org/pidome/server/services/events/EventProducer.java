/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

/**
 * An event producer which only produces events.
 *
 * @author John Sirach
 * @param <T> Type begin produced by this producer.
 */
public class EventProducer<T> extends EventBusMessageHandler<T> {

    /**
     * Constructor for the event consumer.
     *
     * @param address The address where the consumer should listen to.
     */
    public EventProducer(final EventAddress address) {
        super(address);
    }

    /**
     * Handle the passed item.
     *
     * @param item The item to pass.
     */
    @Override
    public void handle(final T item) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
