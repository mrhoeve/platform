/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services;

import io.vertx.core.DeploymentOptions;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.VertXHandler;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.tools.sort.TopologicalSort;
import org.pidome.tools.sort.TopologicalSortCycleException;

/**
 * Main entry class for handling services.
 *
 * @author John
 */
public final class ServiceHandler {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(ServiceHandler.class);

    /**
     * Registered map of running services.
     */
    private final transient ConcurrentHashMap<PiDomeService, AbstractService> services = new ConcurrentHashMap<>();

    /**
     * The start list after ordering the services their dependencies.
     */
    private final transient List<PiDomeService> orderedStartList = Collections.synchronizedList(new ArrayList<>());

    /**
     * The que where services that need to be started are put in.
     */
    private final transient ArrayBlockingQueue<PiDomeService> startQueue = new ArrayBlockingQueue<>(PiDomeService.values().length);

    /**
     * Latch containing the amount of services to start.
     */
    private transient CountDownLatch startLatch;

    /**
     * Latch containing the amount of services to stop.
     */
    private transient CountDownLatch stopLatch;

    /**
     * The service handler static instance.
     */
    private static ServiceHandler handler;

    /**
     * Service handler constructor.
     */
    private ServiceHandler() {
    }

    /**
     * Returns the service handler instance.
     *
     * @return The service handlers.
     */
    public static synchronized ServiceHandler getInstance() {
        if (handler == null) {
            handler = new ServiceHandler();
        }
        return handler;
    }

    /**
     * Performs services Boot sequence.
     *
     * This method makes sure all services are started correctly.
     */
    public void servicesBoot() {
        if (!SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.veryfirstrun", true)) {
            firstBoot();
        } else {
            startServices();
        }
    }

    /**
     * This method makes sure all services are stopped correctly.
     */
    public void servicesShutdown() {
        stopServices();
    }

    /**
     * Starts all services.
     */
    private void startServices() {
        startServices(PiDomeService.values());
    }

    /**
     * Starts the given services and waits for them all to be started.
     *
     * @param startServices The services to start blocking.
     * @throws java.lang.InterruptedException When the blocking queue is
     * interrupted.
     */
    public void startServicesBlocking(final PiDomeService... startServices) throws InterruptedException {
        startServices(startServices);
        this.startLatch.await();
    }

    /**
     * Starts all services.
     *
     * This method starts all services in dependency order and is able to start
     * sets of services when all depending have been started.
     *
     * When called for the second time only services not already started will be
     * started.
     *
     * @param startServices The services to start.
     */
    private void startServices(final PiDomeService... startServices) {
        TopologicalSort<PiDomeService> topoSort = new TopologicalSort<>();
        /// Check if all dependencies are also in the list in case not all services which are dependent are supplied in the original list.
        Set<PiDomeService> vertexList = new HashSet<>();
        for (PiDomeService service : startServices) {
            populateDependencies(service, vertexList);
        }
        startLatch = new CountDownLatch(vertexList.size());
        topoSort.addVertexSet(vertexList);
        vertexList.forEach((serviceItem) -> {
            serviceItem.getDependencies().forEach((edgeService) -> {
                topoSort.addEdge(edgeService, serviceItem);
            });
        });
        try {
            orderedStartList.addAll(topoSort.sort());
            queueServices();
            try {
                while (!orderedStartList.isEmpty()) {
                    PiDomeService service = startQueue.take();
                    startService(service);
                }
            } catch (InterruptedException ex) {
                LOG.error("Start queue unavailable [{}]", ex.getMessage());
            }
        } catch (TopologicalSortCycleException ex) {
            /// Could not sort, hope for the best.
            LOG.error("Sorting service dependencies failed, we are unable to continue");
            throw new RuntimeException(ex);
        }

    }

    /**
     * Populates the dependencies of the given service into serviceList.
     *
     * This is a recursive method which calls itself until the service list is
     * fully populated with all dependencies and services requesting to gather
     * their dependencies.
     *
     * @param service The service to gather the dependencies for.
     * @param serviceList The service list in which the populated services and
     * their dependencies are put into.
     * @return The given set populated.
     */
    private Set<PiDomeService> populateDependencies(final PiDomeService service, final Set<PiDomeService> serviceList) {
        serviceList.add(service);
        if (service.getDependencies().size() > 0) {
            service.getDependencies().stream().forEach(dependency -> {
                populateDependencies(dependency, serviceList);
            });
        }
        return serviceList;
    }

    /**
     * Queue's the next services.
     */
    private void queueServices() {
        synchronized (orderedStartList) {
            Iterator<PiDomeService> servicesIterator = orderedStartList.iterator();
            while (servicesIterator.hasNext()) {
                PiDomeService service = servicesIterator.next();
                if (hasDependenciesLoaded(service)) {
                    servicesIterator.remove();
                    try {
                        LOG.info("Putting service [{}] in queue", service);
                        if (!services.containsKey(service)) {
                            startQueue.put(service);
                            break;
                        } else {
                            LOG.error("Service [{}] has already been started, please contact PiDome to review", service);
                        }
                    } catch (InterruptedException ex) {
                        LOG.error("Unable to put service [{}] in start queue from next batch signal", service, ex.getMessage());
                    }
                }
            }
        }
    }

    /**
     * A check if the dependencies have been loaded for the given service.
     *
     * @param service The service to check for the loaded dependencies.
     * @return true when all dependencies are loaded.
     */
    private boolean hasDependenciesLoaded(final PiDomeService service) {
        if (service.getDependencies() != null && !service.getDependencies().isEmpty()) {
            if (!service.getDependencies().stream().noneMatch((dependency) -> (!services.containsKey(dependency)))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Stops the given services and waits for them all to be stopped.
     *
     * @throws java.lang.InterruptedException When the blocking queue is
     * interrupted.
     */
    public void stopServicesBlocking() throws InterruptedException {
        stopServices();
        LOG.info("Waiting for [{}] services to stop", this.stopLatch.getCount());
        this.stopLatch.await();
    }

    /**
     * Stops all services.
     */
    private void stopServices() {
        stopLatch = new CountDownLatch(this.services.size());
        this.services.keySet().forEach((service) -> {
            stopService(service);
        });
    }

    /**
     * Starts a single service.
     *
     * @param service The PiDome service to start.
     */
    public void startService(final PiDomeService service) {
        if (!services.containsKey(service)) {
            try {
                LOG.info("Request to start service [{}]", service.getServiceName());
                Class<? extends AbstractService> clazz = service.getServiceClass();
                Constructor<?> ctor = clazz.getConstructor();
                final AbstractService serviceObject = (AbstractService) ctor.newInstance();
                final DeploymentOptions blockingOptions = new DeploymentOptions().setWorker(true);
                VertXHandler.getInstance().getVertX().deployVerticle(serviceObject,
                        blockingOptions,
                        result -> {
                            if (result.succeeded()) {
                                services.put(service, serviceObject);
                                LOG.info("Deployment succeeded for [{}]", service.getServiceName());
                            } else {
                                LOG.error("Deployment failed for [{}]", service.getServiceName(), result.cause());
                            }
                            queueServices();
                            startLatch.countDown();
                        });
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOG.error("Service [{}] not started because of [{}]", service.getServiceName(), ex.getCause(), ex);
            }
        } else {
            LOG.warn("Service [{}] already registered, please shut down before starting, or use restart", service);
        }
    }

    /**
     * Stops a single service.
     *
     * @param service The PiDome service to stop.
     */
    public void stopService(final PiDomeService service) {
        LOG.info("Request to stop service [{}]", service.getServiceName());
        if (this.services.containsKey(service)) {
            if (this.services.get(service).deploymentID() != null && !this.services.get(service).deploymentID().isEmpty()) {
                VertXHandler.getInstance().getVertX().undeploy(this.services.get(service).deploymentID(), res -> {
                    if (res.succeeded()) {
                        this.services.remove(service);
                        LOG.info("[{}] has been undeployed successfuly", service.getServiceName());
                    } else {
                        LOG.error("Undeploying of [{}] failed, [{}]", service.getServiceName(), res.cause());
                    }
                    stopLatch.countDown();
                });
            } else {
                LOG.warn("Requesting to undeploy a service that has not been deployed [{}]", service.getServiceName());
            }
        } else {
            LOG.warn("Requesting to stop a service that has not been registered [{}]", service.getServiceName());
        }
    }

    /**
     * Returns the requested service.
     *
     * @param <T> The service.
     * @param service The enum of the service to return.
     * @return The service requested in an Optional.
     */
    @SuppressWarnings("unchecked")
    public <T extends AbstractService> Optional<T> getService(final Class<T> service) {
        for (AbstractService abstractService : services.values()) {
            LOG.trace("service iteration [{}] value requested [{}]", abstractService.getClass(), service);
            if (abstractService.getClass().equals(service)) {
                return Optional.ofNullable((T) abstractService);
            }
        }
        return Optional.empty();
    }

    /**
     * Used when booted for the very first time, or when someone is resetting
     * the full server.
     */
    private void firstBoot() {
        startService(PiDomeService.WEBSERVICE);
    }

}
