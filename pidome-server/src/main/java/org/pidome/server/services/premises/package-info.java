/**
 * Premises service.
 * <p>
 * Provides services giving the ability to interact with premises and geolocation.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.premises;
