/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Base class for handling events from and to the message bus.
 *
 * @author John Sirach
 * @param <T> The Object type being transfered over the bus.
 */
public abstract class EventBusMessageHandler<T> {

    /**
     * The logger.
     */
    private static final Logger LOG = LogManager.getLogger(EventBusMessageHandler.class);

    /**
     * The event address.
     */
    private final EventAddress address;

    /**
     * The consumer for handling the messages.
     */
    private Optional<MessageConsumer<T>> eventHandler = Optional.empty();

    /**
     * Constructor holding the address to listen for.
     *
     * @param eventsAddress The address.
     */
    public EventBusMessageHandler(final EventAddress eventsAddress) {
        this.address = eventsAddress;
    }

    /**
     * Returns the address.
     *
     * @return The events address.
     */
    protected final EventAddress getAddress() {
        return this.address;
    }

    /**
     * Creates the event service based on the given message bus.
     *
     * @param bus The bus to register on.
     * @param hndlr The message handler.
     * @throws org.pidome.server.services.events.ConsumerMissingException When
     * the consumer is not created.
     */
    public final void createEventHandler(final EventBus bus, final Handler<Message<T>> hndlr) throws ConsumerMissingException {
        eventHandler = Optional.ofNullable(bus.consumer(this.address.getAddress(), hndlr));
        eventHandler.orElseThrow(() -> new ConsumerMissingException("Consumer not created")).completionHandler(res -> {
            if (res.succeeded()) {
                LOG.info("The handler registration of [{}] has reached all nodes", hndlr);
            } else {
                LOG.error("Registration of [{}] failed!", hndlr);
            }
        });
    }

    /**
     * Unregisters the event handler from the bus.
     *
     * @throws org.pidome.server.services.events.ConsumerMissingException When
     * unregistering the event handler is not possible because it does not exist
     */
    public final void unregister() throws ConsumerMissingException {
        eventHandler.orElseThrow(() -> new ConsumerMissingException("Consumer not unregistered")).unregister(res -> {
            if (res.succeeded()) {
                LOG.info("The handler de-registration of [{}] has reached all nodes");
            } else {
                LOG.error("Registration of [{}] failed!");
            }
        });
    }

    /**
     * Handle the item.
     *
     * @param item The item to be handled.
     */
    public abstract void handle(T item);

}
