/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api;

import java.util.Optional;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;

/**
 * Base class for endpoints to implement.
 *
 * @author John
 */
public class ApiControllerResource {

    /**
     * Private constructor as this class needs to be extended.
     */
    protected ApiControllerResource() {
        /// Private constructor.
    }

    /**
     * Returns the service requested.
     *
     * @param <T> The service class to retrieve.
     * @param service The service expected.
     * @return The service encapsulated in an optional.
     */
    public final <T extends AbstractService> Optional<T> getService(final Class<T> service) {
        return ServiceHandler.getInstance().getService(service);
    }

}
