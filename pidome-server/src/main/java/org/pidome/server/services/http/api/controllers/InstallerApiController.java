/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import io.vertx.core.http.HttpServerResponse;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.installer.InstallerException;
import org.pidome.server.services.installer.InstallerService;
import org.pidome.server.system.installer.repositories.RepositoryContainer;

/**
 * Controller for all installation related REST actions.
 *
 * @author John
 */
@Path("installation")
@Produces("application/json")
@Consumes("application/json")
public class InstallerApiController extends ApiControllerResource {

    /**
     * Controller logger.
     */
    private static final Logger LOG = LogManager.getLogger(InstallerApiController.class);

    /**
     * No containers found literal.
     */
    private static final String NO_REPOSITORY_CONTAINERS = "Unable to retrieve repository containers from the system";

    /**
     * Returns a list of known repository containers.
     *
     * Repository containers are collections of repositories which are grouped
     * together.This makes it easier for publishers to group multiple
     * repositories.
     *
     * This for example to provide snapshot and release repositories. Or
     * multiple repositories when packages are available on multiple locations.
     *
     * @return Returns a list of repository containers.
     * @throws HttpStatusCodeException On any server error.
     */
    @GET
    @Path("repositories")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When endpoint responds correctly."),
        @ResponseCode(code = ApiResponseCode.HTTP_403, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When a server error occurs.")
    })
    public final List<RepositoryContainer> getRepositoryContainers() throws HttpStatusCodeException {
        try {
            return ServiceHandler.getInstance().getService(InstallerService.class)
                    .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Not available."))
                    .getRepositoryContainers();
        } catch (InstallerException ex) {
            LOG.error(NO_REPOSITORY_CONTAINERS, ex);
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Server error", ex);
        }
    }

    /**
     * Delete a repository container from the system.Completely deletes a
     * container from the system.All linked repositories will also be removed.
     *
     * If there are any modules which depend on this repository they will stay
     * and become orphaned. Removing or updating these is a manual action.
     *
     * @param id The id of the container to delete.
     * @param response The response object.
     * @throws HttpStatusCodeException When there is an error, see response code
     * list for details.
     */
    @DELETE
    @Path("repositories/{id}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When deletion was succesful"),
        @ResponseCode(code = ApiResponseCode.HTTP_401, condition = "When not allowed to delete"),
        @ResponseCode(code = ApiResponseCode.HTTP_404, condition = "When the repository is not found"),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When a system error occurs.")
    })
    public final void deleteRepositoryContainer(final @PathParam("id") Integer id, final @Context HttpServerResponse response) throws HttpStatusCodeException {
        try {
            InstallerService service = ServiceHandler.getInstance().getService(InstallerService.class)
                    .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Not available."));
            RepositoryContainer container = null;
            try {
                container = service.getRepositoryContainerById(id);
                response.setStatusCode(ApiResponseCode.HTTP_200);
            } catch (InstallerException ex) {
                LOG.error(NO_REPOSITORY_CONTAINERS, ex);
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Container not found", ex);
            }
            if (container != null) {
                service.deleteRepositoryContainer(container);
            } else {
                response.setStatusCode(ApiResponseCode.HTTP_404);
            }
            response.end();
        } catch (InstallerException ex) {
            LOG.error(NO_REPOSITORY_CONTAINERS, ex);
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Server error", ex);
        }
    }

    /**
     * Delete a repository container from the system.
     *
     * Completely deletes a container from the system. All linked repositories
     * will also be removed.
     *
     * If there are any modules which depend on this repository they will stay
     * and become orphaned. Removing or updating these is a manual action.
     *
     * @param container The message received by the broadcast.
     * @throws HttpStatusCodeException When there is an error, see response code
     * list for details.
     * @return The repository container updated.
     */
    @PUT
    @Path("repositories")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.HTTP_200, condition = "When an update was succesful"),
        @ResponseCode(code = ApiResponseCode.HTTP_401, condition = "Whe not allowed to update"),
        @ResponseCode(code = ApiResponseCode.HTTP_500, condition = "When a system error occurs.")
    })
    public final RepositoryContainer updateRepositoryContainer(final RepositoryContainer container) throws HttpStatusCodeException {
        try {
            ServiceHandler.getInstance().getService(InstallerService.class)
                    .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Not available."))
                    .updateRepositoryContainer(container);
            return container;
        } catch (InstallerException ex) {
            LOG.error(NO_REPOSITORY_CONTAINERS, ex);
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Server error", ex);
        }
    }

}
