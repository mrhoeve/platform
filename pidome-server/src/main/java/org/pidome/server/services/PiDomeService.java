/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services;

import java.util.Arrays;
import java.util.List;
import org.pidome.server.services.authentication.AuthenticationService;
import org.pidome.server.services.cluster.ClusterService;
import org.pidome.server.services.events.EventBusService;
import org.pidome.server.services.hardware.HardwareService;
import org.pidome.server.services.http.HttpService;
import org.pidome.server.services.installer.InstallerService;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.broadcast.DiscoveryBroadcastService;
import org.pidome.server.services.premises.PremisesService;
import org.pidome.server.services.security.SecurityService;
import org.pidome.server.services.user.UserService;
import org.pidome.server.system.database.DatabaseService;

/**
 * The PiDome available services.
 *
 * These services are internal services exposed by PiDome. These are in the
 * range from the Web service to plugins and more.
 *
 * This interface exposes dependencies. This provides us the ability that if a
 * service is restarted, the depending services will follow.
 *
 * @author John Sirach
 */
public enum PiDomeService {

    /**
     * The network service.
     */
    NETWORK("PiDome network service", NetworkService.class, true),
    /**
     * The network service.
     */
    SECURITY("Security provider service", SecurityService.class, true, NETWORK),
    /**
     * The event bus service.
     */
    EVENTBUS("PiDome events bus", EventBusService.class, false, SECURITY, NETWORK),
    /**
     * The cluster service.
     */
    CLUSTER("PiDome cluster agent", ClusterService.class, true, SECURITY, NETWORK, EVENTBUS),
    /**
     * The database service.
     */
    DATABASESERVICE("PiDome database services", DatabaseService.class, true, SECURITY, NETWORK),
    /**
     * Service to provide installations and updates.
     */
    INSTALLERSERVICE("Service for installations and updates", InstallerService.class, true, CLUSTER, DATABASESERVICE),
    /**
     * Premises services.
     */
    PREMISESSERVICE("Premises services", PremisesService.class, true, DATABASESERVICE),
    /**
     * The user service responsible to provide user based services.
     */
    USERSERVICE("PiDome user services", UserService.class, true, DATABASESERVICE, PREMISESSERVICE),
    /**
     * Service responsible for authentication and authorization.
     */
    AUTHENTICATIONSERVICE("Service for authentication and authorization", AuthenticationService.class, true, SECURITY, NETWORK, USERSERVICE, DATABASESERVICE),
    /**
     * The webservice.
     */
    WEBSERVICE("PiDome Web services", HttpService.class, true, NETWORK, AUTHENTICATIONSERVICE),
    /**
     * The service for controlling and maintaining connections to and from
     * PiDome.
     */
    HARDWARESERVICE("PiDome hardware service", HardwareService.class, false, DATABASESERVICE, INSTALLERSERVICE),
    /**
     * Service to provide server discovery.
     */
    DISCOVERYSERVICE("Server discovery service", DiscoveryBroadcastService.class, false, SECURITY, NETWORK);
    /**
     * The service name.
     */
    private final String name;

    /**
     * The service classpath.
     */
    private final Class<? extends AbstractService> clazz;

    /**
     * If this service can block startup and/or shutdown threads.
     */
    private final boolean blocking;

    /**
     * List of services that this service is depending on.
     */
    private final List<PiDomeService> dependencies;

    /**
     * Enum constructor.
     *
     * @param serviceName The service name.
     * @param serviceClass The service classpath.
     * @param isBlocking If the service blocks depending services until it is
     * started.
     * @param serviceDependencies The dependencies on which this service
     * depends.
     */
    PiDomeService(final String serviceName, final Class<? extends AbstractService> serviceClass, final boolean isBlocking, final PiDomeService... serviceDependencies) {
        this.name = serviceName;
        this.clazz = serviceClass;
        this.blocking = isBlocking;
        this.dependencies = Arrays.asList(serviceDependencies);
    }

    /**
     * Returns the service name.
     *
     * @return The service name.
     */
    public final String getServiceName() {
        return this.name;
    }

    /**
     * Returns the service classPath.
     *
     * @return The service classPath.
     */
    public final Class<? extends AbstractService> getServiceClass() {
        return this.clazz;
    }

    /**
     * If a service can be a thread blocking service or not.
     *
     * @return true when configured as a blocking service.
     */
    public final boolean isBlocking() {
        return blocking;
    }

    /**
     * Returns the list of services this service depends on.
     *
     * @return list of services.
     */
    public final List<PiDomeService> getDependencies() {
        return this.dependencies;
    }

}
