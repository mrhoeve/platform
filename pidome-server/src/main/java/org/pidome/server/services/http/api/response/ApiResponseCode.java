/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

/**
 * Collection of used API response codes.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class ApiResponseCode {

    /**
     * Non instantiatable constructor, utility class.
     */
    private ApiResponseCode() {
    }

    /**
     * OK.
     */
    public static final int HTTP_200 = 200;
    /**
     * Created.
     */
    public static final int HTTP_201 = 201;
    /**
     * OK, but no content returned.
     */
    public static final int HTTP_204 = 204;

    /**
     * Bad request.
     */
    public static final int HTTP_400 = 400;
    /**
     * Unauthorized.
     */
    public static final int HTTP_401 = 401;
    /**
     * Forbidden.
     */
    public static final int HTTP_403 = 403;
    /**
     * Not found.
     */
    public static final int HTTP_404 = 404;
    /**
     * Method not allowed.
     */
    public static final int HTTP_405 = 405;
    /**
     * Not acceptable.
     */
    public static final int HTTP_406 = 406;
    /**
     * Conflict.
     */
    public static final int HTTP_409 = 409;
    /**
     * Gone.
     */
    public static final int HTTP_410 = 410;

    /**
     * A non recoverable server side error.
     */
    public static final int HTTP_500 = 500;

    /**
     * Service not available.
     */
    public static final int HTTP_503 = 503;

}
