/**
 * Network services.
 * <p>
 * Central service to provide network related services and provides networking awareness and related events.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.network;
