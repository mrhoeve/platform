/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.auth;

import org.pidome.server.entities.auth.QrData;
import org.pidome.tools.utilities.NumberUtils;

/**
 * PiDome own QR scheme.
 *
 * @author John Sirach
 */
public final class PiDomeQRSchema {

    /**
     * QR version.
     */
    private static final String VERSION = "VERSION:1.0";
    /**
     * QR start.
     */
    private static final String BEGIN = "BEGIN:PIDOMECONNECTQR";
    /**
     * QR end.
     */
    private static final String END = "END:PIDOMECONNECTQR";
    /**
     * Line feed.
     */
    private static final String LINE_FEED = "\n";
    /**
     * Name of the user identifier.
     */
    private static final String USER_ID = "N";
    /**
     * Remote ip address identifier.
     */
    private static final String REMOTE_IP = "R";
    /**
     * Phone unique id identifier.
     */
    private static final String PHONE_UNIQUE = "P";
    /**
     * Server ip address identifier.
     */
    private static final String SERVER_IP = "S";
    /**
     * The TTL of the QR image identifier.
     */
    private static final String TTL_LENGTH = "TTL";
    /**
     * Random UUID identifier.
     */
    private static final String UUID = "U";

    /**
     * The life time of the QR code.
     */
    private static final String TTL = "10";

    /**
     * The QR data to include in the image.
     */
    private final QrData qrData;

    /**
     * Constructor.
     *
     * @param data The QR data to include in the image.
     */
    public PiDomeQRSchema(final QrData data) {
        super();
        this.qrData = data;
    }

    /**
     * Generates the string to encode.
     *
     * @return The generated string.
     */
    public String generateString() {
        StringBuilder sb = new StringBuilder();
        sb.append(BEGIN).append(LINE_FEED);
        sb.append(VERSION).append(LINE_FEED);
        if (this.qrData.getUid() != NumberUtils.LONG_ONE) {
            sb.append(USER_ID).append(":").append(this.qrData.getUid());
        }
        if (this.qrData.getRemoteIp() != null) {
            sb.append(LINE_FEED).append(REMOTE_IP).append(":").append(this.qrData.getRemoteIp());
        }
        if (this.qrData.getPhoneUniqueId() != null) {
            sb.append(LINE_FEED).append(PHONE_UNIQUE).append(":").append(this.qrData.getPhoneUniqueId());
        }
        if (this.qrData.getServerIp() != null) {
            sb.append(LINE_FEED).append(SERVER_IP).append(":").append(this.qrData.getServerIp());
        }
        if (this.qrData.getUuId() != null) {
            sb.append(LINE_FEED).append(UUID).append(":").append(this.qrData.getUuId());
        }
        if (TTL != null) {
            sb.append(LINE_FEED).append(TTL_LENGTH).append(":").append(TTL);
        }
        sb.append(LINE_FEED).append(END);
        return sb.toString();
    }

    /**
     * @return the lifeTime
     */
    public static String getTTL() {
        return TTL;
    }

}
