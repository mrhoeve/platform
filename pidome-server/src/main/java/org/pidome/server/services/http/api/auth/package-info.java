/**
 * QR code providers and schema's.
 * <p>
 * Package for supplying classes and methods to be able to handle QR codes.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.http.api.auth;
