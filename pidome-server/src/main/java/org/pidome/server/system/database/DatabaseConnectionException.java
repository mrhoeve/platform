/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

/**
 * When the pool can not be created.
 *
 * @author John
 */
public class DatabaseConnectionException extends Exception {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DatabaseConnectionException</code>
     * without detail message.
     */
    public DatabaseConnectionException() {
        //Default constructor.
    }

    /**
     * Constructs an instance of <code>DatabaseConnectionException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public DatabaseConnectionException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>DatabaseConnectionException</code> with
     * the specified cause.
     *
     * @param thr the cause.
     */
    public DatabaseConnectionException(final Throwable thr) {
        super(thr);
    }

}
