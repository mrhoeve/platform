/**
 * Serial peripherals.
 * <p>
 * Provides means and methods to connect PiDome to Serial peripheral interfaces
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.hardware.serial;
