/**
 * PiDome compatible low level connectable hardware.
 * <p>
 * Package for handling devices connected to the hardware of the host system.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.hardware;
