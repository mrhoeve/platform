/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.system.hardware.peripherals.Peripheral;

/**
 * A base class to ensure compatibility with hardware methods.
 *
 * A hardware component is OS and hardware specific. A component on windows will
 * not always be available on other platforms like raspberry pi pins are not
 * available on for example desktops, or at least not in the same form.
 *
 * @author John Sirach
 */
public abstract class HardwareComponent {

    /**
     * The hardware interface of the component.
     */
    public enum Interface {

        /**
         * Pure serial COM interface.
         */
        SERIAL,
        /**
         * USB interface (COM,HID..).
         */
        USB,
        /**
         * Parallel interface.
         */
        LPT,
        /**
         * Network interface.
         */
        NETWORK,
        /**
         * The host the software is running on.
         */
        SERVER;

    }

    /**
     * The interface of this hardware component.
     */
    private final Interface identifyingInterface;

    /**
     * The hardware root.
     */
    private HardwareMutationListener hardwareRoot;

    /**
     * Map of connected peripherals to the hardware component.
     */
    private final List<Peripheral<HardwareDriver>> connectedPeripherals = Collections.synchronizedList(new ArrayList<>());

    /**
     * Identifying constructor.
     *
     * @param componentInterface The hardware interface.
     * @param root The root of it all.
     * @throws UnsupportedOperationException When the hardware component is
     * unsupported on the currentOperationg System.
     */
    public HardwareComponent(final Interface componentInterface, final HardwareMutationListener root) throws UnsupportedOperationException {
        hardwareRoot = root;
        this.identifyingInterface = componentInterface;
    }

    /**
     * De-registers the hardware root.
     */
    @SuppressWarnings("PMD.NullAssignment")
    public final void deregisterRoot() {
        /// Remove pointer to application long living class.
        hardwareRoot = null;
    }

    /**
     * Starts a hardware interface.
     */
    public abstract void start();

    /**
     * Stops a hardware interface.
     */
    public abstract void stop();

    /**
     * Perform auto discovery of devices on the attached hardware component.
     *
     * @throws UnsupportedOperationException When discovery is not supported.
     */
    public abstract void discover() throws UnsupportedOperationException;

    /**
     * @return the hardwareRoot
     */
    protected HardwareMutationListener getHardwareRoot() {
        return hardwareRoot;
    }

    /**
     * Returns the identifying interface.
     *
     * @return The interface identifying this component.
     */
    public Interface getIdentifyingInterface() {
        return this.identifyingInterface;
    }

    /**
     * Registers a peripheral in this hardware interface.
     *
     * @param peripheral The peripheral to register.
     */
    public final void registerPeripheral(final Peripheral<HardwareDriver> peripheral) {
        this.connectedPeripherals.add(peripheral);
    }

    /**
     * Removes a peripheral from this hardware interface.
     *
     * @param peripheral The peripheral to un-register.
     */
    public final void unRegisterPeripheral(final Peripheral<HardwareDriver> peripheral) {
        this.connectedPeripherals.remove(peripheral);
    }

    /**
     * Returns a list of known connected peripherals.
     *
     * The returned list contains both active and waiting peripherals.
     *
     * @return An unmodifiable list of connected peripherals.
     */
    public final List<Peripheral<? extends HardwareDriver>> getConnectedPeripherals() {
        return Collections.unmodifiableList(this.connectedPeripherals);
    }

    /**
     * Returns the peripheral connected to the given port.
     *
     * @param port The port to fetch the peripheral for.
     * @return An optional with possible a peripheral with a connected hardware
     * driver.
     */
    public final Optional<Peripheral<HardwareDriver>> getPeripheralByPort(final String port) {
        Iterator<Peripheral<HardwareDriver>> iterator = this.connectedPeripherals.iterator();
        while (iterator.hasNext()) {
            Peripheral<HardwareDriver> peripheral = iterator.next();
            if (peripheral.getPort().equals(port)) {
                return Optional.of(peripheral);
            }
        }
        return Optional.empty();
    }

    /**
     * Returns the peripheral identified by a given key.
     *
     * @param key The unique key of a peripheral.
     * @return An optional with possible a peripheral with a connected hardware
     * driver.
     */
    public final Optional<Peripheral<HardwareDriver>> getPeripheralByKey(final String key) {
        Iterator<Peripheral<HardwareDriver>> iterator = this.connectedPeripherals.iterator();
        while (iterator.hasNext()) {
            Peripheral<HardwareDriver> peripheral = iterator.next();
            if (peripheral.getDeviceKey().equals(key)) {
                return Optional.of(peripheral);
            }
        }
        return Optional.empty();
    }

    /**
     * Exposes this object in it's string representation.
     *
     * @return This object as a string
     */
    @Override
    public String toString() {
        return new StringBuilder(this.getClass().getSimpleName()).append("[")
                .append(this.identifyingInterface.toString()).append(":").append(this.connectedPeripherals.size()).append(":peripherals")
                .append("]").toString();
    }

}
