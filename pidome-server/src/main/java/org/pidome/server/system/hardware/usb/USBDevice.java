/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.usb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.Transport;
import org.pidome.server.system.hardware.peripherals.Peripheral;

/**
 * Creates an USB device.
 *
 * @author John Sirach
 * @param <D> The driver going to be expected.
 */
public abstract class USBDevice<D extends HardwareDriver> extends Peripheral<D> {

    /**
     * Usb device logger.
     */
    private static final Logger LOG = LogManager.getLogger(USBDevice.class);

    /**
     * The hardware interface where this device originates from.
     */
    private static final HardwareComponent.Interface INTERFACE = HardwareComponent.Interface.USB;

    /**
     * The USB device constructor.
     *
     * @param usedSubSystem The USB subsystem used.
     */
    public USBDevice(final Transport.SubSystem usedSubSystem) {
        super(usedSubSystem);
        LOG.info("Initializing new USB device with [{}] subsystem", usedSubSystem);
    }

    /**
     * Returns the USB hardware interface.
     *
     * @return The hardware interface.
     */
    @Override
    public final HardwareComponent.Interface getHardwareInterface() {
        return INTERFACE;
    }

}
