/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.repositories;

/**
 * repository container exception.
 *
 * @author John
 */
public class RepositoryContainerException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>RepositoryContainerException</code>
     * without detail message.
     */
    public RepositoryContainerException() {
    }

    /**
     * Constructs an instance of <code>RepositoryContainerException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public RepositoryContainerException(final String msg) {
        super(msg);
    }
}
