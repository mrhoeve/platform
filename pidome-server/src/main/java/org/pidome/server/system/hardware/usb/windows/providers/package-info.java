/**
 * Windows USB interface providers.
 * <p>
 * Contains classes to support in finding the required information for USB
 * connected devices. These can be connect ports or other kind of information
 * required for a supported USB device to be able to be configured within PiDome
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.hardware.usb.windows.providers;
