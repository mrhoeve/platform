/**
 * System repositories classes.
 * <p>
 * Supporting classes to be able to install from repositories.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.installer.repositories;
