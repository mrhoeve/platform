/**
 * Windows USB peripherals.
 * <p>
 * Provides means and methods to connect PiDome to USB devices on Windows OS.
 * Makes uses of the JNA platform.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.hardware.usb.windows;
