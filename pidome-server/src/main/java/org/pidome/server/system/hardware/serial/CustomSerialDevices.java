/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.serial;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.config.ConfigPropertiesException;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.config.SystemConfig.Type;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.HardwareMutationListener;
import org.pidome.tools.utilities.Serialization;

/**
 * Base device for serial interfaces which has been custom created by the user.
 *
 * @author John Sirach
 * @todo Refactor to raw serial devices as these devices are not serial using
 * USB but absolute serial ports.
 */
public final class CustomSerialDevices extends HardwareComponent {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(CustomSerialDevices.class);

    /**
     * Custom serial devices mapping.
     */
    private final Map<String, SerialDevice> deviceCollection = new HashMap<>();

    /**
     * Naming for the VID and PID for the custom serial device.
     */
    private static final String CUSTOM_DEVICE_VID_PID_NAME = "Custom";

    /**
     * Constructor for custom serial devices.
     *
     * @param root The root
     * @throws UnsupportedOperationException When the underlying system is
     * unsupported.
     */
    public CustomSerialDevices(final HardwareMutationListener root) throws UnsupportedOperationException {
        super(Interface.SERIAL, root);
    }

    /**
     * The discovery of predefined serial devices.
     *
     * @throws UnsupportedOperationException When the discovery is unsupported.
     */
    @Override
    public void discover() throws UnsupportedOperationException {
        try {
            List<String> custSerials = new ArrayList<>(Arrays.asList(
                    Files.list(new File(SystemConfig.getSetting(Type.SYSTEM, "server.conf.hardware.custserial", "")).toPath()).map(String::valueOf).toArray(size -> new String[size])
            ));
            custSerials.stream().map((fileName) -> new File(fileName)).filter((file) -> (file.exists())).forEachOrdered((file) -> {
                try {
                    CustomSerialDeviceParameters params = Serialization.getDefaultObjectMapper().readValue(file, CustomSerialDeviceParameters.class);
                    List<String> lines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
                    if (lines.size() == NumberUtils.INTEGER_ONE) {

                        SerialDevice serialDevice = new SerialDevice();
                        serialDevice.setDeviceKey(params.getSerialKey());
                        serialDevice.setDevicePort(params.getPortPath());
                        serialDevice.setVendorId(CUSTOM_DEVICE_VID_PID_NAME);
                        serialDevice.setProductId(CUSTOM_DEVICE_VID_PID_NAME);
                        serialDevice.setFriendlyName(params.getFriendlyName());

                        deviceCollection.put(params.getPortPath(), serialDevice);

                        //_fireDeviceEvent(serialDevice, HardwarePeripheralEvent.DEVICE_ADDED);
                    } else {
                        /// incorrect amount of lines
                        if (!file.delete()) {
                            LOG.error("Incorrect parameters save (lines) [{}], but entry could not be deleted.", file.getName());
                        } else {
                            LOG.error("Incorrect parameters save (lines) [{}], entry deleted.", file.getName());
                        }
                    }
                } catch (IOException e) {
                    /// unable to read file
                    if (file.delete()) {
                        LOG.error("Could not read settings file [{}], entry deleted.", file.getName(), e);
                    } else {
                        LOG.error("Could not read settings file [{}], entry could alsno not be deleted, try again or perform manually.", file.getName(), e);
                    }
                }
            });
        } catch (IOException ex) {
            throw new UnsupportedOperationException(ex);
        }
    }

    /*
    final synchronized void _fireDeviceEvent(SerialDevice serialDevice, String eventType) {
        LOG.debug("Event: {}", eventType);
        HardwarePeripheralEvent serialEvent = new HardwarePeripheralEvent(serialDevice, eventType);
        Iterator listeners = getListeners().iterator();
        while (listeners.hasNext()) {
            ((HardwareMutationListener) listeners.next()).deviceMutation(serialEvent);
        }
    }
     */
    public void createCustomSerialDevice(final CustomSerialDeviceParameters parameters) throws ConfigPropertiesException, IOException {
        LOG.info("Trying to save custom serial device: {}", parameters);
        String serialLocation = SystemConfig.getSetting(Type.SYSTEM, "server.conf.hardware.custserial", "");
        if (!serialLocation.isEmpty()) {
            File file = new File(serialLocation + parameters.getPortPath().substring(parameters.getPortPath().lastIndexOf(File.separator)) + 1);
            Serialization.getDefaultObjectMapper().writeValue(file, parameters);

            SerialDevice serialDevice = new SerialDevice();
            serialDevice.setDeviceKey(UUID.randomUUID().toString());
            serialDevice.setDevicePort(parameters.getPortPath());
            serialDevice.setVendorId(CUSTOM_DEVICE_VID_PID_NAME);
            serialDevice.setProductId(CUSTOM_DEVICE_VID_PID_NAME);
            serialDevice.setFriendlyName(parameters.getFriendlyName());

            deviceCollection.put(parameters.getPortPath(), serialDevice);

            //_fireDeviceEvent(serialDevice, HardwarePeripheralEvent.DEVICE_ADDED);
        }

    }

    /**
     * Starts the component.
     *
     * This component does not do a literal start. configuration is loaded on
     * load.
     *
     * @throws UnsupportedOperationException When start is unsupported because
     * of failire.
     */
    @Override
    public void start() throws UnsupportedOperationException {
        /// there is no start, as these devices are custom and loaded on demand so there are no listeners
    }

    /**
     * Stops the hardware custom serial devices component.
     *
     * @throws UnsupportedOperationException When stop is unsupported because of
     * failure.
     */
    @Override
    @SuppressWarnings("PMD.GuardLogStatement")
    public void stop() throws UnsupportedOperationException {
        ArrayList<String> stopSet = new ArrayList<>();
        deviceCollection.keySet().forEach(serialKey -> {
            LOG.info("Removing [{}] device, please wait, removing...", deviceCollection.get(serialKey).getFriendlyName());
            LOG.info("Removed [{}]", deviceCollection.get(serialKey).getFriendlyName());
            stopSet.add(serialKey);
        });
        stopSet.forEach((serialKey) -> {
            deviceCollection.remove(serialKey);
        });
    }

}
