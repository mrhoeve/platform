/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.usb.windows;

import com.sun.jna.WString;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.DBT;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.Win32Exception;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinReg;
import com.sun.jna.platform.win32.WinUser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import jssc.SerialPortList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.hardware.HardwareRoot.Mutation;
import org.pidome.server.system.hardware.Transport.SubSystem;
import org.pidome.server.system.hardware.usb.UnsupportedUsbTypeException;
import org.pidome.server.system.hardware.usb.UsbInterface;
import org.pidome.server.system.hardware.usb.UsbMutationListener;
import org.pidome.server.system.hardware.usb.windows.providers.RegistryEntryNotFoundException;
import org.pidome.server.system.hardware.usb.windows.providers.WindowsUsbProvider;
import org.pidome.server.system.hardware.usb.windows.providers.WindowsUsbRegistryProvider;

/**
 * Used for USB events on windows.
 *
 * @author John Sirach
 * @todo Generic serial devices and individual providers connected to the
 * hardware entry).
 */
public final class WindowsUsb implements WinUser.WindowProc, UsbInterface {

    /**
     * The logger for this class.
     */
    private static final Logger LOG = LogManager.getLogger(WindowsUsb.class);

    /**
     * Hidden window used on windows to be able to catch events.
     */
    private WinDef.HWND hWnd;

    /**
     * Handle used for device notifications.
     */
    private WinUser.HDEVNOTIFY hDevNotify;

    /**
     * The windows USB path.
     */
    private static final String WINDOWS_REG_USB_PATH = "System\\CurrentControlSet\\Enum\\USB\\";

    /**
     * Listeners listening for attaching and detaching usb devices.
     */
    private final List<UsbMutationListener> mutationListeners = Collections.synchronizedList(new ArrayList<>());

    /**
     * Constructor.
     */
    public WindowsUsb() {
        /// default constructor.
    }

    /**
     * Hooks on windows process to receive USB plugs.
     */
    @Override
    @SuppressWarnings("PMD.GuardLogStatement")
    public void startWatchdog() {
        stopWatchdog();
        // define new window class
        WString windowClass = new WString("PiDomeUsbMonitorWindowClass");
        WinDef.HMODULE hInst = Kernel32.INSTANCE.GetModuleHandle("");

        WinUser.WNDCLASSEX wClass = new WinUser.WNDCLASSEX();
        wClass.hInstance = hInst;
        wClass.lpfnWndProc = WindowsUsb.this;
        wClass.lpszClassName = windowClass.toString();

        // register window class
        User32.INSTANCE.RegisterClassEx(wClass);

        // create new window
        hWnd = User32.INSTANCE
                .CreateWindowEx(
                        User32.WS_EX_TOPMOST,
                        windowClass.toString(),
                        "My hidden helper window, used only to catch the windows USB events",
                        0, 0, 0, 0, 0, WinUser.HWND_MESSAGE, null, hInst, null);
        LOG.debug("Succesfully created windows USB monitor window: [{}]", hWnd.getPointer());
        /* this filters for all usb device classes */
        DBT.DEV_BROADCAST_DEVICEINTERFACE notificationFilter = new DBT.DEV_BROADCAST_DEVICEINTERFACE();
        notificationFilter.dbcc_devicetype = DBT.DBT_DEVTYP_DEVICEINTERFACE;
        notificationFilter.dbcc_classguid = DBT.GUID_DEVINTERFACE_USB_DEVICE;
        notificationFilter.dbcc_size = notificationFilter.size();

        hDevNotify = User32.INSTANCE.RegisterDeviceNotification(hWnd, notificationFilter, User32.DEVICE_NOTIFY_WINDOW_HANDLE);
        if (hDevNotify != null) {
            LOG.debug("Succesfully registered windows USB notification listener.");
            WinUser.MSG msg = new WinUser.MSG();
            while (User32.INSTANCE.GetMessage(msg, hWnd, 0, 0) != 0) {
                User32.INSTANCE.TranslateMessage(msg);
                User32.INSTANCE.DispatchMessage(msg);
            }
        } else {
            LOG.error("Failed to register the windows usb listener [{}]", getLastError());
            stopWatchdog();
        }
    }

    /**
     * Removes the OS device listener and removes the hidden window for
     * receiving the USB events.
     */
    @Override
    public void stopWatchdog() {
        if (this.hDevNotify != null) {
            User32.INSTANCE.UnregisterDeviceNotification(this.hDevNotify);
        }
        if (this.hWnd != null) {
            User32.INSTANCE.DestroyWindow(hWnd);
        }
    }

    /**
     * Returns if the service is accepting USB connections.
     *
     * @return true if the watchdog is running.
     */
    @Override
    public boolean watchDogRunning() {
        return this.hWnd != null && this.hDevNotify != null;
    }

    /**
     * Callback function for windows process.
     *
     * @param hwnd The receiving window.
     * @param uMsg The message
     * @param wParam Additional message information
     * @param lParam Additional message information
     * @return process result.
     */
    @Override
    public WinDef.LRESULT callback(final WinDef.HWND hwnd, final int uMsg, final WinDef.WPARAM wParam, final WinDef.LPARAM lParam) {
        switch (uMsg) {
            case WinUser.WM_DEVICECHANGE:
                onDeviceChange(wParam, lParam);
                return new WinDef.LRESULT(0);
            default:
                return User32.INSTANCE.DefWindowProc(hwnd, uMsg, wParam, lParam);
        }
    }

    /**
     * Checks if the broadcasted device change is an device add or removal.
     *
     * @param wParam Message information coming from a windows callback message
     * @param lParam Message information coming from a windows callback message
     */
    protected void onDeviceChange(final WinDef.WPARAM wParam, final WinDef.LPARAM lParam) {
        DBT.DEV_BROADCAST_DEVICEINTERFACE bdif = new DBT.DEV_BROADCAST_DEVICEINTERFACE(lParam.longValue());
        switch (wParam.intValue()) {
            case DBT.DBT_DEVICEARRIVAL:
                LOG.debug("DBT_DEVICEARRIVAL (USB device added): [{}]", bdif.getDbcc_name());
                registerDeviceFromBroadcast(bdif.getDbcc_name());
                break;
            case DBT.DBT_DEVICEREMOVECOMPLETE:
                LOG.debug("DBT_DEVICEREMOVECOMPLETE (USB removed): [{}]", bdif.getDbcc_name());
                fireUsbDeviceRemovedEvent(bdif.getDbcc_name());
                break;
            default:
            //// unhandled device change
        }
    }

    /**
     * Tries to register a broadcasted device for use.
     *
     * @param broadcastName The broadcast name as exposed by the windows USB
     * connect and disconnect events.
     */
    private void registerDeviceFromBroadcast(final String broadcastName) {
        final Matcher matcher = WindowsUsbRegistryProvider.BROADCAST_REG_KEY_PATTERN.matcher(broadcastName.trim());
        if (matcher.matches()) {
            registerDiscoveredUsbDevice(
                    matcher.group(WindowsUsbRegistryProvider.BROADCAST_REG_KEY_PATTERN_VID_PID),
                    matcher.group(WindowsUsbRegistryProvider.BROADCAST_REG_KEY_PATTERN_SERIAL),
                    WINDOWS_REG_USB_PATH
                    + matcher.group(WindowsUsbRegistryProvider.BROADCAST_REG_KEY_PATTERN_VID_PID)
                    + "\\"
                    + matcher.group(WindowsUsbRegistryProvider.BROADCAST_REG_KEY_PATTERN_SERIAL),
                    broadcastName
            );
        }
    }

    /**
     * Used for initial discovery of attached devices during startup.
     *
     * This method iterates the windows user registry USB enums.
     *
     */
    @Override
    public void discover() {
        LOG.info("Started Windows USB discover");
        List<String> knownPorts = getRegisteredComports();
        LOG.debug("Ports discovered [{}]", knownPorts);
        if (!knownPorts.isEmpty()) {
            final String[] usbVidPidArray = Advapi32Util.registryGetKeys(WinReg.HKEY_LOCAL_MACHINE, WINDOWS_REG_USB_PATH);
            for (String usbVidPid : usbVidPidArray) {
                LOG.debug("USB VID&PID key found to traverse [{}]", usbVidPid);
                try {
                    final String usbDevicePath = WINDOWS_REG_USB_PATH + usbVidPid + "\\";
                    LOG.trace("Gathering USB device for key at : {}", usbDevicePath);
                    String[] deviceSerials = Advapi32Util.registryGetKeys(WinReg.HKEY_LOCAL_MACHINE, usbDevicePath);
                    LOG.trace("Found device serials [{}]", (Object[]) deviceSerials);
                    for (String deviceSerial : deviceSerials) {
                        registerDiscoveredUsbDevice(usbVidPid, deviceSerial, usbDevicePath + deviceSerial, null);
                    }
                } catch (Win32Exception ex) {
                    if (LOG.isTraceEnabled()) {
                        LOG.trace("Unable to gather all requirements for device with register key [{}]", usbVidPid, ex);
                    } else {
                        LOG.debug("Unable to gather all requirements for device with register key [{}], [{}]", usbVidPid, ex.getMessage());
                    }
                }
            }
        } else {
            LOG.info("No in use com ports found, discovery aborted.");
        }
        LOG.info("Discovery of USB devices finished");
    }

    /**
     * Collects the parameters to determine the service and registers the
     * device.
     *
     * If there is a broadcast id available pass this, if not, one will be
     * constructed.
     *
     * @param usbVidPid The device's vendor and product id string.
     * @param deviceSerial The device's serial number.
     * @param completeDevicePath The path to this device.
     * @param broadcastId If available provide the broadcast id (hot plug
     * broadcasted name).
     */
    private void registerDiscoveredUsbDevice(final String usbVidPid, final String deviceSerial, final String completeDevicePath, final String broadcastId) {
        String deviceService = Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE, completeDevicePath, "Service");
        final String deviceId;
        if (broadcastId == null) {
            String container = Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE, completeDevicePath, "ContainerID");
            deviceId = new StringBuilder("\\\\?\\USB#")
                    .append(usbVidPid).append("#")
                    .append(deviceSerial).append("#")
                    .append(container)
                    .toString();
        } else {
            deviceId = broadcastId;
        }
        discoverDeviceByRegistryEntry(deviceService, deviceSerial, usbVidPid, completeDevicePath, deviceId);
    }

    /**
     * discovers a device by it's USB registry entry.
     *
     * @param service The service reported by the entry.
     * @param deviceSerial The device serial.
     * @param vidPidOriginalString The Vendor and Product id as single string.
     * @param registryStringPointerReference The original device path.
     * @param broadcastId The id being used by windows connect and disconnect
     * broadcasts.
     */
    private void discoverDeviceByRegistryEntry(final String service, final String deviceSerial, final String vidPidOriginalString, final String registryStringPointerReference, final String broadcastId) {
        try {
            WindowsUsbProvider.valueOf(service.toUpperCase(Locale.US)).getRegistryProvider().ifPresent(provider -> {
                try {
                    String deviceName = provider.getDeviceName(vidPidOriginalString, deviceSerial);
                    String devicePort = provider.getPort(vidPidOriginalString, deviceSerial);
                    Matcher vidPidMatcher = WindowsUsbRegistryProvider.REG_KEY_VID_PID_PATTERN.matcher(vidPidOriginalString.trim());
                    if (vidPidMatcher.matches()) {
                        fireUsbDeviceAddedEvent(deviceName,
                                deviceSerial,
                                vidPidMatcher.group(1),
                                vidPidMatcher.group(2),
                                broadcastId,
                                devicePort);
                    } else {
                        LOG.error("Unable to register device because of undetectable VID&PID identifier [{}]", registryStringPointerReference);
                    }
                } catch (Win32Exception | RegistryEntryNotFoundException ex) {
                    LOG.error("Unable to identify device at [{}]", registryStringPointerReference, ex);
                }
            });
        } catch (IllegalArgumentException ex) {
            if (LOG.isTraceEnabled()) {
                LOG.trace("Service type [{}] is not supported by PiDome (device found in path [{}])", service, registryStringPointerReference, ex);
            } else {
                LOG.debug("Service type [{}] is not supported by PiDome (device found in path [{}])", service, registryStringPointerReference);
            }
        }
    }

    /**
     * Retrieves a list of known com ports.
     *
     * @return List<String> List of com ports
     */
    private List<String> getRegisteredComports() {
        return Arrays.asList(SerialPortList.getPortNames());
    }

    /**
     * Adds an event listener for USB device changes.
     *
     * @param listener Listener
     */
    @Override
    public void addEventListener(final UsbMutationListener listener) {
        if (!mutationListeners.contains(listener)) {
            mutationListeners.add(listener);
            LOG.debug("Added listener: {}", listener.getClass().getName());
        }
    }

    /**
     * Removes a listener for USB device changes.
     *
     * @param listener Listener
     */
    @Override
    public void removeEventListener(final UsbMutationListener listener) {
        mutationListeners.remove(listener);
        LOG.debug("Removed listener: {}", listener.getClass().getName());
    }

    /**
     * This usb device event is used when a device disconnects.
     *
     * @param usbKey The key to uniquely identify the usb device on which the
     * event is registered.
     */
    private void fireUsbDeviceRemovedEvent(final String usbKey) {
        LOG.debug("USB device removed event: {}", usbKey);
        synchronized (mutationListeners) {
            Iterator<UsbMutationListener> listeners = mutationListeners.iterator();
            while (listeners.hasNext()) {
                try {
                    listeners.next().deviceMutation(Mutation.REMOVE, SubSystem.SERIAL, "Unknown", null, null, null, usbKey, null);
                } catch (UnsupportedUsbTypeException ex) {
                    LOG.warn("The removed USB device is unsupported [{}]", ex.getMessage());
                }
            }
        }
    }

    /**
     * Used for when a device is added.
     *
     * @param deviceName the device name.
     * @param usbSerial The vendor id.
     * @param vendorId The vendor id.
     * @param deviceId The device id.
     * @param usbKey The usb key.
     * @param devicePort The device port.
     */
    private void fireUsbDeviceAddedEvent(final String deviceName, final String usbSerial, final String vendorId, final String deviceId, final String usbKey, final String devicePort) {
        LOG.debug("USB device added event: {}", usbKey);
        synchronized (mutationListeners) {
            Iterator<UsbMutationListener> listeners = mutationListeners.iterator();
            while (listeners.hasNext()) {
                try {
                    listeners.next().deviceMutation(Mutation.ADD, SubSystem.SERIAL, usbSerial, deviceName, vendorId, deviceId, usbKey, devicePort);
                } catch (UnsupportedUsbTypeException ex) {
                    LOG.warn("The attached USB device is unsupported [{}]", ex.getMessage());
                }
            }
        }
    }

    /**
     * Gets the last error.
     *
     * @return the last error
     */
    private int getLastError() {
        int rc = Kernel32.INSTANCE.GetLastError();
        if (rc != 0) {
            LOG.error(rc);
        }
        return rc;
    }

}
