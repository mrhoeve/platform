/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system;

import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Log4J2LoggerFactory;
import io.vertx.core.Vertx;

/**
 * The Vert.x handler used for Vert.x based services.
 *
 * @author John
 */
public final class VertXHandler {

    /**
     * Vertx instance.
     */
    private final Vertx vertx;

    /**
     * Static instance.
     */
    private static VertXHandler handler;

    /**
     * Constructor. Private as it should not be instantiated.
     */
    private VertXHandler() {
        vertx = Vertx.vertx();
    }

    /**
     * Returns an Vertx instance.
     *
     * @return static VertX instance.
     */
    public static VertXHandler getInstance() {
        if (handler == null) {
            System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.Log4j2LogDelegateFactory");
            System.setProperty("vertx.disableFileCPResolving", "true");
            InternalLoggerFactory.setDefaultFactory(Log4J2LoggerFactory.INSTANCE);
            handler = new VertXHandler();
        }
        return handler;
    }

    /**
     * Returns the Vertx instance.
     *
     * @return The VertX instance.
     */
    public Vertx getVertX() {
        return this.vertx;
    }

}
