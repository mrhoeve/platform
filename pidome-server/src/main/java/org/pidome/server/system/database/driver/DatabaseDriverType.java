/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import org.pidome.tools.enums.ResourceType;

/**
 * Collection of available drivers.
 *
 * @author John
 */
public enum DatabaseDriverType {

    /**
     * SqLite database type.
     *
     * This driver is candidate to be deprecated.
     */
    @Deprecated
    SQLITE("Sqlite", "sqlite", ResourceType.FILESYSTEM, org.pidome.server.system.database.driver.Sqlite.class, true),
    /**
     * H2 file system database type.
     */
    H2("H2 on disk", "h2", ResourceType.FILESYSTEM, org.pidome.server.system.database.driver.Sqlite.class, true),
    /**
     * H2 file system database type.
     */
    H2_MEM("H2 in memory", "h2", ResourceType.MEMORY, org.pidome.server.system.database.driver.H2.class, true),
    /**
     * MySQL database type.
     */
    MYSQL("Mysql", "mysql", ResourceType.NETWORK, org.pidome.server.system.database.driver.Mysql.class, false);

    /**
     * The service name.
     */
    private final String name;

    /**
     * The JDBC identifier.
     */
    private final String jdbcIdentifier;

    /**
     * The service classpath.
     */
    private final Class<? extends DatabaseDriverInterface> clazz;

    /**
     * Marks the current driver active or not. Can be used for incubating
     * drivers.
     */
    private final boolean active;

    /**
     * The boolean if the driver type is based on local file system.
     */
    private final ResourceType resourceType;

    /**
     * Enum constructor.
     *
     * @param serviceName The service name.
     * @param identifier The jdbc driver as string.
     * @param type The resource type.
     * @param driverClazz The class used as driver.
     * @param isActive If the driver should be active.
     */
    DatabaseDriverType(final String serviceName, final String identifier, final ResourceType type, final Class<? extends DatabaseDriverInterface> driverClazz, final boolean isActive) {
        this.name = serviceName;
        this.jdbcIdentifier = identifier;
        this.clazz = driverClazz;
        this.active = isActive;
        this.resourceType = type;
    }

    /**
     * Returns the service name.
     *
     * @return The service name.
     */
    public final String getDriverName() {
        return this.name;
    }

    /**
     * Returns the resource type.
     *
     * @return The resource type.
     */
    public final ResourceType getResourceType() {
        return this.resourceType;
    }

    /**
     * Returns the service classPath.
     *
     * @return The service classPath.
     */
    public final Class<? extends DatabaseDriverInterface> getDriverClass() {
        return this.clazz;
    }

    /**
     * Returns if a driver is active or not.
     *
     * @return If active or not.
     */
    public final boolean isActive() {
        return this.active;
    }

    /**
     * The JDBC identifier.
     *
     * @return the jdbcIdentifier
     */
    public String getJdbcIdentifier() {
        return jdbcIdentifier;
    }

}
