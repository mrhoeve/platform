/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb.windows.providers;

import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import java.util.regex.Matcher;
import org.pidome.server.system.hardware.Transport;

/**
 * Provider for FTDI based service.
 *
 * Rule of thumb, the device key used by FTDI references is the FTDI serial.
 *
 * Ps. I have no clue why there is an A added at the end of the serial when
 * looking for values in the FTDIBUS node. If someone knows email me and if
 * possible how to identify if there is a registry setting with FTDI drivers to
 * identify if an A must be added or not.
 *
 * @author johns
 */
public final class FtdiProvider implements WindowsUsbRegistryProvider {

    /**
     * The location of the FTDI bus.
     *
     * For example:
     * SYSTEM\CurrentControlSet\Enum\FTDIBUS\VID_0403+PID_6015+DO3HR4O5A\0000
     */
    private static final String FTDI_BUS_LOCATION = "SYSTEM\\CurrentControlSet\\Enum\\FTDIBUS\\VID_[VID_NUMBER]+PID_[PID_NUMBER]+[FTDI_SERIAL]A\\0000";

    /**
     * @inheritDoc
     */
    @Override
    public Transport.SubSystem getDeviceTransportType(final String originalVidPidIdentifier, final String deviceKey) throws RegistryEntryNotFoundException {
        return Transport.SubSystem.SERIAL;
    }

    /**
     * @inheritDoc
     */
    @Override
    public String getDeviceName(final String originalVidPidIdentifier, final String deviceKey) throws RegistryEntryNotFoundException {
        return "FTDI: " + Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE,
                createFtdiBusRegistryPath(originalVidPidIdentifier, deviceKey),
                "FriendlyName");
    }

    /**
     * @inheritDoc
     */
    @Override
    public String getPort(final String originalVidPidIdentifier, final String deviceKey) throws RegistryEntryNotFoundException {
        return Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE,
                createFtdiBusRegistryPath(originalVidPidIdentifier, deviceKey) + "\\Device Parameters",
                "PortName");
    }

    /**
     * Method to create the path to an FTDI device.
     *
     * @param originalVidPidIdentifier The original found vid and pid string as
     * found in the registry.
     * @param deviceKey The FTDI serial string.
     * @return The path to the FTDI device identified with vid and pid string
     * with the given serial.
     * @throws RegistryEntryNotFoundException When a path can not be constructed
     * resulting in an exception in other methods depending on this method.
     */
    private String createFtdiBusRegistryPath(final String originalVidPidIdentifier, final String deviceKey) throws RegistryEntryNotFoundException {
        Matcher m = WindowsUsbRegistryProvider.REG_KEY_VID_PID_PATTERN.matcher(originalVidPidIdentifier.trim());
        if (m.matches()) {
            return FTDI_BUS_LOCATION.replace("[VID_NUMBER]", m.group(1))
                    .replace("[PID_NUMBER]", m.group(2))
                    .replace("[FTDI_SERIAL]", deviceKey);
        } else {
            throw new RegistryEntryNotFoundException("No match found in regular expression to construct FTDI path with supplied registry value [" + originalVidPidIdentifier + "]");
        }
    }

}
