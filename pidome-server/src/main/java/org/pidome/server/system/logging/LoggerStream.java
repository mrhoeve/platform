package org.pidome.server.system.logging;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

/**
 * Custom implementation for streaming out log output.
 *
 * @author John
 */
public final class LoggerStream extends OutputStream {

    /**
     * The logger.
     */
    private final Logger logger;
    /**
     * The log level applied to the log.
     */
    private final Level logLevel;

    /**
     * Constructor.
     *
     * @param streamLogger The logger to apply.
     * @param level The log level used.
     */
    public LoggerStream(final Logger streamLogger, final Level level) {
        super();
        this.logger = streamLogger;
        this.logLevel = level;
    }

    /**
     * Write the log entry.
     *
     * @param b Bytes to create string from to write.
     * @throws IOException When logger is unable to write.
     */
    @Override
    public void write(final byte[] b) throws IOException {
        String string = new String(b, "US-ASCII");
        if (!string.trim().isEmpty()) {
            logger.log(logLevel, string);
        }
    }

    /**
     * Write the log entry.
     *
     * @param b Bytes to write.
     * @param off write offset.
     * @param len write length.
     * @throws IOException When logger is unable to write.
     */
    @Override
    public void write(final byte[] b, final int off, final int len) throws IOException {
        String string = new String(b, off, len, "US-ASCII");
        if (!string.trim().isEmpty()) {
            logger.log(logLevel, string);
        }
    }

    /**
     * Write the log entry.
     *
     * @param b Integer to create char from to write.
     * @throws IOException When logger is unable to write.
     */
    @Override
    public void write(final int b) throws IOException {
        String string = String.valueOf((char) b);
        if (!string.trim().isEmpty()) {
            logger.log(logLevel, string);
        }
    }
}
