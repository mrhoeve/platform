/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import org.pidome.platform.hardware.driver.interfaces.BluetoothDriverInterface;
import org.pidome.platform.hardware.driver.interfaces.HardwareDriverInterface;
import org.pidome.platform.hardware.driver.interfaces.HidDriverInterface;
import org.pidome.platform.hardware.driver.interfaces.I2CDriverInterface;
import org.pidome.platform.hardware.driver.interfaces.SerialDriverInterface;

/**
 * Interface for the transport used.
 *
 * @author John Sirach
 */
public interface Transport {

    /**
     * The sub system used to communicate with the peripheral.
     */
    enum SubSystem {
        /**
         * An unknown subsystem.
         *
         * Used to inform an end user the driver is incorrectly configured.
         */
        UNKNOWN("Unknown", DummyDriverInterface.class),
        /**
         * A Server internally used communication channel.
         */
        SERVER("Server", ServerAsDeviceDriverInterface.class),
        /**
         * I2C Communication subsystem.
         */
        I2C("I2C", I2CDriverInterface.class),
        /**
         * Network based communication channel.
         */
        NETWORK("Network", NetworkDriverInterface.class),
        /**
         * Serial communication channel.
         */
        SERIAL("Serial", SerialDriverInterface.class),
        /**
         * USB HID communication channel.
         */
        HID("USB HID", HidDriverInterface.class),
        /**
         * Bluetooth communication channel.
         */
        BLUETOOTH("Bluetooth", BluetoothDriverInterface.class);

        /**
         * The description of the communication subsystem enum.
         */
        private final String description;

        /**
         * The implementation required for the specific transport.
         */
        private final Class<? extends HardwareDriverInterface> implementation;

        /**
         * The enum constructor.
         *
         * @param fieldDescription The description of the enum.
         * @param implementation The implementation of the subsystem.
         */
        SubSystem(final String fieldDescription, final Class<? extends HardwareDriverInterface> implementation) {
            this.description = fieldDescription;
            this.implementation = implementation;
        }

        /**
         * Returns the description of the enum.
         *
         * @return The enum description.
         */
        public String getDescription() {
            return description;
        }

        /**
         * Returns the implementation required for the transport type.
         *
         * @return An implementation.
         */
        public final Class<? extends HardwareDriverInterface> getImplementation() {
            return implementation;
        }

        /**
         * Returns the enum by the given name.
         *
         * @param enumName The name given to retrieve the enum for.
         * @return The enum found by it's name. If not found
         * <code>UNKNOWN</code> is returned.
         */
        public static SubSystem getByName(final String enumName) {
            for (SubSystem sub : SubSystem.values()) {
                if (sub.name().equals(enumName)) {
                    return sub;
                }
            }
            return SubSystem.UNKNOWN;
        }

    }

    /*
    public default SubSystem getTransportSubSystem() {
        return SubSystem.getByName(((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName());
    }
     */
}
