/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import com.zaxxer.hikari.HikariConfig;

/**
 * SqLite specific configuration.
 *
 * @author John
 */
public final class Sqlite implements DatabaseDriverInterface {

    /**
     * The SQLite driver class path.
     */
    private static final String DRIVER_CLASS_PATH = "org.sqlite.JDBC";

    /**
     * SQLite dialect.
     */
    private static final String DRIVER_DIALECT = "org.hibernate.dialect.SQLiteDialect";

    /**
     * Loads the specific driver class.
     *
     * @throws DatabaseConfigurationException When the driver class can not be
     * loaded.
     */
    @Override
    public void loadDriver() throws DatabaseConfigurationException {
        try {
            Class.forName(DRIVER_CLASS_PATH);
        } catch (ClassNotFoundException ex) {
            throw new DatabaseConfigurationException(ex);
        }
    }

    /**
     * Returns the dialect used.
     *
     * @return dialect.
     */
    @Override
    public String getDialect() {
        return DRIVER_DIALECT;
    }

    /**
     * Applies driver specific configuration.
     *
     * @param hikariConfig HikariCP configuration.
     * @return The adjusted HikariCP configuration specific for this driver.
     */
    @Override
    public HikariConfig appendConfiguration(final HikariConfig hikariConfig) {
        hikariConfig.addDataSourceProperty("characterEncoding", "utf8");
        hikariConfig.addDataSourceProperty("useUnicode", "true");
        hikariConfig.setMaximumPoolSize(1); /// SQLITE uses file locking, one connection waits for any lock to release, sigh....
        return hikariConfig;
    }

}
