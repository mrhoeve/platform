/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb.windows.providers;

/**
 *
 * @author johns
 */
public class RegistryEntryNotFoundException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>RegistryEntryNotFoundException</code>
     * without detail message.
     */
    public RegistryEntryNotFoundException() {
        /// Empty default constructor.
    }

    /**
     * Constructs an instance of <code>RegistryEntryNotFoundException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public RegistryEntryNotFoundException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>RegistryEntryNotFoundException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     * @param ex The original exception.
     */
    public RegistryEntryNotFoundException(final String msg, final Throwable ex) {
        super(msg, ex);
    }

}
