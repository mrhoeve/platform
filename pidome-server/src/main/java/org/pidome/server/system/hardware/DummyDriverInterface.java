/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import org.pidome.platform.hardware.driver.config.HardwareConfiguration;
import org.pidome.platform.hardware.driver.config.SerialDeviceConfiguration;
import org.pidome.platform.hardware.driver.interfaces.HardwareDriverInterface;

/**
 * This is used as a dummy interface.
 *
 * Temporary implementation.
 *
 * @author John Sirach
 */
public interface DummyDriverInterface extends HardwareDriverInterface, HardwareConfiguration<SerialDeviceConfiguration> {

}
