/**
 * Packages.
 * <p>
 * Class identifying packages used within the PiDome Server.
 *
 * This package includes the class GenericVersion which has been shamelessly
 * been copied from the aether project at:
 * https://github.com/apache/maven-resolver/blob/master/maven-resolver-util/src/main/java/org/eclipse/aether/util/version/GenericVersion.java.
 * This class is extremely useful in determining, ordering and identifying
 * package versions. If only this was made public in the original package.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.packages;
