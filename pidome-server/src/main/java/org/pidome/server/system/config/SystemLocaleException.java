/*
 * Copyright 2017 PiDome.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.config;

/**
 * Exception thrown when there is a locale error.
 *
 * @author John
 */
public final class SystemLocaleException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>SystemLocaleException</code> without
     * detail message.
     */
    public SystemLocaleException() {
        /// default constructor.
    }

    /**
     * Constructs an instance of <code>SystemLocaleException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public SystemLocaleException(final String msg) {
        super(msg);
    }
}
