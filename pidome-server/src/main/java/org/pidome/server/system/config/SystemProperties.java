/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Extended Properties class to load the requested properties file but also the
 * default properties file for loading the default configuration settings.
 *
 * By using this class as new Props(String filename, new Props(String
 * default_filename))); it will load the default_filename as the default
 * properties and filename to overwrite the defaults.
 *
 * When saving is called the default_filename will be left alone and filename
 * will be saved.
 *
 * @author John
 */
public final class SystemProperties extends Properties {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(SystemProperties.class);

    /**
     * The properties file in question.
     */
    private final String propertiesFile;

    /**
     * Loads the default properties with the overwriting properties.
     *
     * @param prefixDir The directory prefix.
     * @param fileName The file name of the file to load.
     * @param defaultProps If default SystemProperties are set load them here.
     * @throws IOException When the system properties file can not be loaded.
     */
    public SystemProperties(final String prefixDir, final String fileName, final SystemProperties defaultProps) throws IOException {
        super(defaultProps);
        propertiesFile = prefixDir + fileName + ".properties";
        LOG.info("Reading from [{}][{}]", System.getProperty("user.dir"), propertiesFile);
        File propsFile = new File(propertiesFile);
        if (!propsFile.exists() && !propsFile.createNewFile()) {
            throw new IOException("Unable to create new system properties file");
        }
        try (InputStream stream = new FileInputStream(propsFile)) {
            load(stream);
        }
    }

    /**
     * Loads a properties file.
     *
     * @param prefixDir Prefix the directory choice
     * @param fileName the filename to load.
     * @throws IOException thrown when the file can not be loaded.
     */
    public SystemProperties(final String prefixDir, final String fileName) throws IOException {
        this(prefixDir, fileName, false);
    }

    /**
     * Loads a properties file.
     *
     * @param prefixDir Prefix the directory choice.
     * @param fileName the filename to load.
     * @param fromJar set to true to load from local jar file.
     * @throws IOException thrown when the file can not be loaded.
     */
    @SuppressWarnings("PMD.UseProperClassLoader")
    public SystemProperties(final String prefixDir, final String fileName, final boolean fromJar) throws IOException {
        super();
        propertiesFile = prefixDir + fileName + ".properties";
        LOG.info("Reading from [{}]", propertiesFile);
        if (!fromJar) {
            try (InputStream stream = new FileInputStream(propertiesFile)) {
                load(stream);
            }
        } else {
            try (InputStream stream = this.getClass().getClassLoader().getResourceAsStream(propertiesFile)) {
                load(stream);
            }
        }
    }

    /**
     * Stores a properties file. comment may be null;
     *
     * @param comment The comment to set when saving the file.
     * @throws IOException When the file can not be saved.
     */
    public void store(final String comment) throws IOException {
        try (FileOutputStream stream = new FileOutputStream(propertiesFile)) {
            store(stream, comment);
        }
    }

    /**
     * Returns the hashcode.
     *
     * @return the hashcode.
     */
    @Override
    public synchronized int hashCode() {
        return super.hashCode() + propertiesFile.hashCode();
    }

    /**
     * Checks object's equality.
     *
     * @param o Object to check.
     * @return true when equal.
     */
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        /// If the same properties file is used we consider it equal.
        if (o instanceof SystemProperties && ((SystemProperties) o).propertiesFile.equals(propertiesFile)) {
            return true;
        }
        return super.equals(o);
    }

}
