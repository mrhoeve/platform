/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import com.zaxxer.hikari.HikariConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.tools.enums.ResourceType;

/**
 * Gathers and supplies a data source configuration.
 *
 * @author John
 */
public final class DatasourceConfiguration {

    /**
     * Threshold used for leak detection.
     */
    private static final int LEAK_THRESHOLD = 5000;

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(DatasourceConfiguration.class);

    /**
     * The configured driver type.
     */
    private DatabaseDriverType driverType;

    /**
     * The location of the database.
     */
    private String location;

    /**
     * The database name where the tables are stored.
     */
    private String databaseName;

    /**
     * The database access user.
     */
    private String user;

    /**
     * The database access password.
     */
    private String password;

    /**
     * The generated jdbcPath.
     */
    private String jdbcPath;

    /**
     * The spoken dialect.
     */
    private String dialect;

    /**
     * The created hikariConfig.
     */
    private HikariConfig hikariConfig;

    /**
     * Constructor.
     *
     * Gathers the information.
     *
     * @throws DatabaseConfigurationException When the configuration can not be
     * applied.
     */
    public DatasourceConfiguration() throws DatabaseConfigurationException {
        readConfig();
        applyConfig();
    }

    /**
     * The configured driver type.
     *
     * @return the driverType
     */
    public DatabaseDriverType getDriverType() {
        return driverType;
    }

    /**
     * The location of the database.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * The database name where the tables are stored.
     *
     * @return the databaseName
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * The database access user.
     *
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * The database access password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * The created hikariConfig.
     *
     * @return the hikariConfig
     */
    public HikariConfig getHikariConfig() {
        return hikariConfig;
    }

    /**
     * The generated jdbcPath.
     *
     * @return the jdbcPath
     */
    public String getJdbcPath() {
        return jdbcPath;
    }

    /**
     * Sets the dialect of the chosen driver.
     *
     * @param dialect The dialect.
     */
    public void setDialect(final String dialect) {
        this.dialect = dialect;
    }

    /**
     * Returns the dialect of the chosen driver.
     *
     * @return The dialect.
     */
    public String getDialect() {
        return this.dialect;
    }

    /**
     * Reads the configuration for the data source.
     *
     * @throws DatabaseConfigurationException When the configuration can not be
     * read.
     */
    private void readConfig() throws DatabaseConfigurationException {
        driverType = resolveDriverType();
        location = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.system.location", "");
        if (driverType.getResourceType().equals(ResourceType.FILESYSTEM)) {
            location = SystemConfig.getResourcePath("databases/") + location;
        }
        databaseName = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.system.name", "");
        user = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.system.user", "");
        password = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.system.pass", "");
        String jdbcAppend = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.JDBC_append", "");
        jdbcPath = new StringBuilder("jdbc:")
                .append(getDriverType().getJdbcIdentifier()).append(":")
                .append(location).append(":")
                .append(databaseName)
                .append(!jdbcAppend.isBlank() ? ";" + jdbcAppend : "")
                .toString();
        LOG.debug("Constructed JDBC path [{}]", jdbcPath);
    }

    /**
     * Applies the configuration read by <code>readConfig</code>.
     */
    private void applyConfig() {
        hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(getJdbcPath());
        hikariConfig.setUsername(getUser());
        hikariConfig.setPassword(getPassword());
        hikariConfig.setLeakDetectionThreshold(LEAK_THRESHOLD);
        hikariConfig.setPoolName("DBConnectionPool_PiDomeServer");
        hikariConfig.addDataSourceProperty("characterEncoding", "utf8");
        hikariConfig.addDataSourceProperty("useUnicode", "true");
        hikariConfig.setAutoCommit(false);
    }

    /**
     * Resolves the driver type.
     *
     * @return The driver type configured.
     * @throws DatabaseConfigurationException when the driver type can not be
     * gathered.
     */
    private DatabaseDriverType resolveDriverType() throws DatabaseConfigurationException {
        String configuredDriver = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.system.driver", "");
        if (configuredDriver.isEmpty()) {
            throw throwInvalidDriverException(configuredDriver);
        }
        for (DatabaseDriverType driver : DatabaseDriverType.values()) {
            if (driver.isActive() && configuredDriver.equalsIgnoreCase(driver.name())) {
                LOG.info("Resolved database driver [{}]", driver.getDriverName());
                return driver;
            }
        }
        throw throwInvalidDriverException(configuredDriver);
    }

    /**
     * Invalid driver exception builder.
     *
     * @param configuredDriver The given invalid driver.
     * @return Returns a <code>DatabaseConfigurationException</code> on purpose
     * meant to be thrown.
     */
    private DatabaseConfigurationException throwInvalidDriverException(final String configuredDriver) {
        StringBuilder valid = new StringBuilder("Invalid database configured [database.system.driver]: [")
                .append(configuredDriver)
                .append("]. Should be one of [");
        for (DatabaseDriverType driver : DatabaseDriverType.values()) {
            if (driver.isActive()) {
                valid.append(driver.getJdbcIdentifier()).append(",");
            }
        }
        return new DatabaseConfigurationException(valid.deleteCharAt(valid.length() - 1).append("]").toString());
    }

}
