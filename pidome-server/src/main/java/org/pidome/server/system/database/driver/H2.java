/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import com.zaxxer.hikari.HikariConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.Server;
import org.pidome.server.system.config.SystemConfig;

/**
 * H2 database loader.
 *
 * @author John Sirach
 */
@SuppressWarnings("CPD-START")
public final class H2 implements DatabaseDriverInterface {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(H2.class);

    /**
     * Embedded database tcp server for database console.
     *
     * Only available when ran in development mode.
     */
    private Server tcpServer;

    /**
     * Embedded database console.
     *
     * Only available when ran in development mode.
     */
    private Server webServer;

    /**
     * The SQLite driver class path.
     */
    private static final String DRIVER_CLASS_PATH = "org.h2.Driver";

    /**
     * SQLite dialect.
     */
    private static final String DRIVER_DIALECT = "org.hibernate.dialect.H2Dialect";

    /**
     * Loads the specific driver class.
     *
     * @throws DatabaseConfigurationException When the driver class can not be
     * loaded.
     */
    @Override
    public void loadDriver() throws DatabaseConfigurationException {
        try {
            Class.forName(DRIVER_CLASS_PATH);
        } catch (ClassNotFoundException ex) {
            throw new DatabaseConfigurationException(ex);
        }
    }

    /**
     * Returns the dialect used.
     *
     * @return dialect.
     */
    @Override
    public String getDialect() {
        return DRIVER_DIALECT;
    }

    /**
     * Applies driver specific configuration.
     *
     * @param hikariConfig HikariCP configuration.
     * @return The adjusted HikariCP configuration specific for this driver.
     */
    @Override
    public HikariConfig appendConfiguration(final HikariConfig hikariConfig) {
        return hikariConfig;
    }

    /**
     * Starts the embedded database console when in development mode.
     *
     * @param datasourceConfig The configuration.
     */
    @Override
    public void afterInit(final DatasourceConfiguration datasourceConfig) {
        if (SystemConfig.isDevMode()) {
            LOG.warn("Server in development mode, stating embedded database console");
            try {
                tcpServer = Server.createTcpServer().start();
                webServer = Server.createWebServer().start();
                LOG.warn("Database console server started on [{}]. use [jdbc:h2:{}/{}:{}] to connect in the console", webServer.getURL(), tcpServer.getURL(), datasourceConfig.getLocation(), datasourceConfig.getDatabaseName());
            } catch (Exception ex) {
                LOG.error("Could not start embedded database console server [{}], console not available", ex.getMessage(), ex);
            }
        }
    }

    /**
     * Stops the embedded database console.
     */
    @Override
    public void onShutdown() {
        if (webServer != null) {
            webServer.stop();
        }
        if (tcpServer != null) {
            tcpServer.stop();
        }
    }

}
