/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.packages;

/**
 * The type of component used for installation.
 *
 * @author John
 */
public enum ServerPackageType {

    /**
     * A driver package.
     */
    DRIVER("Hardware driver"),
    /**
     * A plugin package.
     */
    PLUGIN("Plugin"),
    /**
     * A device package.
     */
    DEVICE("Device"),
    /**
     * Mixed type package.
     */
    MIXED("Package possibly containing hardware drivers, plugins and/or devices");

    /**
     * Package type description.
     */
    private String description;

    /**
     * Constructor.
     *
     * @param description The description to set.
     */
    ServerPackageType(final String description) {
        this.description = description;
    }

    /**
     * Returns the description.
     *
     * @return The description of the enum.
     */
    public String getDescription() {
        return this.description;
    }

}
