/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import com.zaxxer.hikari.HikariConfig;

/**
 * Mysql driver proxy.
 *
 * @author John
 */
@SuppressWarnings("CPD-START")
public final class Mysql implements DatabaseDriverInterface {

    /**
     * Loads the mysql driver.
     *
     * @throws DatabaseConfigurationException When configuring the mysql driver
     * fails.
     */
    @Override
    public void loadDriver() throws DatabaseConfigurationException {
        throw new UnsupportedOperationException("MySQL not yet active");
    }

    /**
     * Returns the dialect used.
     *
     * @return dialect.
     */
    @Override
    public String getDialect() {
        throw new UnsupportedOperationException("MySQL not yet active");
    }

    /**
     * Appends configuration parameters in code which could be required specific
     * for PiDome.
     *
     * @param hikariConfig The original configuration.
     * @return Returns the supplied configuration.
     */
    @Override
    public HikariConfig appendConfiguration(final HikariConfig hikariConfig) {
        return hikariConfig;
    }

}
