/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.usb;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.HardwareMutationListener;
import org.pidome.server.system.hardware.HardwareRoot;
import org.pidome.server.system.hardware.Transport.SubSystem;
import org.pidome.server.system.hardware.usb.linux.LinuxUsb;
import org.pidome.server.system.hardware.usb.windows.WindowsUsb;

/**
 * Covering class for all USB actions.
 *
 * @author John Sirach
 */
public final class USBDevices extends HardwareComponent implements UsbMutationListener {

    /**
     * Hardware interface for USB compatible supported systems.
     */
    private final UsbInterface usbInterface;

    /**
     * Logger for usb devices.
     */
    private static final Logger LOG = LogManager.getLogger(USBDevices.class);

    /**
     * Constructor.
     *
     * The constructor finds the OS for the underlying subsystem.
     *
     * @param root The hardware root.
     * @throws UnsupportedOperationException When the underlying subsystem is
     * unsupported.
     */
    public USBDevices(final HardwareMutationListener root) throws UnsupportedOperationException {
        super(Interface.USB, root);
        LOG.info("Detected [{}:{}] USB", PlatformOs.getOs(), PlatformOs.getArch());
        switch (PlatformOs.getOs()) {
            case WINDOWS:
                switch (PlatformOs.getArch()) {
                    case ARCH_64:
                    case ARCH_86:
                        usbInterface = new WindowsUsb();
                        break;
                    default:
                        throw new UnsupportedOperationException(PlatformOs.getReportedOs() + " on " + PlatformOs.getReportedArch() + " is unsupported for realtime USB connection status");
                }
                break;
            case LINUX:
                switch (PlatformOs.getArch()) {
                    case ARCH_ARM:
                    case ARCH_64:
                        usbInterface = new LinuxUsb();
                        break;
                    default:
                        throw new UnsupportedOperationException(PlatformOs.getReportedOs() + " on " + PlatformOs.getReportedArch() + " is unsupported for realtime USB connection status");
                }
                break;
            default:
                throw new UnsupportedOperationException(PlatformOs.getReportedOs() + " is unsupported for realtime USB connection status");
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void deviceMutation(final HardwareRoot.Mutation mutationType, final SubSystem subSystem, final String serial, final String deviceName, final String vendorId, final String deviceId, final String usbKey, final String devicePort) throws UnsupportedUsbTypeException {
        USBDevice<HardwareDriver> usbDevice;
        LOG.debug("Performing mutation type [{}] on device with key [{}]", mutationType, usbKey);
        switch (mutationType) {
            case ADD:
                switch (subSystem) {
                    //case HID:
                    //    usbDevice = new UsbHidDevice();
                    //    break;
                    case SERIAL:
                        usbDevice = new UsbSerialDevice();
                        /*
                        SerialDeviceConfiguration config = new SerialDeviceConfiguration();
                        config.setPort(devicePort);
                        config.setName(deviceName);
                        config.setPath(devicePort);
                        PiDomeSerialDriver driver = new PiDomeSerialDriver();
                        usbDevice.setDriver(driver);
                        Future<Void> driverConfFuture = Future.future();
                        driver.configure(config, driverConfFuture.setHandler(handle -> {
                            if (handle.failed()) {
                                LOG.error("Configuration of [{}] failed due to [{}]", usbDevice.getFriendlyName(), handle.cause().getMessage());
                            } else if (handle.succeeded()) {
                                LOG.info("Configuration of [{}] succeeded", usbDevice.getFriendlyName());
                                Future<Void> driverStartFuture = Future.future();
                                usbDevice.getDriver().startDriver(driverStartFuture.setHandler(handleStart -> {
                                    if (handleStart.failed()) {
                                        LOG.error("Starting device [{}] with driver [{}] failed due to [{}]", usbDevice.getFriendlyName(), usbDevice.getDriver(), handleStart.cause());
                                    } else if (handleStart.succeeded()) {
                                        LOG.info("Driver [{}] on device [{}] started succesfully", usbDevice.getFriendlyName(), usbDevice.getDriver());
                                    }
                                }));
                            }
                        }));
                         */
                        break;
                    default:
                        throw new UnsupportedUsbTypeException("USB subsystem of type [" + subSystem + "] is currently unsupported");
                }
                usbDevice.setSerial(serial);
                usbDevice.setDeviceKey(usbKey);
                usbDevice.setDevicePort(devicePort);
                usbDevice.setVendorId(vendorId);
                usbDevice.setProductId(deviceId);
                usbDevice.setFriendlyName(deviceName);
                registerPeripheral(usbDevice);
                getHardwareRoot().hardwareMutation(usbDevice, HardwareRoot.Mutation.ADD);
                break;
            case REMOVE:
                this.getPeripheralByKey(usbKey).ifPresent(peripheralToRemove -> {
                    getHardwareRoot().hardwareMutation(peripheralToRemove, HardwareRoot.Mutation.REMOVE);
                    unRegisterPeripheral(peripheralToRemove);
                });
                break;
            default:
                throw new UnsupportedUsbTypeException("Unsupported mutation type for USB [" + mutationType + "]");
        }
    }

    /**
     * Returns if the usb watchdog is running for real time connects.
     *
     * @return true if the watchdog is running.
     */
    public boolean watchdogRunning() {
        return usbInterface.watchDogRunning();
    }

    /**
     * Prepares the USB implementation.
     *
     * @throws UnsupportedOperationException when the OS and ARCH combination is
     * unsupported.
     */
    private void prepare() throws UnsupportedOperationException {
        usbInterface.addEventListener(this);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void discover() throws UnsupportedOperationException {
        usbInterface.discover();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void start() {
        prepare();
        Thread usbThread = new Thread() {
            @Override
            public void run() {
                usbInterface.startWatchdog();
                LOG.info("[{}] USB watchdog started", PlatformOs.getOs());
            }
        };
        usbThread.setName(PlatformOs.getOs() + " USB Monitor");
        usbThread.start();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void stop() throws UnsupportedOperationException {
        usbInterface.stopWatchdog();
    }
}
