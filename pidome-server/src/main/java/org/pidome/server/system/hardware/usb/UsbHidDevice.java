/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb;

import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.system.hardware.Transport.SubSystem;

/**
 * An USB HID device.
 *
 * @author John Sirach
 */
public class UsbHidDevice extends USBDevice<HardwareDriver> {

    /**
     * USB HID device constructor.
     */
    public UsbHidDevice() {
        super(SubSystem.HID);
    }

}
