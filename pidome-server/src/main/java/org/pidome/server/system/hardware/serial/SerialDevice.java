/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.serial;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.Transport.SubSystem;
import org.pidome.server.system.hardware.peripherals.Peripheral;

/**
 * A device that communicates through the serial interface.
 *
 * @author John Sirach
 */
public class SerialDevice extends Peripheral<HardwareDriver> {

    /**
     * Logger for a serial device.
     */
    private static final Logger LOG = LogManager.getLogger(SerialDevice.class);

    /**
     * The hardware interface where this device originates from.
     */
    private static final HardwareComponent.Interface HARDWARE_INTERFACE_IMPL = HardwareComponent.Interface.SERIAL;

    /**
     * Serial device constructor.
     */
    public SerialDevice() {
        super(SubSystem.SERIAL);
        LOG.debug("New Serial device");
    }

    /**
     * Returns the USB hardware interface.
     *
     * @return The hardware interface.
     */
    @Override
    public final HardwareComponent.Interface getHardwareInterface() {
        return HARDWARE_INTERFACE_IMPL;
    }

}
