/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import com.zaxxer.hikari.HikariConfig;

/**
 * Interface for database connections.
 *
 * @author John
 */
public interface DatabaseDriverInterface {

    /**
     * Returns the data source of the driver.
     *
     * @throws DatabaseConfigurationException When a database driver can not be
     * loaded.
     */
    void loadDriver() throws DatabaseConfigurationException;

    /**
     * Returns the dialect used.
     *
     * @return The dialect as string.
     */
    String getDialect();

    /**
     * Modifier for a given HikariCP configuration.
     *
     * @param hikariConfig The HikariCP configuration to modify.
     * @return The modified HikariCP configuration.
     */
    HikariConfig appendConfiguration(HikariConfig hikariConfig);

    /**
     * Optionally executed after initialization.
     *
     * @param datasourceConfig The configuration.
     */
    default void afterInit(DatasourceConfiguration datasourceConfig) {
        /// Empty method.
    }

    /**
     * Optionally ran during server shutdown.
     */
    default void onShutdown() {
        /// Empty method.
    }

}
