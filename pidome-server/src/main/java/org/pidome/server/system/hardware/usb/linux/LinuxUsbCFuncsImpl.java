/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb.linux;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;
import org.pidome.server.system.hardware.usb.linux.LinuxUsbCFuncsImpl.LinuxCLib.FDSet;
import org.pidome.server.system.hardware.usb.linux.LinuxUsbCFuncsImpl.LinuxCLib.FDSetImpl;

/**
 * Mapping C to Java implementation.
 *
 * @author John Sirach
 */
public final class LinuxUsbCFuncsImpl {

    /**
     * Utility class, private constructor.
     */
    private LinuxUsbCFuncsImpl() {
        /// private constructor.
    }

    /**
     * New instance of the file descriptor set.
     *
     * @return New file descriptor set.
     */
    public static FDSet newFDSet() {
        return new FDSetImpl();
    }

    /**
     * Creates a file descriptor set.
     *
     * @param fd File descriptors.
     * @param set The set to create.
     */
    public static void FD_SET(final int fd, final LinuxCLib.FDSet set) {
        if (set == null) {
            return;
        }
        LinuxCLib.FDSetImpl p = (LinuxCLib.FDSetImpl) set;
        p.bits[fd / LinuxCLib.FDSetImpl.NFBBITS] |= 1 << (fd % LinuxCLib.FDSetImpl.NFBBITS);
    }

    /**
     * Explicit zero.
     *
     * @param set the set to set to explicit zero.
     */
    public static void FD_ZERO(final FDSet set) {
        if (set == null) {
            return;
        }
        FDSetImpl p = (FDSetImpl) set;
        java.util.Arrays.fill(p.bits, 0);
    }

    /**
     * Execute select method.
     *
     * @param nfds Number of file descriptors.
     * @param rfds Read descriptors.
     * @param wfds Write descriptors
     * @param efds Except descriptors.
     * @param timeout Wait timeout
     * @return int result.
     */
    public static int select(final int nfds, final FDSet rfds, final FDSet wfds, final FDSet efds, final TimeVal timeout) {
        LinuxCLib.timeval tout = null;
        if (timeout != null) {
            tout = new LinuxCLib.timeval(timeout);
        }

        int[] r = rfds != null ? ((FDSetImpl) rfds).bits : null;
        int[] w = wfds != null ? ((FDSetImpl) wfds).bits : null;
        int[] e = efds != null ? ((FDSetImpl) efds).bits : null;
        return LinuxCLib.INSTANCE.select(nfds, r, w, e, tout);
    }

    /**
     * Returning result.
     *
     * @param fd File descriptors.
     * @param set File descriptor set.
     * @return Boolean based on a filled set or not, filled=true.
     */
    public static boolean FD_ISSET(final int fd, final FDSet set) {
        if (set == null) {
            return false;
        }
        FDSetImpl p = (FDSetImpl) set;
        return (p.bits[fd / FDSetImpl.NFBBITS] & (1 << (fd % FDSetImpl.NFBBITS))) != 0;
    }

    /**
     * Mapping to time.h.
     */
    public static class TimeVal {

        /**
         * Seconds.
         */
        public long tv_sec;
        /**
         * Microseconds.
         */
        public long tv_usec;
    }

    /**
     * Interface to the LinuxCLib library.
     */
    public interface LinuxCLib extends Library {

        /**
         * Linux lib instance.
         */
        LinuxCLib INSTANCE = Native.load("c", LinuxCLib.class);

        /**
         * Mapping to select method.
         *
         * @param n Number of file descriptors.
         * @param read Read descriptors.
         * @param write Write descriptors
         * @param error Except descriptors.
         * @param timeout Wait timeout
         * @return int result.
         */
        int select(int n, int[] read, int[] write, int[] error, timeval timeout);

        /**
         * Mapping to timing structure.
         */
        class timeval extends Structure {

            /**
             * Seconds.
             */
            private NativeLong tv_sec;
            /**
             * Microseconds.
             */
            private NativeLong tv_usec;

            /**
             * Provide correct field order.
             *
             * @return The correct order.
             */
            @Override
            protected List<String> getFieldOrder() {
                return Arrays.asList(
                        "tv_sec",
                        "tv_usec"
                );
            }

            /**
             * Constructor.
             *
             * @param timeout The timeout to set.
             */
            public timeval(final TimeVal timeout) {
                tv_sec = new NativeLong(timeout.tv_sec);
                tv_usec = new NativeLong(timeout.tv_usec);
            }

            /**
             * @return the tv_sec
             */
            public NativeLong getTv_sec() {
                return tv_sec;
            }

            /**
             * @return the tv_usec
             */
            public NativeLong getTv_usec() {
                return tv_usec;
            }
        }

        /**
         * Implementation of FDSet.
         */
        class FDSetImpl extends FDSet {

            /**
             * Descriptor amount.
             */
            static final int FD_SET_SIZE = 1024;
            /**
             * Word value.
             */
            static final int NFBBITS = 32;
            /**
             * Bit size.
             */
            int[] bits = new int[(FD_SET_SIZE + NFBBITS - 1) / NFBBITS];

            @Override
            public String toString() {
                return String.format("%08X%08X", bits[0], bits[1]);
            }
        }

        /**
         * File descriptor mapping.
         */
        @SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
        abstract class FDSet {
        }

    }

}
