/**
 * System core providers.
 * <p>
 * Provides system core elements.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system;
