/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer;

import java.util.List;
import org.pidome.server.system.packages.ServerPackage;

/**
 * Update search result containing a list of of versions related to the
 * component given in the search.
 *
 * @author John
 */
public class UpdateSearchResult {

    /**
     * List of newer versions.
     */
    private List<String> newer;

    /**
     * List of older versions.
     */
    private List<String> older;

    /**
     * List of non compatible versions.
     */
    private List<String> incompatible;

    /**
     * The component for which this result is.
     */
    private ServerPackage checkPackage;

    /**
     * The repository where the search result originates from.
     */
    private String repository;

    /**
     * The repository container where the search result originates from.
     */
    private String repositoryContainer;

    /**
     * @return the newer
     */
    public List<String> getNewer() {
        return newer;
    }

    /**
     * @param newer the newer to set
     */
    public void setNewer(final List<String> newer) {
        this.newer = newer;
    }

    /**
     * @return the older
     */
    public List<String> getOlder() {
        return older;
    }

    /**
     * @param older the older to set
     */
    public void setOlder(final List<String> older) {
        this.older = older;
    }

    /**
     * @return the incompatible
     */
    public List<String> getIncompatible() {
        return incompatible;
    }

    /**
     * @param incompatible the incompatible to set
     */
    public void setIncompatible(final List<String> incompatible) {
        this.incompatible = incompatible;
    }

    /**
     * Class to string.
     *
     * @return The string representation.
     */
    @Override
    public String toString() {
        return new StringBuilder("UpdateSearchResult:[")
                .append(this.checkPackage).append(", ")
                .append("older: ").append(this.older).append(", ")
                .append("newer: ").append(this.newer)
                .append("]").toString();
    }

    /**
     * @return the component
     */
    public ServerPackage getCheckPackage() {
        return checkPackage;
    }

    /**
     * @param checkPackage the package to set
     */
    public void setCheckPackage(final ServerPackage checkPackage) {
        this.checkPackage = checkPackage;
    }

    /**
     * @return the repository
     */
    public String getRepository() {
        return repository;
    }

    /**
     * @param repository the repository to set
     */
    public void setRepository(final String repository) {
        this.repository = repository;
    }

    /**
     * @return the repositoryContainer
     */
    public String getRepositoryContainer() {
        return repositoryContainer;
    }

    /**
     * @param repositoryContainer the repositoryContainer to set
     */
    public void setRepositoryContainer(final String repositoryContainer) {
        this.repositoryContainer = repositoryContainer;
    }

}
