package org.pidome.server.system.logging;

import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.pidome.server.system.config.SystemConfig;

@SuppressWarnings("PMD.SystemPrintln")
@Plugin(name = "ColoredConsoleLogger", category = "Core", elementType = "appender", printObject = true)
public final class ColoredConsoleLogger extends AbstractAppender {

    /**
     * Readwrite lock.
     */
    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    /**
     * Read lock for locking the writing mechanism.
     */
    private final Lock readLock = rwLock.readLock();

    /**
     * Character set used for the output.
     */
    private static final String CHAR_SET = "UTF-8";

    /**
     * Prefix.
     */
    private static final String PREFIX = "\u001b[";
    /**
     * Suffix.
     */
    private static final String SUFFIX = "m";
    /**
     * Seperator.
     */
    private static final char SEPARATOR = ';';
    /**
     * End color.
     */
    private static final String END_COLOUR = PREFIX + SUFFIX;

    /**
     * Normal color.
     */
    private static final int TEXT_NORMAL = 0;
    /**
     * Bright color.
     */
    //private static final int TEXT_BRIGHT = 1; Not used now
    /**
     * Black color.
     */
    private static final int TEXT_COLOR_BLACK = 30;
    /**
     * Red color.
     */
    private static final int TEXT_COLOR_RED = 31;
    /**
     * Green color.
     */
    private static final int TEXT_COLOR_GREEN = 32;
    /**
     * Yellow color.
     */
    private static final int TEXT_COLOR_YELLOW = 33;
    /**
     * Blue color.
     */
    private static final int TEXT_COLOR_BLUE = 34;

    /**
     * Default black color.
     */
    private static final String DEFAULT_COLOUR = PREFIX + TEXT_NORMAL + SEPARATOR + TEXT_COLOR_BLACK + SUFFIX;
    /**
     * Yellow warning color.
     */
    private static final String WARN_COLOUR = PREFIX + TEXT_NORMAL + SEPARATOR + TEXT_COLOR_YELLOW + SUFFIX;
    /**
     * Red Error color.
     */
    private static final String ERROR_COLOUR = PREFIX + TEXT_NORMAL + SEPARATOR + TEXT_COLOR_RED + SUFFIX;
    /**
     * Green debug color.
     */
    private static final String DEBUG_COLOUR = PREFIX + TEXT_NORMAL + SEPARATOR + TEXT_COLOR_GREEN + SUFFIX;
    /**
     * Blue trace color.
     */
    private static final String TRACE_COLOUR = PREFIX + TEXT_NORMAL + SEPARATOR + TEXT_COLOR_BLUE + SUFFIX;

    /**
     * Application logger constructor.
     *
     * @param name The name of the logger.
     * @param filter The filter used.
     * @param layout The layout used.
     * @param ignoreExceptions Ignore exceptions or not.
     */
    protected ColoredConsoleLogger(final String name, final Filter filter, final Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    /**
     * Append logged content to whatever output.
     *
     * @param event The log event.
     */
    @Override
    public void append(final LogEvent event) {
        readLock.lock();
        try {
            final byte[] bytes = getLayout().toByteArray(event);
            if (SystemConfig.isDevMode()) {
                if (event.getLevel().equals(Level.ERROR) || event.getLevel().equals(Level.FATAL)) {
                    System.out.print(ERROR_COLOUR + new String(bytes, CHAR_SET) + END_COLOUR);
                } else if (event.getLevel().equals(Level.WARN)) {
                    System.out.print(WARN_COLOUR + new String(bytes, CHAR_SET) + END_COLOUR);
                } else if (event.getLevel().equals(Level.DEBUG)) {
                    System.out.print(DEBUG_COLOUR + new String(bytes, CHAR_SET) + END_COLOUR);
                } else if (event.getLevel().equals(Level.TRACE)) {
                    System.out.print(TRACE_COLOUR + new String(bytes, CHAR_SET) + END_COLOUR);
                } else {
                    System.out.print(DEFAULT_COLOUR + new String(bytes, CHAR_SET) + END_COLOUR);
                }
            } else {
                if (event.getLevel().equals(Level.ERROR) || event.getLevel().equals(Level.FATAL) || event.getLevel().equals(Level.WARN)) {
                    System.err.print(new String(bytes, CHAR_SET));
                } else {
                    System.out.print(new String(bytes, CHAR_SET));
                }
            }
        } catch (Exception ex) {
            if (!ignoreExceptions()) {
                throw new AppenderLoggingException(ex);
            }
        } finally {
            readLock.unlock();
        }
    }

    /**
     * Factory for creating the appender referenced from the log4j xml.
     *
     * @param name The name.
     * @param layout The layout.
     * @param filter The filter.
     * @param otherAttribute Any custom attributes.
     * @return The Application logger instance.
     */
    @PluginFactory
    public static ColoredConsoleLogger createAppender(
            @PluginAttribute("name") final String name,
            @PluginElement("Layout") final Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") final String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for ColoredConsoleLogger");
            return null;
        }
        return new ColoredConsoleLogger(name, filter, layout != null ? layout : PatternLayout.createDefaultLayout(), true);
    }
}
