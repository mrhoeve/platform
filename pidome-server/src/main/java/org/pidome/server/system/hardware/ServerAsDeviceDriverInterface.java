/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import org.pidome.platform.hardware.driver.config.HardwareConfiguration;
import org.pidome.platform.hardware.driver.config.SerialDeviceConfiguration;
import org.pidome.platform.hardware.driver.interfaces.HardwareDriverInterface;

/**
 * This interface is used internally to be able to expose server information.
 *
 * It is not exposed.
 *
 * @author John Sirach
 */
public interface ServerAsDeviceDriverInterface extends HardwareDriverInterface, HardwareConfiguration<SerialDeviceConfiguration> {

}
