/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.config;

/**
 * Exception used for configuration properties.
 *
 * @author John
 * @since 1.0
 */
public final class ConfigPropertiesException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>ConfigPropertiesException</code> without
     * detail message.
     */
    public ConfigPropertiesException() {
    }

    /**
     * Constructs an instance of <code>ConfigPropertiesException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ConfigPropertiesException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>ConfigPropertiesException</code> with the
     * specified detail message and throwable exception.
     *
     * @param msg The detail message.
     * @param thr The throwable exception.
     */
    public ConfigPropertiesException(final String msg, final Throwable thr) {
        super(msg, thr);
    }
}
