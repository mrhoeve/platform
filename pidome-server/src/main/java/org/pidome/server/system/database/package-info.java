/**
 * Database interface providers.
 * <p>
 * Package holding all the needed classes and methods to support database
 * actions in the system.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.database;
