/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import io.vertx.core.Future;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.HardwareDriverDiscovery;
import org.pidome.server.system.config.SystemConfig;

/**
 * The hardware driver store tracking the hardware drivers.
 *
 * @author John Sirach
 * @todo Singleton
 */
public final class HardwareDriverStore {

    /**
     * Private constructor.
     */
    private HardwareDriverStore() {
        /// default private constructor.
    }

    /**
     * Collection of possible hardware drivers categorized per transport type.
     */
    private static final Map<Transport.SubSystem, Set<Class<? extends HardwareDriver>>> COLLECTION = Collections.synchronizedMap(new HashMap<>());

    /**
     * Prepare the store.
     */
    static {
        for (Transport.SubSystem transport : Transport.SubSystem.values()) {
            COLLECTION.put(transport, new HashSet<>());
        }
    }

    /**
     * Checks if there are drivers available of the given transport.
     *
     * @param transport The transport subsystem to get the driver for.
     * @return true if a driver set is found.
     */
    public static boolean hasDriverAvailable(final Transport.SubSystem transport) {
        return COLLECTION.containsKey(transport) && COLLECTION.get(transport) != null && !COLLECTION.get(transport).isEmpty();
    }

    /**
     * Returns the list of available drivers for the given key.
     *
     * @param transport The transport to get the drivers for.
     * @return List of drivers.
     */
    public static Set<Class<? extends HardwareDriver>> getTransportDrivers(final Transport.SubSystem transport) {
        return COLLECTION.get(transport);
    }

    /**
     * Scans for drivers annotated with HardwareDriverDiscovery.
     *
     * @param future the future to indicate it's done.
     */
    @SuppressWarnings("unchecked")
    public static void scan(final Future<Void> future) {
        AccessController.doPrivileged((PrivilegedAction<Void>) () -> {
            try (ScanResult scanResult
                    = new ClassGraph()
                            .addClassLoader(new ScannerClassClassLoader(ClassLoader.getSystemClassLoader()))
                            .blacklistPackages("org.pidome.server", "org.pidome.tools")
                            .enableExternalClasses()
                            .enableAnnotationInfo()
                            .disableRuntimeInvisibleAnnotations()
                            .filterClasspathElements(filter -> {
                                return filter.contains(SystemConfig.getModulesPath().replace(".", ""));
                            })
                            .scan()) {
                        scanResult.getClassesWithAnnotation(HardwareDriverDiscovery.class.getCanonicalName()).stream().forEach(matchedClass -> {
                            for (Transport.SubSystem subSystem : Transport.SubSystem.values()) {
                                Class<? extends HardwareDriver> rawClass = (Class<? extends HardwareDriver>) matchedClass.loadClass();
                                if (Arrays.asList(rawClass.getInterfaces()).contains(subSystem.getImplementation())) {
                                    COLLECTION.get(subSystem).add((Class<? extends HardwareDriver>) rawClass);
                                }
                            }
                        });
                    } catch (Exception ex) {
                        future.fail(ex);
                    }
                    future.complete();
                    return null;
        });
    }

    /**
     * Returns all the registered hardware drivers.
     *
     * @return Map of registered drivers.
     */
    public static Map<Transport.SubSystem, Set<Class<? extends HardwareDriver>>> getRegisteredDrivers() {
        return COLLECTION;
    }

    /**
     * Class loader used to supprot scanning for modules.
     */
    private static final class ScannerClassClassLoader extends URLClassLoader {

        /**
         * Constructor.
         *
         * @param classLoader the original class loader.
         * @throws MalformedURLException When the internally given modules path
         * is malformed.
         * @throws IOException Thrown when walking the possible module paths
         * fail.
         */
        ScannerClassClassLoader(final ClassLoader classLoader) throws MalformedURLException, IOException {
            super(new URL[0], classLoader);
            Path connectorPath = new File(SystemConfig.getModulesPath()).toPath();
            SystemConfig.UrlPaths paths = new SystemConfig.UrlPaths();
            Files.walkFileTree(connectorPath, paths);
            paths.getLibrariesList().stream().forEach(url -> {
                addURL(url);
            });
        }

        /**
         * Method to add urls to the class loader paths list.
         *
         * @param url The url to add.
         */
        @Override
        protected void addURL(final URL url) {
            super.addURL(url);
        }
    }

}
