/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb;

/**
 * The interface to be implemented by systems supporting the USB subsystem.
 *
 * @author John Sirach
 */
public interface UsbInterface {

    /**
     * If the usb watchdog is running or not.
     *
     * @return if running or not.
     */
    boolean watchDogRunning();

    /**
     * Starts the discovery of USB devices.
     *
     * @throws UnsupportedOperationException When discovery is not supported.
     */
    void discover() throws UnsupportedOperationException;

    /**
     * Starts the real time plugging event handler.
     */
    void startWatchdog();

    /**
     * Starts the real time plugging event handler.
     */
    void stopWatchdog();

    /**
     * Register on USB events.
     *
     * @param listener The listener.
     */
    void addEventListener(UsbMutationListener listener);

    /**
     * De-register from USB events.
     *
     * @param listener The listener.
     */
    void removeEventListener(UsbMutationListener listener);

}
