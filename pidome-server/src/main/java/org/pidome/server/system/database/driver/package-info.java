/**
 * Database drivers.
 * <p>
 * All classes needed to support the database interface providing proxy's and
 * interfaces to different kind of database servers.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.database.driver;
