/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.packages;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * A server package.
 *
 * @author John
 */
@Entity
public class ServerPackage implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Entity id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Presentation name visible to the end user.
     */
    private String presentationName;

    /**
     * Name of the package.
     */
    private String packageName;

    /**
     * Version of the package.
     */
    private String packageVersion;

    /**
     * Group of the package.
     */
    private String packageGroup;

    /**
     * Type of package.
     */
    private ServerPackageType packageType;

    /**
     * @return the presentationName
     */
    public String getPresentationName() {
        return presentationName;
    }

    /**
     * @param presentationName the presentationName to set
     */
    public void setPresentationName(final String presentationName) {
        this.presentationName = presentationName;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(final String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the packageVersion
     */
    public String getPackageVersion() {
        return packageVersion;
    }

    /**
     * @param packageVersion the packageVersion to set
     */
    public void setPackageVersion(final String packageVersion) {
        this.packageVersion = packageVersion;
    }

    /**
     * @return the packageGroup
     */
    public String getPackageGroup() {
        return packageGroup;
    }

    /**
     * @param packageGroup the packageGroup to set
     */
    public void setPackageGroup(final String packageGroup) {
        this.packageGroup = packageGroup;
    }

    /**
     * @return the packageType
     */
    public ServerPackageType getPackageType() {
        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(final ServerPackageType packageType) {
        this.packageType = packageType;
    }

    /**
     * Class to string.
     *
     * @return The string representation.
     */
    @Override
    public String toString() {
        return new StringBuilder("Component:[")
                .append("packageType: ").append(this.packageType).append(", ")
                .append("packageGroup: ").append(this.packageGroup).append(", ")
                .append("packageName: ").append(this.packageName).append(", ")
                .append("packageVersion: ").append(this.packageVersion)
                .append("]").toString();
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

}
