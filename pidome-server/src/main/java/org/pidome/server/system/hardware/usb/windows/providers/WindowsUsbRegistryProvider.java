/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb.windows.providers;

import java.util.regex.Pattern;
import org.pidome.server.system.hardware.Transport;

/**
 * The base interface for a provider to provide the information required to
 * identify an USB connected device.
 *
 * @author johns
 */
public interface WindowsUsbRegistryProvider {

    /**
     * The registry pattern used by the supported USB devices (SymbolicName).
     */
    Pattern REG_KEY_VID_PID_PATTERN = Pattern.compile("(?i)VID_([0-9a-z]+)&PID_([0-9a-z]+).*");

    /**
     * The broadcast name of a device into a pattern to substract vid, pid and
     * serial.
     */
    Pattern BROADCAST_REG_KEY_PATTERN = Pattern.compile("(?i).+#(VID_[0-9a-z]+&PID_[0-9a-z]+)#(.+)#\\{(.+)\\}");

    /**
     * Group index for vid in broadcast regex pattern.
     */
    int BROADCAST_REG_KEY_PATTERN_VID_PID = 1;

    /**
     * Group index for serial in broadcast regex pattern.
     */
    int BROADCAST_REG_KEY_PATTERN_SERIAL = 2;

    /**
     * Returns the transport type of the device for driver type identification.
     *
     * @param originalVidPidIdentifier The original identifier as found in the
     * registry.
     * @param deviceKey The key which is used to identify the device.
     * @return The transport subsystem type.
     * @throws RegistryEntryNotFoundException When the key is invalid for this
     * type.
     */
    Transport.SubSystem getDeviceTransportType(String originalVidPidIdentifier, String deviceKey) throws RegistryEntryNotFoundException;

    /**
     * The port on which the device is available.This method returns a port
     * based on the transport type.
     *
     * @param originalVidPidIdentifier The original identifier as found in the
     * registry.
     * @param deviceKey The key which is used to identify the device.
     * @return The port based on the transport type.
     * @throws RegistryEntryNotFoundException When the key is invalid for this
     * type.
     */
    String getDeviceName(String originalVidPidIdentifier, String deviceKey) throws RegistryEntryNotFoundException;

    /**
     * The port on which the device is available.This method returns a port
     * based on the transport type.
     *
     * @param originalVidPidIdentifier The original identifier as found in the
     * registry.
     * @param deviceKey The key which is used to identify the device.
     * @return The port based on the transport type.
     * @throws RegistryEntryNotFoundException When the key is invalid for this
     * type.
     */
    String getPort(String originalVidPidIdentifier, String deviceKey) throws RegistryEntryNotFoundException;

}
