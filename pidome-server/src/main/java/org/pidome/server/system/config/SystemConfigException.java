/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.config;

/**
 * Exception for system configuration.
 *
 * @author John
 */
public final class SystemConfigException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>SystemConfigException</code> without
     * detail message.
     */
    public SystemConfigException() {
        // Default constructor
    }

    /**
     * Constructs an instance of <code>SystemConfigException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public SystemConfigException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>SystemConfigException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     * @param original The original thrown exception.
     */
    public SystemConfigException(final String msg, final Throwable original) {
        super(msg, original);
    }

}
