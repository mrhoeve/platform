/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb.windows.providers;

import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import org.pidome.server.system.hardware.Transport;

/**
 *
 * @author johns
 */
public class GenericSerialProvider implements WindowsUsbRegistryProvider {

    /**
     * @inheritDoc
     */
    @Override
    public Transport.SubSystem getDeviceTransportType(final String originalVidPidIdentifier, final String deviceKey) {
        return Transport.SubSystem.SERIAL;
    }

    /**
     * @inheritDoc
     */
    @Override
    public String getDeviceName(final String originalVidPidIdentifier, final String deviceKey) {
        return Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE,
                "SYSTEM\\CurrentControlSet\\Enum\\USB\\" + originalVidPidIdentifier + "\\" + deviceKey,
                "FriendlyName");
    }

    /**
     * @inheritDoc
     */
    @Override
    public String getPort(final String originalVidPidIdentifier, final String deviceKey) {
        return Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE,
                "SYSTEM\\CurrentControlSet\\Enum\\USB\\" + originalVidPidIdentifier + "\\" + deviceKey + "\\Device Parameters",
                "PortName");
    }

}
