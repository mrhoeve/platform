/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.repositories;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.pidome.server.system.installer.UpdateSearchResult;
import org.pidome.server.system.packages.ServerPackage;

/**
 * A provider for repositories.
 *
 * A repository provider is a collection of repositories where a package
 * originates from. The provider can consist of multiple repositories which
 * normally would contain normal and snapshot versions of a package.
 *
 * @author John
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RepositoryContainer implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Entity id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * the id of the repository provider.
     */
    private String repositoryProviderId;

    /**
     * The name of the repository provider.
     */
    private String repositoryProviderName;

    /**
     * The description of the repository provider.
     */
    private String repositoryProviderDescription;

    /**
     * If the container is active.
     */
    private boolean active;

    /**
     * If this container is read only.
     *
     * This parameter should be considered read only, used to identify
     * internally used repositories.
     */
    private boolean readOnly;

    /**
     * List of repositories in the provider.
     */
    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Repository> repositories;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @return the repositoryProviderId
     */
    public String getRepositoryProviderId() {
        return repositoryProviderId;
    }

    /**
     * @param repositoryProviderId the repositoryProviderId to set
     */
    public void setRepositoryProviderId(final String repositoryProviderId) {
        this.repositoryProviderId = repositoryProviderId;
    }

    /**
     * @return the repositoryProviderName
     */
    public String getRepositoryProviderName() {
        return repositoryProviderName;
    }

    /**
     * @param repositoryProviderName the repositoryProviderName to set
     */
    public void setRepositoryProviderName(final String repositoryProviderName) {
        this.repositoryProviderName = repositoryProviderName;
    }

    /**
     * @return the repositoryProviderDescription
     */
    public String getRepositoryProviderDescription() {
        return repositoryProviderDescription;
    }

    /**
     * @param repositoryProviderDescription the repositoryProviderDescription to
     * set
     */
    public void setRepositoryProviderDescription(final String repositoryProviderDescription) {
        this.repositoryProviderDescription = repositoryProviderDescription;
    }

    /**
     * @return the repositories
     */
    public List<Repository> getRepositories() {
        return repositories;
    }

    /**
     * @param repositories the repositories to set
     */
    public void setRepositories(final List<Repository> repositories) {
        this.repositories = repositories;
    }

    /**
     * Adds a single repository to the container.
     *
     * @param repository the repository to add.
     * @return true when added.
     * @throws RepositoryContainerException When adding a repository fails
     */
    public boolean addRepository(final Repository repository) throws RepositoryContainerException {
        return false;
    }

    /**
     * Removes a single repository to the container.
     *
     * @param repository the repository to remove.
     * @return true when removed.
     * @throws RepositoryContainerException When removing a repository fails
     */
    public boolean removeRepository(final Repository repository) throws RepositoryContainerException {
        return false;
    }

    /**
     * Does an update check on the registered repositories.
     *
     * @param checkPackage The package to check updates for.
     * @return The search results.
     */
    public List<UpdateSearchResult> getVersions(final ServerPackage checkPackage) {
        List<UpdateSearchResult> versions = new ArrayList<>();
        this.repositories.forEach((repository) -> {
            versions.add(repository.getUpdates(repositoryProviderId, checkPackage));
        });
        return versions;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * @return the readOnly
     */
    public boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Set read only.
     *
     * Non functional method only used for internal repositories.
     *
     * @param readOnly the readOnly to set
     */
    public void setReadOnly(final boolean readOnly) {
        this.readOnly = readOnly;
    }

}
