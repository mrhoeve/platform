/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb.linux;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;

/**
 * Mapping interface to C functions.
 *
 * @author John Sirach
 */
interface LinuxUsbUdevLib extends Library {

    /**
     * Mapping to udev.
     */
    LinuxUsbUdevLib INSTANCE = Native.load("udev", LinuxUsbUdevLib.class);

    /**
     * Mapping to PID.
     *
     * @return number.
     */
    int getpid();

    /**
     * Mapping to parent process.
     *
     * @return number.
     */
    int getppid();

    /**
     * Get epoch.
     *
     * @param buf pointer to store in.
     * @return The epoch.
     */
    @SuppressWarnings("PMD.UseVarargs")
    long time(long[] buf);

    /**
     * Creating a new udev instance.
     *
     * @return new udev instance.
     */
    LinuxUsbUdevLib.udev udev_new();

    /**
     * Udev events monitor.
     *
     * @param udev udev instance.
     * @param name Monitor name.
     * @return New monitor instance.
     */
    LinuxUsbUdevLib.udev_monitor udev_monitor_new_from_netlink(LinuxUsbUdevLib.udev udev, String name);

    /**
     * Set the filter for the udev events monitor.
     *
     * @param udev_monitor Udev monitor.
     * @param subsystem subsystem to filter.
     * @param devtype device type to filter.
     * @return negative on failure, otherwise success.
     */
    int udev_monitor_filter_add_match_subsystem_devtype(LinuxUsbUdevLib.udev_monitor udev_monitor, String subsystem, String devtype);

    /**
     * Retrieves the device reported by the monitor.
     *
     * @param udev_monitor The monitor.
     * @return The device reported.
     */
    LinuxUsbUdevLib.udev_device udev_monitor_receive_device(LinuxUsbUdevLib.udev_monitor udev_monitor);

    /**
     * Returns usb device information representation.
     *
     * @param udev_device The USB device.
     * @param entryName The field to retrieve. This can be vendor_id,product_id
     * etc..
     * @return Attribute value as String
     */
    String udev_device_get_sysattr_value(LinuxUsbUdevLib.udev_device udev_device, String entryName);

    /**
     * Returns the device's node.
     *
     * @param udev_device The device.
     * @return Node as string.
     */
    String udev_device_get_devnode(LinuxUsbUdevLib.udev_device udev_device);

    /**
     * Returns the device's subsystem.
     *
     * @param udev_device The device.
     * @return The subsystem reported.
     */
    String udev_device_get_subsystem(LinuxUsbUdevLib.udev_device udev_device);

    /**
     * Returns the device's dev type.
     *
     * @param udev_device The device.
     * @return The device type reported.
     */
    String udev_device_get_devtype(LinuxUsbUdevLib.udev_device udev_device);

    /**
     * Retrieves the dev path.
     *
     * @param udev_device The device.
     * @return Path to the device.
     */
    String udev_device_get_devpath(LinuxUsbUdevLib.udev_device udev_device);

    /**
     * Returns the action for this device.
     *
     * @param udev_device The device.
     * @return The action reported for this device.
     */
    String udev_device_get_action(LinuxUsbUdevLib.udev_device udev_device);

    /**
     * Removes reference.
     *
     * @param udev_device The device to release.
     */
    void udev_device_unref(LinuxUsbUdevLib.udev_device udev_device);

    /**
     * Retrieves the file descriptor of the monitor.
     *
     * @param udev_monitor The monitor.
     * @return The file descriptor.
     */
    int udev_monitor_get_fd(LinuxUsbUdevLib.udev_monitor udev_monitor);

    /**
     * Enables receiving.
     *
     * @param udev_monitor The monitor
     * @return negative on failure, otherwise success.
     */
    int udev_monitor_enable_receiving(LinuxUsbUdevLib.udev_monitor udev_monitor);

    /**
     * Release the monitor.
     *
     * @param udev_monitor The monitor to remove reference to.
     */
    void udev_monitor_unref(LinuxUsbUdevLib.udev_monitor udev_monitor);

    /**
     * Release self reference.
     *
     * @param udev The udev lib to release.
     */
    void udev_unref(LinuxUsbUdevLib.udev udev);

    /**
     * enumerate reference.
     *
     * @param udev_enumerate the enumerate struct
     * @return The given struct.
     */
    LinuxUsbUdevLib.udev_enumerate udev_enumerate_ref(LinuxUsbUdevLib.udev_enumerate udev_enumerate);

    /**
     * Scan already connected devices.
     *
     * @param udev_enumerate The enumerate struct
     * @return negative on failure, otherwise success.
     */
    int udev_enumerate_scan_devices(LinuxUsbUdevLib.udev_enumerate udev_enumerate);

    /**
     * New enumerator for devices.
     *
     * @param udev The udev lib.
     * @return The enumerate struct.
     */
    LinuxUsbUdevLib.udev_enumerate udev_enumerate_new(LinuxUsbUdevLib.udev udev);

    /**
     * Enum list entries.
     *
     * @param udev_enumerate The enumerate struct.
     * @return List entry struct.
     */
    LinuxUsbUdevLib.udev_list_entry udev_enumerate_get_list_entry(LinuxUsbUdevLib.udev_enumerate udev_enumerate);

    /**
     * Go to the next entry in the enumeration.
     *
     * @param list_entry List entry struct.
     * @return given List entry struct on next position.
     */
    LinuxUsbUdevLib.udev_list_entry udev_list_entry_get_next(LinuxUsbUdevLib.udev_list_entry list_entry);

    /**
     * Finds the parent with the given subsystem.
     *
     * @param udev_device The device.
     * @param subsystem The subsystem.
     * @param devtype The device type.
     * @return The parent device.
     */
    LinuxUsbUdevLib.udev_device udev_device_get_parent_with_subsystem_devtype(udev_device udev_device, String subsystem, String devtype);

    /**
     * Get the filename from the entry.
     *
     * @param list_entry The list entry.
     * @return Path to device.
     */
    String udev_list_entry_get_name(LinuxUsbUdevLib.udev_list_entry list_entry);

    /**
     * Create a device from a filename.
     *
     * @param udev The udev lib.
     * @param syspath the path to the device.
     * @return The device pointed to.
     */
    LinuxUsbUdevLib.udev_device udev_device_new_from_syspath(LinuxUsbUdevLib.udev udev, String syspath);

    /**
     * release reference to enum.
     *
     * @param udev_enumerate the enumerate to release.
     */
    void udev_enumerate_unref(LinuxUsbUdevLib.udev_enumerate udev_enumerate);

    /**
     * Udev pointer.
     */
    public class udev extends PointerType {

        /**
         * Constructor.
         *
         * @param address The pointer address.
         */
        public udev(final Pointer address) {
            super(address);
        }

        /**
         * Constructor without pointer.
         */
        public udev() {
            super();
        }
    };

    /**
     * Pointer to the udev monitor.
     */
    public class udev_monitor extends PointerType {

        /**
         * constructor for the monitor.
         *
         * @param address The pointer address.
         */
        public udev_monitor(final Pointer address) {
            super(address);
        }

        /**
         * Constructor without pointer address.
         */
        public udev_monitor() {
            super();
        }
    };

    /**
     * The pointer to the device reported by the monitor.
     */
    public class udev_device extends PointerType {

        /**
         * Constructor for the device.
         *
         * @param address The address pointer.
         */
        public udev_device(final Pointer address) {
            super(address);
        }

        /**
         * Constructor without pointer.
         */
        public udev_device() {
            super();
        }
    };

    /**
     * udev devices enumerator.
     */
    public class udev_enumerate extends PointerType {

        /**
         * Constructor for enumeration.
         *
         * @param address The address pointer.
         */
        public udev_enumerate(final Pointer address) {
            super(address);
        }

        /**
         * Constructor for enumeration without pointer.
         */
        public udev_enumerate() {
            super();
        }
    };

    /**
     * get list entry.
     */
    public class udev_list_entry extends PointerType {

        /**
         * Consgtructor for a list entry with pointer.
         *
         * @param address The pointer address.
         */
        public udev_list_entry(final Pointer address) {
            super(address);
        }

        /**
         * Constructor without pointer.
         */
        public udev_list_entry() {
            super();
        }
    };

}
