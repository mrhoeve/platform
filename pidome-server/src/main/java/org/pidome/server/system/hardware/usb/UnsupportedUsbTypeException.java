/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb;

/**
 * The exception used when an unsupported usb type is encountered.
 *
 * @author John Sirach
 */
public class UnsupportedUsbTypeException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>UnsupportedUsbTypeException</code>
     * without detail message.
     */
    public UnsupportedUsbTypeException() {
        // default constructor.
    }

    /**
     * Constructs an instance of <code>UnsupportedUsbTypeException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public UnsupportedUsbTypeException(final String msg) {
        super(msg);
    }
}
