/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.system.hardware.peripherals.Peripheral;

/**
 * Listener interface for hardware mutations.
 *
 * @author John Sirach
 */
public interface HardwareMutationListener {

    /**
     * A mutation in one of the subsystems.
     *
     * @param peripheral The peripheral attached or removed.
     * @param mutation The mutation type.
     */
    void hardwareMutation(Peripheral<? extends HardwareDriver> peripheral, HardwareRoot.Mutation mutation);

}
