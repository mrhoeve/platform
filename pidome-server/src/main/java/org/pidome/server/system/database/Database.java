/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import com.zaxxer.hikari.HikariDataSource;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.database.driver.DatabaseConfigurationException;
import org.pidome.server.system.database.driver.DatabaseDriverInterface;
import org.pidome.server.system.database.driver.DatasourceConfiguration;
import org.pidome.tools.enums.ResourceType;
import org.pidome.tools.utilities.NetworkUtils;

/**
 * ORM handler for the database connection.
 *
 * @author John
 */
public final class Database {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(Database.class);

    /**
     * The configuration living for this instance.
     */
    private DatasourceConfiguration configuration;

    /**
     * The used database driver interface.
     *
     * Only available after configuring.
     */
    private DatabaseDriverInterface activeDriver;

    /**
     * The database instance.
     */
    private static Database instance;

    /**
     * Sync lock on get instance.
     */
    private static final Object GET_INSTANCE_LOCK = new Object();

    /**
     * Hibernate entityManager factory factory.
     */
    private EntityManagerFactory entityManagerFactory;

    /**
     * Get the database instance.
     *
     * @return a Database instance.
     */
    public static Database getInstance() {
        synchronized (GET_INSTANCE_LOCK) {
            if (instance == null) {
                LOG.debug("Creating [Database] instance");
                instance = new Database();
            }
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * See <code>getInstance()</code>
     */
    private Database() {
        // Default unreachable constructor.
    }

    /**
     * Initializes the DB.
     *
     * @throws DatabaseConfigurationException When the configuration is
     * insufficient.
     * @throws DatabaseConnectionException When there is a connection problem.
     * @throws DatabasePreEmptException When the database preparation fails.
     */
    public void init() throws DatabaseConfigurationException, DatabaseConnectionException, DatabasePreEmptException {
        configure();
        createConnection();
        preEmpt();
        activeDriver.afterInit(this.configuration);
    }

    /**
     * Configures the data source.
     *
     * @throws DatabaseConfigurationException When the configuration can not be
     * applied.
     */
    private void configure() throws DatabaseConfigurationException {
        if (configuration == null) {
            configuration = new DatasourceConfiguration();
            activeDriver = applyDatasourceModifier(configuration);
            if (!resourceExists() && configuration.getDriverType().getResourceType().equals(ResourceType.FILESYSTEM)) {
                File dbFile = new File(configuration.getLocation());
                try {
                    Files.createDirectories(dbFile.getParentFile().toPath());
                    LOG.info("Created non existing [{}] database file [{}]", configuration.getDriverType(), configuration.getLocation());
                } catch (IOException ex) {
                    throw new DatabaseConfigurationException(ex);
                }
            }
        }
    }

    /**
     * Creates the connection pool.
     *
     * Datasource close is suppressed as it is closed at service stop by
     * <code>EntityManagerFactory.close()</code>
     *
     * @throws DatabaseConnectionException When the connection pool can not be
     * created.
     */
    @SuppressWarnings("PMD.CloseResource")
    private void createConnection() throws DatabaseConnectionException {
        if (entityManagerFactory == null || !entityManagerFactory.isOpen()) {
            try {
                HikariDataSource dataSource = new HikariDataSource(configuration.getHikariConfig());

                Map<String, Object> entityManagerOptionsMap = new HashMap<>();

                /// caching
                entityManagerOptionsMap.put(AvailableSettings.USE_SECOND_LEVEL_CACHE, true);
                entityManagerOptionsMap.put(AvailableSettings.CACHE_REGION_FACTORY, "org.hibernate.cache.jcache.JCacheRegionFactory");
                entityManagerOptionsMap.put("hibernate.javax.cache.provider", "org.ehcache.jsr107.EhcacheCachingProvider");

                entityManagerOptionsMap.put(AvailableSettings.DATASOURCE, dataSource);
                entityManagerOptionsMap.put(AvailableSettings.DIALECT, configuration.getDialect());
                entityManagerOptionsMap.put(AvailableSettings.SCANNER_DISCOVERY, "class, hbm");

                entityManagerOptionsMap.put(AvailableSettings.LOADED_CLASSES, entityScanner());

                if (SystemConfig.getLogLevel().equals(Level.TRACE)) {
                    entityManagerOptionsMap.put(AvailableSettings.SHOW_SQL, true);
                    entityManagerOptionsMap.put(AvailableSettings.FORMAT_SQL, true);
                }

                entityManagerFactory = new HibernatePersistenceProvider().createEntityManagerFactory(
                        "pidome-default",
                        entityManagerOptionsMap);

                LOG.info("Connected with [{}] via [{}]", configuration.getDriverType(), configuration.getLocation());
            } catch (Exception ex) {
                throw new DatabaseConnectionException(ex);
            }
        }
    }

    /**
     * Closes a connection.
     */
    public void disconnect() {
        LOG.info("Closing the database connections");
        activeDriver.onShutdown();
        entityManagerFactory.close();
    }

    /**
     * Applies the selected driver specific configuration.
     *
     * @param config The generic configuration object
     * @throws DatabaseConfigurationException When the configuration can not be
     * applied.
     * @return Returns a datasource object interface with configurations
     * modified from a configuration.
     */
    private DatabaseDriverInterface applyDatasourceModifier(final DatasourceConfiguration config) throws DatabaseConfigurationException {
        try {
            Class<? extends DatabaseDriverInterface> clazz = config.getDriverType().getDriverClass();
            Constructor<?> ctor = clazz.getConstructor();
            DatabaseDriverInterface dataBaseDataSourceObject = (DatabaseDriverInterface) ctor.newInstance();
            LOG.info("Applying [{}] database specific settings", config.getDriverType().getDriverName());
            dataBaseDataSourceObject.appendConfiguration(config.getHikariConfig());
            config.setDialect(dataBaseDataSourceObject.getDialect());
            return dataBaseDataSourceObject;
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new DatabaseConfigurationException(ex.getMessage(), ex);
        }
    }

    /**
     * Method to check if a database file or connection exists. This method
     * currently only supports <code>ResourceType.NETWORK</code> and
     * <code>ResourceType.FILESYSTEM</code>. All other methods return false.
     *
     * @return true when existing, false when unreachable or non existing.
     */
    private boolean resourceExists() {
        switch (configuration.getDriverType().getResourceType()) {
            case FILESYSTEM:
                File dbFile = new File(configuration.getLocation());
                return dbFile.exists();
            case NETWORK:
                return NetworkUtils.remotePortAvailable(configuration.getLocation());
            case MEMORY:
                return true; /// we really have no clue.
            default:
                return false;
        }
    }

    /**
     * Method used to (re)create the databases with all the minimal required
     * information. This method works in a procedural way from beginning to end.
     * When there is a failure it starts over from the beginning.
     *
     * @return Indicates if the operation was successful or not.
     * @throws DatabasePreEmptException When there is an error during
     * (re)creation
     */
    private boolean preEmpt() throws DatabasePreEmptException {
        if (!resourceExists()) {
            if (configuration.getDriverType().getResourceType().equals(ResourceType.NETWORK)) {
                createPreEmptException("Database is unreachable");
            } else {
                createPreEmptException("Database file does not exist");
            }
        }
        LOG.info("Database structure initialized");
        return true;
    }

    /**
     * Returns a new session from the session factory.
     *
     * @return The entitymanager.
     */
    public EntityManager getNewManager() {
        return this.entityManagerFactory.createEntityManager();
    }

    /**
     * Returns an autoclosable entity manager.
     *
     * @return an entitymanager which is auto closed.
     */
    public AutoClosableEntityManager getNewAutoClosableManager() {
        return new AutoClosableEntityManager(getNewManager());
    }

    /**
     * Creates a preEmpt exception with all the required information.
     *
     * @param message The message as reason
     * @throws DatabasePreEmptException Always thrown exception from this method
     * to be used with <code>preEmpt</code>
     */
    private void createPreEmptException(final String message) throws DatabasePreEmptException {
        String pre = "Error with database [" + configuration.getDriverType().getDriverName() + "] located at [" + configuration.getLocation() + "]: " + message;
        throw new DatabasePreEmptException(pre);
    }

    /**
     * Scans for classes with the entity annotation.
     *
     * @return A collection of entities for the service to prepare.
     */
    @SuppressWarnings("unchecked")
    private List<Class> entityScanner() {
        final List<Class> entityClasses = new ArrayList<>();
        try (ScanResult scanResult
                = new ClassGraph()
                        .whitelistPackages("org.pidome.server")
                        .enableAnnotationInfo()
                        .scan()) {
                    scanResult.getClassesWithAnnotation(Entity.class.getCanonicalName()).stream().forEach(matchedClass -> {
                        entityClasses.add(matchedClass.loadClass());
                    });
                }
                return entityClasses;
    }

    /**
     * A wrapper around the entity manager.
     */
    public static class AutoClosableEntityManager implements AutoCloseable {

        /**
         * The original entity manager.
         */
        private final EntityManager manager;

        /**
         * Constructing the autoclosable manager.
         *
         * @param manager The original entity manager.
         */
        public AutoClosableEntityManager(final EntityManager manager) {
            this.manager = manager;
        }

        /**
         * Returns the manager.
         *
         * @return The original manager.
         */
        public EntityManager getManager() {
            return this.manager;
        }

        /**
         * Closes the manager.
         */
        @Override
        public void close() {
            manager.close();
        }

    }
}
