/**
 * USB peripherals.
 * <p>
 * Provides means and methods to connect PiDome to USB peripheral interfaces and proxies to OS specific implementations
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.hardware.usb;
