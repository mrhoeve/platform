/**
 * Maven repositories.
 * <p>
 * Package providing maven type repositories.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.installer.repositories.maven;
