/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import io.vertx.core.Future;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceException;
import org.pidome.server.system.database.driver.DatabaseConfigurationException;

/**
 * The database service class.
 *
 * Configures and provides access to dao objects.
 *
 * @author John Sirach
 */
public class DatabaseService extends AbstractService {

    /**
     * The database service.
     */
    private Database database;

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(DatabaseService.class);

    /**
     * Starts the database service.
     *
     * @param startFuture The future used to detect success or failure.
     */
    @Override
    public final void start(final Future<Void> startFuture) {
        LOG.info("Calling start database service");
        try {
            database = Database.getInstance();
            database.init();
            try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
                /// Check if it opens.
                Query query = autoManager.getManager().createNativeQuery("SELECT 1");
                query.getSingleResult();
            }
            startFuture.complete();
        } catch (DatabaseConfigurationException ex) {
            startFuture.fail(new ServiceException("Failed to start database service because of a configuration error", ex));
        } catch (DatabaseConnectionException ex) {
            startFuture.fail(new ServiceException("Failed to start database service because of a connection error", ex));
        } catch (DatabasePreEmptException ex) {
            startFuture.fail(new ServiceException("Failed to start database service because of a database population error", ex));
        }
    }

    /**
     * Stops the database service.
     *
     * @param stopFuture The future used to detect success or failure.
     */
    @Override
    public final void stop(final Future<Void> stopFuture) {
        database.disconnect();
        stopFuture.complete();
    }

}
