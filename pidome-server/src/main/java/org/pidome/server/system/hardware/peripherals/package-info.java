/**
 * Peripheral classes
 * <p>
 * Package for supplying peripheral handling.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.hardware.peripherals;
