/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.peripherals;

/**
 * A driver candidate for an attached peripheral.
 *
 * @author John Sirach
 */
public class PeripheralDriverCandidate {

    /**
     * The name of the driver.
     */
    private String name;

    /**
     * The description of the driver.
     */
    private String description;

    /**
     * The class for the driver.
     */
    private String driver;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the driver
     */
    public String getDriver() {
        return driver;
    }

    /**
     * @param driver the driver to set
     */
    public void setDriver(final String driver) {
        this.driver = driver;
    }

    /**
     * String representation of this class.
     *
     * @return This class as String.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("PeripheralDriverCandidate:[");
        builder.append("name:").append(name).append(",");
        builder.append("description:").append(description).append(",");
        builder.append("driver:").append(driver).append("]");
        return builder.toString();
    }

}
