/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.repositories;

import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import org.pidome.server.system.installer.UpdateSearchResult;
import org.pidome.server.system.packages.ServerPackage;

/**
 * A single remote repository.
 *
 * @author John
 */
@Entity
@Inheritance
@DiscriminatorColumn(name = "repository_type")
public abstract class Repository implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Entity id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Repository name.
     */
    private String repositoryName;

    /**
     * A description of the remote repository.
     */
    private String repositoryDescription;

    /**
     * Repository location.
     */
    private String repositoryLocation;

    /**
     * If the remote repository contains experimental items.
     */
    private boolean experimental;

    /**
     * The repository type.
     */
    private RepositoryType repositoryType;

    /**
     * The local repository.
     */
    private String localRepositoryLocation;

    /**
     * Constructor.
     *
     * @param repositoryType The type of repository
     */
    protected Repository(final RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    /**
     * @return the repositoryName
     */
    public String getRepositoryName() {
        return repositoryName;
    }

    /**
     * @param repositoryName the repositoryName to set
     */
    public void setRepositoryName(final String repositoryName) {
        this.repositoryName = repositoryName;
    }

    /**
     * @return the repositoryDescription
     */
    public String getRepositoryDescription() {
        return repositoryDescription;
    }

    /**
     * @param repositoryDescription the repositoryDescription to set
     */
    public void setRepositoryDescription(final String repositoryDescription) {
        this.repositoryDescription = repositoryDescription;
    }

    /**
     * @return the repositoryLocation
     */
    public String getRepositoryLocation() {
        return repositoryLocation;
    }

    /**
     * @param repositoryLocation the repositoryLocation to set
     */
    public void setRepositoryLocation(final String repositoryLocation) {
        this.repositoryLocation = repositoryLocation;
    }

    /**
     * @return the experimental
     */
    public boolean isExperimental() {
        return experimental;
    }

    /**
     * @param experimental the experimental to set
     */
    public void setExperimental(final boolean experimental) {
        this.experimental = experimental;
    }

    /**
     * @return the repositoryType
     */
    public RepositoryType getRepositoryType() {
        return repositoryType;
    }

    /**
     * @param repositoryType the repositoryType to set
     */
    public void setRepositoryType(final RepositoryType repositoryType) {
        this.repositoryType = repositoryType;
    }

    /**
     * @return the localRepositoryLocation
     */
    public String getLocalRepositoryLocation() {
        return localRepositoryLocation;
    }

    /**
     * @param localRepositoryLocation the localRepositoryLocation to set
     */
    public void setLocalRepositoryLocation(final String localRepositoryLocation) {
        this.localRepositoryLocation = localRepositoryLocation;
    }

    /**
     * Checks for updates.
     *
     * @param repositoryId The id of the repository.
     * @param component The component to get the updates for.
     * @return The result of the updates request.
     */
    public abstract UpdateSearchResult getUpdates(String repositoryId, ServerPackage component);

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * Constructs a local repository location.
     *
     * @param repoId Repository id.
     * @return The local repository location.
     */
    public String constructLocalRepositoryLocation(final String repoId) {
        return new StringBuilder(this.getLocalRepositoryLocation())
                .append("/").append(this.repositoryType.name())
                .append("/").append(repoId)
                .append("/").append(this.experimental ? "snapshots" : "releases")
                .toString();
    }

}
