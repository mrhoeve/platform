/**
 * System installer classes.
 * <p>
 * Contains all classes required to install or update drivers, plugins and other
 * components in the server.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.installer;
