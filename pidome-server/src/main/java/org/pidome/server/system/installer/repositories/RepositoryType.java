/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.repositories;

/**
 * The different kind of repository types.
 *
 * @author John
 */
public enum RepositoryType {

    /**
     * A remote maven 2 repository.
     */
    MAVEN2;

}
