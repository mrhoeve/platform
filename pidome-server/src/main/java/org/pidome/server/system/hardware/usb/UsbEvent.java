/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb;

/**
 * Event class used to determine which event occurred on the USB subsystem.
 *
 * @author John Sirach
 */
public class UsbEvent {

    /**
     * The type of USB event.
     */
    public enum Type {

        /**
         * Device added to the subsystem.
         */
        DEVICE_ADDED,
        /**
         * Device removed from the subsystem.
         */
        DEVICE_REMOVED,

    }

}
