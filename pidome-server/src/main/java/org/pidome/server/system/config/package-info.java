/**
 * System configuration classes.
 * <p>
 * All configuration classes used to configure the system.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.config;
