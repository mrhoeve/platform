/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

/**
 * Exception used when database configuration errors are found.
 *
 * @author John
 */
public class DatabaseConfigurationException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DatabaseConfigurationException</code>
     * without detail message.
     */
    public DatabaseConfigurationException() {
    }

    /**
     * Constructs an instance of <code>DatabaseConfigurationException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public DatabaseConfigurationException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>DatabaseConfigurationException</code>
     * with the specified <code>Throwable</code>.
     *
     * @param thrw the throwable.
     */
    public DatabaseConfigurationException(final Throwable thrw) {
        super(thrw);
    }

    /**
     * Constructs an instance of <code>DatabaseConfigurationException</code>
     * with the specified detail message and <code>Throwable</code>.
     *
     * @param msg the detail message.
     * @param thrw the throwable.
     */
    public DatabaseConfigurationException(final String msg, final Throwable thrw) {
        super(msg, thrw);
    }
}
