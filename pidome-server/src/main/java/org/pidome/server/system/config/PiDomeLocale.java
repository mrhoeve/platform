/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.config;

import java.io.IOException;
import java.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.tools.utilities.LocaleUtils;

/**
 * The system's registered locale settings.
 *
 * @author John
 */
public final class PiDomeLocale {

    /**
     * Locale logger.
     */
    private static final Logger LOG = LogManager.getLogger(PiDomeLocale.class);

    /**
     * Sets the default locale from the settings file.
     *
     * @throws org.pidome.server.system.config.SystemLocaleException When the
     * locale can not be set.
     */
    public static void setDefaultLocale() throws SystemLocaleException {
        setNewLocale(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "system.locale", "en_US"), false);
        LOG.debug("Running with locale {}", Locale.getDefault().getDisplayName());
    }

    /**
     * Sets a new locale and stores it in the settings file.
     *
     * @param localeName the name of the new locale to store.
     * @param store set to true to store the new setting.
     * @return The set Locale.
     */
    public static Locale setNewLocale(final String localeName, final boolean store) {
        Locale locale = LocaleUtils.getLocaleFromString(localeName);
        LOG.info("Updating default locale input {} to {} as default", localeName, locale.getDisplayVariant());
        Locale.setDefault(locale);
        SystemConfig.setSetting(SystemConfig.Type.SYSTEM, "system.locale", localeName);
        try {
            SystemConfig.storeSettings(SystemConfig.Type.SYSTEM, "save for locale");
        } catch (IOException ex) {
            LOG.error("Could not store locale data", ex);
        }
        return getLocale();
    }

    /**
     * Returns the Locale as a list instead of an array.
     *
     * @return Locale list.
     */
    public static Locale[] getAvailableLocales() {
        return Locale.getAvailableLocales();
    }

    /**
     * Returns the current locale, same as Locale.getDefault();.
     *
     * @return The current locale.
     */
    public static Locale getLocale() {
        return Locale.getDefault();
    }

    /**
     * Private constructor, utility class.
     */
    private PiDomeLocale() {
    }

}
