/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import io.vertx.core.Future;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.server.system.hardware.peripherals.Peripheral;
import org.pidome.server.system.hardware.serial.SerialDevices;
import org.pidome.server.system.hardware.usb.USBDevices;

/**
 * Hardware interface to the collection of peripherals connected to the server.
 *
 * The hardware root is the most parent hub to all the connected peripherals and
 * hardware available systems and subsystems on the running host.
 *
 * This peripheral maintenance class provides the possibility to perform
 * mutations on peripherals and moves peripherals between waiting and active
 * state.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class HardwareRoot implements HardwareMutationListener {

    /**
     * Hardware root logger.
     */
    private static final Logger LOG = LogManager.getLogger(HardwareRoot.class);

    /**
     * A mutation within a peripheral.
     */
    public enum Mutation {
        /**
         * A peripheral has been connected to the hardware.
         */
        ADD,
        /**
         * A peripheral has been removed from the hardware.
         */
        REMOVE,
        /**
         * A peripheral has been updated.
         */
        UPDATE

    }

    /**
     * Collection of available hardware subsystems.
     */
    private final Map<HardwareComponent.Interface, HardwareComponent> availableHardware = Collections.synchronizedMap(new HashMap<>());

    /**
     * I2C Connected devices.
     */
    //I2CDevices I2Cdevices;
    /**
     * Devices used by the server for local actions.
     */
    //ServerDevices serverDevices;
    /**
     * Peripherals which are connected.
     */
    /**
     * Empty hardware root constructor.
     */
    public HardwareRoot() {
        try {
            USBDevices usb = new USBDevices(this);
            availableHardware.put(usb.getIdentifyingInterface(), usb);
        } catch (UnsupportedOperationException ex) {
            LOG.error("USB devices are unsupported on this system [{}]", ex.getMessage());
        }
        try {
            SerialDevices serial = new SerialDevices(this);
            availableHardware.put(serial.getIdentifyingInterface(), serial);
        } catch (UnsupportedOperationException ex) {
            LOG.error("Serial devices are unsupported on this system [{}]", ex.getMessage());
        }
    }

    /**
     * Returns a list of available hardware.
     *
     * @return Hardware available within the system.
     */
    public Map<HardwareComponent.Interface, HardwareComponent> getHardware() {
        return Collections.unmodifiableMap(availableHardware);
    }

    /**
     * Returns the hardware component of the given interface.
     *
     * @param interfaceKey The interface key of the hardware to retrieve.
     * @return The hardware component of the interface.
     */
    public Optional<HardwareComponent> getHardware(final HardwareComponent.Interface interfaceKey) {
        return Optional.ofNullable(this.availableHardware.get(interfaceKey));
    }

    /**
     * Starts the peripheral service.
     *
     * @param completedfuture Future to indicate start is done.
     */
    public void start(final Future<Void> completedfuture) {
        Future<Void> future = Future.future();
        HardwareDriverStore.scan(future.setHandler(result -> {
            if (result.succeeded()) {
                LOG.info("Driver scanning done, registered [{}]", HardwareDriverStore.getRegisteredDrivers());
            } else {
                LOG.error("Scanning for available drivers failed [{}], hardware will continue but without initial drivers.", result.cause().getMessage(), result.cause());
            }
            this.availableHardware.values().forEach((component) -> {
                LOG.info("Starting hardware component [{}]", component);
                try {
                    component.start();
                    LOG.info("Starting discovery of compatible devices on [{}]", component);
                    component.discover();
                } catch (UnsupportedOperationException ex) {
                    LOG.warn("An unsupported features was requested [{}]", ex.getMessage());
                }
            });
            completedfuture.complete();
        }));
    }

    /**
     * @inheritDoc
     */
    @Override
    public void hardwareMutation(final Peripheral<? extends HardwareDriver> peripheral, final Mutation mutation) {
        switch (mutation) {
            case ADD:
                LOG.info("Should load driver for registered peripheral [{}]", peripheral);
                break;
            case REMOVE:
                LOG.info("Should unload driver for registered peripheral [{}]", peripheral);
                break;
            default:
                LOG.error("Unsupported mutation [{}] used.", mutation);
                break;
        }
    }

    /**
     * Stops the peripheral service.
     *
     * @todo stop hardware
     */
    public void stop() {

    }

}
