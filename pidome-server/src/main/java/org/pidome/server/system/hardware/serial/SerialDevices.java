/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.serial;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.HardwareMutationListener;

/**
 * Root class for serial based devices.
 *
 * @author John Sirach
 * @todo finish implementation
 */
public class SerialDevices extends HardwareComponent {

    /**
     * Serial devices logger.
     */
    private static final Logger LOG = LogManager.getLogger(SerialDevices.class);

    /**
     * The single serial device.
     */
    private SerialDevice serialDevice;

    /**
     * Constructor.
     *
     * @param root The hardware root.
     * @throws UnsupportedOperationException When the platform is unsupported.
     */
    public SerialDevices(final HardwareMutationListener root) throws UnsupportedOperationException {
        super(Interface.SERIAL, root);
        if (!PlatformOs.isOs(PlatformOs.OS.LINUX)) {
            LOG.error("Platform [{}] is not (yet) supported", PlatformOs.getOs());
            throw new UnsupportedOperationException("Discovery of serial devices not supported on [" + PlatformOs.getOs() + "]");
        }
    }

    /**
     * Discovery of the serial devices.
     *
     * @throws UnsupportedOperationException When the discovery is unsupported.
     */
    @Override
    public void discover() throws UnsupportedOperationException {
        if (PlatformOs.isOs(PlatformOs.OS.LINUX)) {
            SerialUtils.discoverPorts();
            serialDevice = new SerialDevice();
            serialDevice.setDeviceKey("InternalGPIOSerial");
            serialDevice.setDevicePort("/dev/ttyAMA0");
            serialDevice.setVendorId("PiDome");
            serialDevice.setProductId("GPIOSerial");
            serialDevice.setFriendlyName("Raspberry Pi GPIO Serial");
        } else {
            throw new UnsupportedOperationException("Discovery of serial devices not supported on [" + PlatformOs.getOs() + "]");
        }
    }

    /*
    final synchronized void _fireDeviceEvent(SerialDevice serialDevice, String eventType) {
        LOG.debug("Event: {}", eventType);
        HardwarePeripheralEvent gpioSerialEvent = new HardwarePeripheralEvent(serialDevice, eventType);
        Iterator listeners = getListeners().iterator();
        while (listeners.hasNext()) {
            ((HardwareMutationListener) listeners.next()).deviceMutation(gpioSerialEvent);
        }
    }
     */
    /**
     * Starts the Serial Devices component.
     *
     * @throws UnsupportedOperationException When serial devices are not
     * supported.
     */
    @Override
    public void start() throws UnsupportedOperationException {
        /// Not used, no watchdog available for this interface.
    }

    /**
     * Stops the serial devices component.
     *
     * @throws UnsupportedOperationException When serial devices are not
     * supported.
     */
    @Override
    public void stop() throws UnsupportedOperationException {
        /// Not used, no watchdog available for this interface.
    }

}
