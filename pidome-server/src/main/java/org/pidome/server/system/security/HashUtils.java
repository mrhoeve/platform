/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.security;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Hashing functions.
 *
 * @author John Sirach
 */
public final class HashUtils {

    /**
     * Private constructor, utility class.
     */
    private HashUtils() {
        /// Default constructor.
    }

    /**
     * Base number to use for hashing.
     */
    public static final int BASE_CLASS_HASH_NUMBER = 7;

    /**
     * Base class hashing iterations.
     */
    public static final int BASE_CLASS_HASH_ITERATIONS = 23;

    /**
     * The default hash key length.
     */
    public static final int DEFAULT_HASH_KEY_LENGTH = 256;

    /**
     * The default hash key length.
     */
    public static final int DEFAULT_HASH_ITERATIONS = 11;

    /**
     * the default seed length.
     */
    private static final int DEFAULT_SEED_LENGTH = 30;

    /**
     * Hash a password.
     *
     * @param password The password to hash.
     * @param salt The salt applied.
     * @param iterations The number of iterations.
     * @param keyLength The length of the key.
     * @return a hashed password.
     * @throws IllegalArgumentException Thrown when there is a problem in
     * hashing the password.
     */
    public static byte[] hashPassword(final char[] password, final byte[] salt, final int iterations, final int keyLength) throws IllegalArgumentException {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Generates a random salt.
     *
     * @return A salt.
     * @throws IllegalArgumentException Thrown when there is a problem in salt
     * creation algorithm.
     */
    public static byte[] generateRandomSalt() throws IllegalArgumentException {
        try {
            return SecureRandom.getInstance("SHA1PRNG").generateSeed(DEFAULT_SEED_LENGTH);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
