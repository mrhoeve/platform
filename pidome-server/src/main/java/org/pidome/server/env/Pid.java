/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.env;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.config.SystemConfig;

/**
 * Pid control.
 *
 * @author John
 */
public final class Pid {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(Pid.class);

    /**
     * Current pid id.
     */
    private static String currentPid = "";

    /**
     * Default pid, otherwise new pid when determined on linux based systems.
     */
    private static String pidLocation = "/var/run/";

    /**
     * Name of the pid file on linux based systems.
     */
    private static final String PID_FILE = "pidome.pid";

    /**
     * Creates the application pid.
     *
     * @throws RuntimeException When it is not possible to set pid. The server
     * should not start without it.
     */
    public static void createPid() throws RuntimeException {
        switch (PlatformOs.getOs()) {
            case WINDOWS:
                pidLocation = SystemConfig.getResourcePath() + SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.windows.pid", null);
                setWindowsSelfPid();
                break;
            default:
                pidLocation = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.linux.pid", null);
                setLinuxSelfPid();
                break;
        }
        if (pidLocation == null) {
            throw new RuntimeException("Could not determine pid location, start aborted");
        }
        File pidPath = new File(pidLocation);
        if (PlatformOs.isOs(PlatformOs.OS.WINDOWS) && !pidPath.exists() && !pidPath.mkdirs()) {
            throw new RuntimeException("Could not create windows pid file directory [" + pidLocation + "]");
        }
        pidLocation += PID_FILE;
        File pidFile = new File(pidLocation);
        LOG.debug("Writing pid [{}] to file file [{}]", currentPid, pidLocation);
        try (FileOutputStream stream = new FileOutputStream(pidFile, false)) {
            stream.write(currentPid.getBytes(Charset.forName("US-ASCII")));
        } catch (FileNotFoundException ex) {
            LOG.error("Could not create pid file [{}], aborting because of file issue", pidLocation);
            throw new RuntimeException("Could not create pid file, aborting because of file issue", ex);
        } catch (IOException ex) {
            LOG.error("Could not write to pid file [{}], aborting because of write issue", pidLocation);
            throw new RuntimeException("Could not write to pid file, aborting because of write issue", ex);
        }
        LOG.debug("Running under PID: {}", currentPid);
    }

    /**
     * Tries to set the pid id on windows.
     */
    private static void setWindowsSelfPid() {
        String pidName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (pidName == null || pidName.isEmpty()) {
            throw new RuntimeException("Could not determine process id, halting");
        }
        currentPid = pidName.split("@")[0];
    }

    /**
     * Tries to determine the process id if this (self) process.
     */
    private static void setLinuxSelfPid() {
        File procSelf = new File("/proc/self");
        if (procSelf.exists()) {
            try {
                currentPid = procSelf.getCanonicalFile().getName();
            } catch (Exception e) {
                throw new RuntimeException("Could not determine current pid number, aborting start.", e);
            }
        } else {
            throw new RuntimeException("Could not determine current pid number, can not find proc self file, Start aborted.");
        }
    }

    /**
     * Returns the current pid number.
     *
     * @return The current pid number.
     */
    public static String getPidId() {
        return currentPid;
    }

    /**
     * Unset any pid registration for shutdown purposes.
     */
    public static void shutDown() {
        File pidFile = new File(pidLocation);
        if (pidFile.exists()) {
            LOG.debug("Removing PID [{}] succesfull [{}]", pidLocation, pidFile.delete());
        } else {
            LOG.warn("Can not find PID [{}] for removal. Please do a manual check otherwise there can be startup problems.", pidLocation);
        }
    }

    /**
     * Private constructor, utility class.
     */
    private Pid() {
    }
}
