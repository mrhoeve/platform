/**
 * Package supplying the server environment.
 * <p>
 * Classes supporting the server's environment from the system Locale to PID
 * control.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.env;
