/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.env;

import java.awt.GraphicsEnvironment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.tools.utilities.NumberUtils;

/**
 * Utility class to check current platform.
 *
 * @author John
 * @since 1.0
 */
public final class PlatformOs {

    /**
     * The logger.
     */
    private static final Logger LOG = LogManager.getLogger(PlatformOs.class);

    /**
     * Default string for unknown os'es.
     */
    private static final String UNKNOWN_ITEM = "UNKNONW";

    /**
     * Operating system type enum.
     */
    public enum OS {
        /**
         * Windows type os.
         */
        WINDOWS("Windows"),
        /**
         * Linux type os.
         */
        LINUX("Linux"),
        /**
         * Unknown os.
         */
        UNKNOWN(UNKNOWN_ITEM);

        /**
         * OS platform as string.
         */
        private final String platform;

        /**
         * enum constructor.
         *
         * @param identifiedPlatform OS platform name.
         */
        OS(final String identifiedPlatform) {
            this.platform = identifiedPlatform;
        }

        /**
         * Returns platform name.
         *
         * @return Platform name.
         */
        public String platform() {
            return this.platform;
        }

    }

    /**
     * Operating system type enum.
     */
    public enum ARCH {
        /**
         * 64BIT.
         */
        ARCH_64("amd64"),
        /**
         * 32Bit.
         */
        ARCH_86("x86"),
        /**
         * ARM platform.
         */
        ARCH_ARM("arm"),
        /**
         * Unknown platform.
         */
        UNKNOWN(UNKNOWN_ITEM);

        /**
         * OS platform as string.
         */
        private final String archName;

        /**
         * enum constructor.
         *
         * @param identifiedArchName OS platform name.
         */
        ARCH(final String identifiedArchName) {
            this.archName = identifiedArchName;
        }

        /**
         * Returns platform name.
         *
         * @return Platform name.
         */
        public String archName() {
            return this.archName;
        }
    }

    /**
     * Running OS.
     */
    private static OS os = OS.UNKNOWN;
    /**
     * Running Arch.
     */
    private static ARCH arch = ARCH.UNKNOWN;

    /**
     * Literally reported OS.
     */
    private static String reportedOs = UNKNOWN_ITEM;
    /**
     * The version reported by the OS.
     */
    private static String reportedOsVersion = UNKNOWN_ITEM;
    /**
     * Literally reported ARCH.
     */
    private static String reportedArch = UNKNOWN_ITEM;
    /**
     * If we are running headless or not. By default we assume we are not.
     * unless configuration tells us otherwise.
     */
    private static Boolean headless = false;

    /**
     * Set if we have gathered the information.
     */
    private static boolean reported = false;

    /**
     * True if the OS running on is a raspberry pi.
     *
     * This private should only be modified form the Linux detection mechanism.
     */
    private static boolean raspberryPi = false;

    /**
     * Gathers the OS, ARCH and Headless mode. This method should be ran at boot
     * as it will throw an exception when the OS reported is unsupported.
     *
     * @throws PlatformException Thrown when the OS is unsupported.
     */
    public static void setPlatform() throws PlatformException {
        if (!reported) {
            setReportedOs();
            setReportedArch();
            if (reportedOs.startsWith(OS.WINDOWS.platform)) {
                os = OS.WINDOWS;
                System.setProperty("com.resource.dir", "resources");
            } else if (reportedOs.startsWith("Linux")) {
                os = OS.LINUX;
                final File file = new File("/etc", "os-release");
                try (FileInputStream fis = new FileInputStream(file);
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis, "UTF-8"))) {
                    bufferedReader.lines().forEachOrdered(line -> {
                        String[] splitted = line.split("=");
                        if (splitted.length == NumberUtils.INTEGER_TWO) {
                            if (splitted[0].toLowerCase(Locale.US).contains("version")) {
                                reportedOsVersion = splitted[1].replace("\"", "");
                            } else if (splitted[0].toLowerCase(Locale.US).contains("name")) {
                                reportedOs = splitted[1].replace("\"", "");
                                if (splitted[1].replace("\"", "").toLowerCase(Locale.US).startsWith("raspbian")) {
                                    raspberryPi = true;
                                }
                            }
                        }
                    });
                } catch (final Exception e) {
                    LOG.warn("Unable to determine if running on a raspberry pi or not", e);
                }
            } else {
                throw new PlatformException("Your OS '" + reportedOs + "' is unsupported.");
            }
            for (ARCH archValue : ARCH.values()) {
                if (archValue.archName().equals(reportedArch)) {
                    arch = archValue;
                    break;
                }
            }
            if (arch.equals(ARCH.UNKNOWN)) {
                throw new PlatformException("Your architecture '" + reportedArch + "' on '" + reportedOs + "' is unsupported.");
            }
            setIsHeadless();
            reported = true;
        }
    }

    /**
     * Sets OS.
     */
    private static void setReportedOs() {
        reportedOs = System.getProperty("os.name");
    }

    /**
     * Sets ARCH.
     */
    private static void setReportedArch() {
        reportedArch = System.getProperty("os.arch");
    }

    /**
     * Set if in headless mode.
     */
    private static void setIsHeadless() {
        headless = GraphicsEnvironment.isHeadless();
    }

    /**
     * Returns system reported arch.
     *
     * @return system reported arch.
     */
    public static String getReportedArch() {
        return reportedArch;
    }

    /**
     * Returns system reported OS.
     *
     * @return system reported OS.
     */
    public static String getReportedOs() {
        return reportedOs;
    }

    /**
     * Returns the System reported OS version.
     *
     * @return the OS version.
     */
    public static String getReportedOsVersion() {
        return reportedOsVersion;
    }

    /**
     * Returns if headless.
     *
     * @return if headless.
     */
    public static Boolean isHeadLess() {
        return headless;
    }

    /**
     * Returns the gathered OS enum.
     *
     * @return The gathered OS.
     */
    public static OS getOs() {
        return os;
    }

    /**
     * Check if the given OS is the registered OS.
     *
     * @param osCheck The OS to check against the current.
     * @return true if the current OS equals the given OS.
     */
    public static boolean isOs(final OS osCheck) {
        return osCheck.equals(os);
    }

    /**
     * Returns the gathered arch enum.
     *
     * @return the gathered arch.
     */
    public static ARCH getArch() {
        return arch;
    }

    /**
     * Check if the given ARCH is the registered OS.
     *
     * @param checkArch The ARCH to check against the current.
     * @return true if the current ARCH equals the given ARCH.
     */
    public static boolean isArch(final ARCH checkArch) {
        return checkArch.equals(arch);
    }

    /**
     * @return the raspberryPi
     */
    public static boolean isRaspberryPi() {
        return raspberryPi;
    }

    /**
     * private constructor, utility class.
     */
    private PlatformOs() {
    }

}
