/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.env;

/**
 * Exception used with the platform.
 *
 * @author John Sirach
 */
public final class PlatformException extends Exception {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Constructs the exception.
     */
    public PlatformException() {
        /// Default constructor.
    }

    /**
     * Constructs the exception with a message.
     *
     * @param message The message to include.
     */
    public PlatformException(final String message) {
        super(message);
    }

    /**
     * Constructs the exception with a <code>Throwable</code>.
     *
     * @param cause The throwable cause.
     */
    public PlatformException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs the exception with a message and a <code>Throwable</code>
     * cause.
     *
     * @param message The message to include.
     * @param cause The throwable cause.
     */
    public PlatformException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
