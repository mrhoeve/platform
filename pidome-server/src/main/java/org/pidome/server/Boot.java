package org.pidome.server;

import java.net.UnknownHostException;
import java.util.Locale;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.env.Pid;
import org.pidome.server.env.PlatformException;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.system.config.PiDomeLocale;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.config.SystemConfigException;
import org.pidome.server.system.config.SystemLocaleException;

/**
 * The PiDome server boot class.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class Boot {

    /**
     * No constructor publicized.
     */
    private Boot() {
    }

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(Boot.class);

    /**
     * @param args the command line arguments
     * @throws PlatformException When running platform is unsupported.
     * @throws SystemConfigException When the system configuration is invalid.
     * @throws UnknownHostException when the local host can not be determined
     * which is required
     * @since 1.0
     */
    public static void main(final String[] args) throws PlatformException, SystemConfigException, UnknownHostException {
        //SystemConfig.setLogLevel(Level.INFO);
        LOG.info("Server initialization");
        LOG.info("Running from [{}]", System.getProperty("user.dir"));
        Thread.setDefaultUncaughtExceptionHandler((Thread t, Throwable e) -> {
            LOG.error("An unhandled exception thrown in [{}]:", t, e);
        });
        for (String s : args) {
            switch (s) {
                case "debug":
                    SystemConfig.setLogLevel(Level.DEBUG);
                    break;
                case "trace":
                    SystemConfig.setLogLevel(Level.TRACE);
                    break;
                default:
                    break;
            }
        }
        SystemConfig.initialize();
        Pid.createPid();
        try {
            PiDomeLocale.setDefaultLocale();
        } catch (SystemLocaleException ex) {
            LOG.warn("No default locale set, update the configuration with [system.locale], using default [{}]", Locale.getDefault(), ex.getMessage());
        }
        String serverBuild = new StringBuilder(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_MAJOR", "0"))
                .append(".")
                .append(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_MINOR", "0"))
                .append(".")
                .append(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_PATCH", "0"))
                .append(", Build: ")
                .append(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_BUILD", "0"))
                .toString();
        LOG.info("Server initialization done");
        LOG.info("Starting server verion [{}] [headless:{}] with Java [{}] on [{}] with ARCH [{}], Locale [{}] and PID: [{}]",
                serverBuild, PlatformOs.isHeadLess(), System.getProperty("java.version"), PlatformOs.getReportedOs(), PlatformOs.getReportedArch(), Locale.getDefault(), Pid.getPidId());
        ServiceHandler.getInstance().servicesBoot();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                this.setName("shutdown");
                LOG.info("Server shutdown instantiated");
                ServiceHandler.getInstance().servicesShutdown();
                Pid.shutDown();
                LOG.info("Goodbye, see you soon");
            }
        });
        /*
        if(SystemConfig.getBoolSetting(SystemConfig.Type.SYSTEM, "server.veryfirstrun", false) == true){
            SystemConfig.setBoolSetting(SystemConfig.Type.SYSTEM, "server.veryfirstrun", false);
            try {
                SystemConfig.storeSettings(SystemConfig.Type.SYSTEM);
            } catch (IOException ex) {
                LOG.error("Could not register first run done. If a future save succeeds settings will be saved, otherwise current settings are temporary [{}]", ex.getMessage());
            }
        }
         */
    }
}
