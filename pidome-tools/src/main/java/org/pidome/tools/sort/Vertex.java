/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.sort;

/**
 * Class used for topological sorting.
 *
 * @author John Sirach
 * @param <T> The type of vertex.
 */
class Vertex<T> {

    /**
     * An item to be sorted.
     */
    private final transient T vertexItem;

    /**
     * Constructor setting the item to be sorted.
     *
     * @param item An item to be sorted.
     */
    Vertex(final T item) {
        vertexItem = item;
    }

    /**
     * Get the vertex item.
     *
     * @return The vertex item of type <code>T</code>
     */
    public T get() {
        return this.vertexItem;
    }
}
