/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.sort;

/**
 * Thrown when a circular dependency is encountered in a Topological sort.
 *
 * @author John Sirach
 */
public class TopologicalSortCycleException extends Exception {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>TopologicalSortCycleException</code>
     * without detail message.
     */
    public TopologicalSortCycleException() {
        /// Default constructor without reason.
    }

    /**
     * Constructs an instance of <code>TopologicalSortCycleException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public TopologicalSortCycleException(final String msg) {
        super(msg);
    }
}
