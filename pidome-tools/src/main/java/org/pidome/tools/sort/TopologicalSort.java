/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.sort;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Provides a topological sort for the given items.
 *
 * @author John Sirach
 * @param <T> The item type to be sorted.
 */
@SuppressWarnings("unchecked")
public class TopologicalSort<T> {

    /**
     * Maximum amount of vertexes.
     */
    private static final int MAX_VERTS = 20;

    /**
     * List of vertices.
     */
    private final transient Vertex<T>[] vertexList = (Vertex<T>[]) Array.newInstance(Vertex.class, MAX_VERTS);

    /**
     * Adjacency matrix.
     */
    private final transient int[][] matrix = new int[MAX_VERTS][MAX_VERTS];

    /**
     * Current number of vertices.
     */
    private transient int numVerts;

    /**
     * To map the items to be sorted to a position.
     */
    private final transient Map<T, Integer> vertexMap = new HashMap<>();

    /**
     * Constructor initializing.
     */
    public TopologicalSort() {
        numVerts = 0;
        for (int i = 0; i < MAX_VERTS; i++) {
            for (int k = 0; k < MAX_VERTS; k++) {
                matrix[i][k] = 0;
            }
        }
    }

    /**
     * Adds a vertex.
     *
     * @param item The item to be sorted.
     */
    public void addVertex(final T item) {
        vertexMap.put(item, vertexMap.size());
        vertexList[numVerts++] = new Vertex<>(item);
    }

    /**
     * Adds a vertex.
     *
     * @param items The items to be sorted.
     */
    public void addVertexSet(final Set<T> items) {
        items.stream().forEach(item -> {
            vertexMap.put(item, vertexMap.size());
            vertexList[numVerts++] = new Vertex<>(item);
        });
    }

    /**
     * Adds a dependency.
     *
     * @param dependency The dependency the depending relies on.
     * @param depending The depending item.
     */
    public void addEdge(final T dependency, final T depending) {
        matrix[vertexMap.get(dependency)][vertexMap.get(depending)] = 1;
    }

    /**
     * Returns the vertex a specific position.
     *
     * @param v The index to get.
     * @return The item at the given index.
     */
    public T getVertex(final int v) {
        return vertexList[v].get();
    }

    /**
     * Perform the topological sort.
     *
     * @return The sorted list.
     * @throws org.pidome.tools.sort.TopologicalSortCycleException When a
     * circular dependency is detected.
     */
    public List<T> sort() throws TopologicalSortCycleException {
        List<T> sortedList = new ArrayList<>();
        while (numVerts > 0) {
            int currentVertex = noSuccessors();
            if (currentVertex == -1) {
                throw new TopologicalSortCycleException("Graph has cycles, check your dependency chain");
            }
            sortedList.add(vertexList[currentVertex].get());
            deleteVertex(currentVertex);
        }
        return sortedList;
    }

    /**
     * Returns the vertex without successors.
     *
     * @return Vertex without successor or -1 if there is no such.
     */
    public int noSuccessors() {
        boolean isEdge;
        for (int row = 0; row < numVerts; row++) {
            isEdge = false;
            for (int col = 0; col < numVerts; col++) {
                if (matrix[row][col] > 0) {
                    isEdge = true;
                    break;
                }
            }
            if (!isEdge) {
                return row;
            }
        }
        return -1; // no
    }

    /**
     * Deletes given vertex.
     *
     * @param delVert The vertex index to delete.
     */
    public void deleteVertex(final int delVert) {
        if (delVert != numVerts - 1) {
            for (int j = delVert; j < numVerts - 1; j++) {
                vertexList[j] = vertexList[j + 1];
            }

            for (int row = delVert; row < numVerts - 1; row++) {
                moveRowUp(row, numVerts);
            }

            for (int col = delVert; col < numVerts - 1; col++) {
                moveColLeft(col, numVerts - 1);
            }
        }
        numVerts--;
    }

    /**
     * Moves a row up.
     *
     * @param row The row.
     * @param length The length
     */
    private void moveRowUp(final int row, final int length) {
        for (int col = 0; col < length; col++) {
            matrix[row][col] = matrix[row + 1][col];
        }
    }

    /**
     * Moves a column left.
     *
     * @param col The column.
     * @param length The length.
     */
    private void moveColLeft(final int col, final int length) {
        for (int row = 0; row < length; row++) {
            matrix[row][col] = matrix[row][col + 1];
        }
    }
}
