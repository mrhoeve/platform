/**
 * Sorting utilities.
 * <p>
 * Provides classes which supports special case sortings.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.tools.sort;
