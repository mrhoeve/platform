/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utilities on network operations.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class NetworkUtils {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(NetworkUtils.class);

    /**
     * The default timeout time in milliseconds for network actions.
     */
    private static final int DEFAULT_TIMEOUT = 1000;

    /**
     * Private constructor.
     *
     * Static utility methods.
     */
    private NetworkUtils() {
    }

    /**
     * Checks if a remote port is available or not.
     *
     * The default is to check for one second.
     *
     * @param remoteHost The remote host to check This method must be in the
     * form of "host:port".
     * @return true when available, false when not, or undetermined.
     */
    public static boolean remotePortAvailable(final String remoteHost) {
        if (remoteHost.contains(":")) {
            return NetworkUtils.remotePortAvailable(remoteHost.split(":")[0], Integer.parseInt(remoteHost.split(":")[1]), DEFAULT_TIMEOUT);
        } else {
            return false;
        }
    }

    /**
     * Checks if a remote port is available or not.
     *
     * The default is to check for one second.
     *
     * @param remoteHost The remote host to check.
     * @param port The port to check.
     * @return true when available, false when not, or undetermined.
     */
    public static boolean remotePortAvailable(final String remoteHost, final int port) {
        return NetworkUtils.remotePortAvailable(remoteHost, port, DEFAULT_TIMEOUT);
    }

    /**
     * Checks if a remote port is available or not.
     *
     * @param remoteHost The remote host to check.
     * @param port The port to check.
     * @param timeout The timeout to stop checking.
     * @return true when available, false when not, or undetermined.
     */
    public static boolean remotePortAvailable(final String remoteHost, final int port, final int timeout) {
        try (
                 Socket socket = new Socket()) {
            socket.setReuseAddress(true);
            SocketAddress sa = new InetSocketAddress(remoteHost, port);
            socket.connect(sa, timeout);
            return true;
        } catch (SocketTimeoutException e) {
            LOG.warn("Port [{}] on [{}] is timing out in creating connection (timeout: {})", port, remoteHost, timeout, e);
        } catch (UnknownHostException e) {
            LOG.warn("Port [{}] on [{}] is unavailable remote host not known", port, remoteHost, e);
        } catch (IOException e) {
            if (e.getMessage().equals("Connection refused")) {
                LOG.warn("Port [{}] on [{}] refused connection", port, remoteHost);
            } else {
                LOG.warn("Unable to check port [{}] on [{}]", port, remoteHost, e);
            }
        }
        return false;
    }

}
