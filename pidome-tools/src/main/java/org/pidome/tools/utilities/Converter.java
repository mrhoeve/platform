/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import java.nio.ByteBuffer;
import java.util.Locale;

/**
 * Various methods to support some conversions between datatypes.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class Converter {

    /**
     * 0xff.
     */
    private static final int BYTE_255 = 0xff;

    /**
     * 16.
     */
    private static final int RADIX_16 = 16;

    /**
     * Private constructor in utility class.
     */
    private Converter() {
        /// Default private constructor.
    }

    /**
     * Adds zeros before a string to result in a fixed string length.
     *
     * Example usage:
     *
     * <code>String sMyNumber = "10.00";
     * String sNumberWithZerosLeading = MiscImpl.setZeroLead(sMyNumber, sMyNumber.length()+2); /// to add two leading zeros
     * String sNumberWithZerosLeading = MiscImpl.setZeroLead(sMyNumber, 5); /// Will add no zeros because length is already 5, but if sMyNumber="1.00" it will add one zero resulting in "01.00"</code>
     *
     * @param number Number as string
     * @param lengthWithLead The total length to return including leading zeros
     * @return string with zeros if needed
     */
    public static String setZeroLeading(final String number, final int lengthWithLead) {
        StringBuilder returnNumber = new StringBuilder(number);
        while (returnNumber.length() < lengthWithLead) {
            returnNumber.insert(0, "0");
        }
        return returnNumber.toString();
    }

    /**
     * Converts a byte array to a float.
     *
     * @param b The 4 byte byte array to convert
     * @return float value
     */
    public static float byteArrayToFloat(final byte[] b) {   // Byte to float conversion
        return ByteBuffer.wrap(b).getFloat();
    }

    /**
     * Converts a byte array to a double.
     *
     * @param b The 8 byte byte array to convert
     * @return double value
     */
    public static double byteArrayToDouble(final byte[] b) {   // Byte to float conversion
        return ByteBuffer.wrap(b).getDouble();
    }

    /**
     * Puts a byte array into a hex string.
     *
     * @param array The array of bytes to convert to a string.
     * @return The string converted from the byte array.
     */
    public static String byteArrayToHexString(final byte[] array) {
        StringBuilder sb = new StringBuilder();
        for (byte b : array) {
            int v = b & BYTE_255;
            if (v < RADIX_16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase(Locale.US);
    }

}
