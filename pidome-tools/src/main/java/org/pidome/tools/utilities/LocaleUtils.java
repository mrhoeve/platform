/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import java.util.Locale;

/**
 * Utility methods on behalf of Locale.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class LocaleUtils {

    /**
     * Private constructor, utility class.
     */
    private LocaleUtils() {
    }

    /**
     * Returns a Locale based on the given locale string.
     *
     * Following formats is supported:
     *
     * "default": Returns default locale.
     *
     * "language[_country[_variant]]": Returns Locale based on language,
     * language and country or language, country and variant.
     *
     * @param localeString String to extract the locale from.
     * @return The locale based on the string.
     */
    public static Locale getLocaleFromString(final String localeString) {
        if (localeString == null) {
            return null;
        }
        if ("default".equalsIgnoreCase(localeString)) {
            return Locale.getDefault();
        }

        // Extract language
        int languageIndex = localeString.indexOf('_');
        String language;
        if (languageIndex == -1) {
            // No further "_" so is "{language}" only
            return new Locale(localeString, "");
        } else {
            language = localeString.substring(0, languageIndex);
        }

        // Extract country
        int countryIndex = localeString.indexOf('_', languageIndex + 1);
        String country;
        if (countryIndex == -1) {
            // No further "_" so is "{language}_{country}"
            country = localeString.substring(languageIndex + 1);
            return new Locale(language, country);
        } else {
            // Assume all remaining is the variant so is "{language}_{country}_{variant}"
            country = localeString.substring(languageIndex + 1, countryIndex);
            String variant = localeString.substring(countryIndex + 1);
            return new Locale(language, country, variant);
        }
    }

}
