/**
 * Utility classes used server wide.
 * <p>
 * These utility classes provide a broad range of utility functionalities.
 *
 * Most of these classes are classes you must use when implementing
 * functionalities. For example the color translations are depending on these
 * classes.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.tools.utilities;
