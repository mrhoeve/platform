/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

/**
 * Utility class for referencing numbers.
 *
 * @author johns
 */
public final class NumberUtils {

    /**
     * Non instantiatable constructor.
     */
    private NumberUtils() {
        /// private default constructor.
    }

    /**
     * Integer zero.
     */
    public static final int INTEGER_ZERO = 0;

    /**
     * Integer one.
     */
    public static final int INTEGER_ONE = 1;

    /**
     * Integer two.
     */
    public static final int INTEGER_TWO = 2;

    /**
     * Long zero.
     */
    public static final long LONG_ZERO = 0L;

    /**
     * Long one.
     */
    public static final long LONG_ONE = 1L;

    /**
     * Long two.
     */
    public static final long LONG_TWO = 2L;

}
