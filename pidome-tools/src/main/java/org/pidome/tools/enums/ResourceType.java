/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.enums;

/**
 * Resource type enums. This resource type is defacto to be used to identify
 * resource types across all pidome projects.
 *
 * @author John
 */
public enum ResourceType {
    /**
     * A file system resource type.
     */
    FILESYSTEM,
    /**
     * A network based resource type.
     */
    NETWORK,
    /**
     * A local memory based resource type.
     */
    MEMORY,
    /**
     * A local socket resource type.
     */
    SOCK;
}
