/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.enums;

/**
 * Color type enums used.
 *
 * @author John Sirach
 */
public enum ColorType {

    /**
     * RGB color type.
     */
    RGB,
    /**
     * HSB color type.
     */
    HSB,
    /**
     * Hexadecimal color type.
     */
    HEX

}
