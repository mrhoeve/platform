/**
 * Root package for full platform wide used enums.
 * <p>
 * Provides purposed enums.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.tools.enums;
