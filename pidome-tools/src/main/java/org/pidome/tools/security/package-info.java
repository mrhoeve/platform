/**
 * Security utilities.
 * <p>
 * Provides classes for security purposes. Think of hashing etc.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.tools.security;
