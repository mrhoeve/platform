/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.security;

/**
 * Hashing utilities.
 *
 * @author John Sirach
 */
public final class HashUtils {

    /**
     * Private constructor, utility class.
     */
    private HashUtils() {
    }

    /**
     * Create a SHA-256 hash.
     *
     * The string will be hashed with the UTF-8 Character set.
     *
     * @param toHash The string to hash.
     * @return The hashed string.
     * @throws Exception When encryption fails.
     */
    public static byte[] createSha256Hash(final String toHash) throws Exception {
        java.security.MessageDigest digest = java.security.MessageDigest.getInstance("SHA-256");
        digest.reset();
        digest.update(toHash.getBytes("UTF-8"));
        return digest.digest();
    }
}
