package org.pidome.tools.properties;

/**
 * The String property binding.
 *
 * @author John
 */
public final class StringPropertyBinding extends ObjectPropertyBinding<String> {

    /**
     * Property constructor.
     *
     * Defaults to <code>null</code>.
     *
     */
    public StringPropertyBinding() {
        super(null);
    }

    /**
     * Property constructor.
     *
     * @param initialValue The initial value for the property.
     */
    public StringPropertyBinding(final String initialValue) {
        super(initialValue);
    }

    /**
     * Safe way to return a non default value initialized
     * <code>StringPropertyBinding</code>.
     *
     * @return A safe (blank) value when initialized without initial value.
     */
    public String getValueSafe() {
        if (getValue() == null) {
            return "";
        } else {
            return getValue();
        }
    }

}
