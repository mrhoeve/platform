/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A property binding bean for typed objects.
 *
 * @author John Sirach
 * @param <T> The type to use.
 * @since 1.0
 */
public class ObjectPropertyBinding<T> implements PropertyChangeListener {

    /**
     * The object property.
     */
    private final ObjectProperty<T> property;

    /**
     * List of listeners to be notified when <code>Type</code> is updated.
     */
    private final transient List<ObjectPropertyBindingListener<T>> listeners = new ArrayList<>();

    /**
     * The minimum required listeners before a property change listener is
     * instantiated.
     */
    private static final int MINIMUM_LISTENERS_REQUIRED = 1;

    /**
     * Constructor with empty value.
     */
    public ObjectPropertyBinding() {
        this.property = new ObjectProperty<>();
    }

    /**
     * Constructor with initial value.
     *
     * @param initialValue The initial value to set.
     */
    public ObjectPropertyBinding(final T initialValue) {
        this.property = new ObjectProperty<>(initialValue);
    }

    /**
     * Sets a new value.
     *
     * @param newValue The new value to set.
     */
    public final void setValue(final T newValue) {
        this.property.setValue(newValue);
    }

    /**
     * Shortcut for setValue.
     *
     * @param newValue The new value to set.
     */
    public final void set(final T newValue) {
        setValue(newValue);
    }

    /**
     * Returns the property value.
     *
     * @return The current property value.
     */
    public final T getValue() {
        return this.property.getValue();
    }

    /**
     * Shortcut for getValue.
     *
     * @return The current property value
     */
    public final T get() {
        return getValue();
    }

    /**
     * Adds a pure property listener.
     *
     * @param listener The object capable for listening to changes in the
     * property value.
     */
    public final void addListener(final ObjectPropertyBindingListener<T> listener) {
        synchronized (this.listeners) {
            if (!this.listeners.contains(listener)) {
                this.listeners.add(listener);
            }
            if (this.listeners.size() == MINIMUM_LISTENERS_REQUIRED) {
                this.property.getProperty().addPropertyChangeListener(this);
            }
        }
    }

    /**
     * Removes a pure property listener.
     *
     * @param listener The object capable for listening to changes in the
     * property value.
     */
    public final void removeListener(final ObjectPropertyBindingListener<T> listener) {
        synchronized (this.listeners) {
            this.listeners.remove(listener);
            if (this.listeners.isEmpty()) {
                this.property.getProperty().removePropertyChangeListener(this);
            }
        }
    }

    /**
     * Update listeners to receive the updated property.
     *
     * @param pce The property change event fired from the underlying property.
     */
    @Override
    @SuppressWarnings("unchecked")
    public final void propertyChange(final PropertyChangeEvent pce) {
        synchronized (this.listeners) {
            Iterator<ObjectPropertyBindingListener<T>> iterator = this.listeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().handle((T) pce.getNewValue(), (T) pce.getOldValue());
            }
        }
    }

    /**
     * Returns the underlying property.
     *
     * @return The underlying property in this binding.
     */
    protected final ObjectProperty<T> getProperty() {
        return this.property;
    }

}
