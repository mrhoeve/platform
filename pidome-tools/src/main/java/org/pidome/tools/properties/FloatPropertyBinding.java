package org.pidome.tools.properties;

/**
 * A boolean property class.
 *
 * @author John Sirach
 * @since 1.0
 */
public class FloatPropertyBinding extends ObjectPropertyBinding<Float> {

    /**
     * Property constructor.
     *
     * Defaults to <code>null</code>.
     *
     */
    public FloatPropertyBinding() {
        super(null);
    }

    /**
     * Property constructor.
     *
     * @param initialValue The initial value for the property.
     */
    public FloatPropertyBinding(final Float initialValue) {
        super(initialValue);
    }

}
