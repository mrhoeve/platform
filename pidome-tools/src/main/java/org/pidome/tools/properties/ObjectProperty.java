/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import java.beans.PropertyChangeSupport;

/**
 * A simple object property.
 *
 * This class is an internal class used for the property bindings package.
 *
 * @author John Sirach
 * @param <T> The type to use in this property.
 */
public class ObjectProperty<T> {

    /**
     * Holds the object current value.
     */
    private T value;

    /**
     * Property change support listener.
     */
    private final transient PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Empty constructor.
     */
    protected ObjectProperty() {
        /// Default constructor for initializing an empty instance.
    }

    /**
     * Constructor for initial value.
     *
     * @param initialValue The initial value.
     */
    protected ObjectProperty(final T initialValue) {
        this.value = initialValue;
    }

    /**
     * Returns the property change support.
     *
     * @return Returns the property.
     */
    protected final PropertyChangeSupport getProperty() {
        return propertyChangeSupport;
    }

    /**
     * Returns the current value.
     *
     * @return The current known value from the property.
     */
    protected final T getValue() {
        return value;
    }

    /**
     * Set's the property value and notifies listeners.
     *
     * Must call sync(newValue) after all the listeners have been notified.
     *
     * @param newValue The value to propagate to listeners.
     */
    protected final void setValue(final T newValue) {
        T oldValue = this.value;
        this.value = newValue;
        propertyChangeSupport.firePropertyChange("value", oldValue, newValue);
    }

}
