package org.pidome.tools.properties;

/**
 * An integer property for binding.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class IntegerPropertyBinding extends ObjectPropertyBinding<Integer> {

    /**
     * Property constructor.
     *
     * Defaults to <code>null</code>.
     *
     */
    public IntegerPropertyBinding() {
        super(null);
    }

    /**
     * Property constructor.
     *
     * @param initialValue The initial value for the property.
     */
    public IntegerPropertyBinding(final Integer initialValue) {
        super(initialValue);
    }

}
