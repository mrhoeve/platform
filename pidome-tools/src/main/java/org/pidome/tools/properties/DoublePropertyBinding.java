package org.pidome.tools.properties;

/**
 * A boolean property class.
 *
 * @author John Sirach
 * @since 1.0
 */
public class DoublePropertyBinding extends ObjectPropertyBinding<Double> {

    /**
     * Property constructor.
     *
     * Defaults to <code>null</code>.
     *
     */
    public DoublePropertyBinding() {
        super(null);
    }

    /**
     * Property constructor.
     *
     * @param initialValue The initial value for the property.
     */
    public DoublePropertyBinding(final Double initialValue) {
        super(initialValue);
    }

}
