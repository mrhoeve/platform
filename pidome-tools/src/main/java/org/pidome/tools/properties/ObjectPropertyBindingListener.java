/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

/**
 * The interface for listeners who want to listen for object property changes.
 *
 * @author John Sirach
 * @param <T> The type for the interface.
 */
@FunctionalInterface
public interface ObjectPropertyBindingListener<T> {

    /**
     * Handle a object property change.
     *
     * @param newValue The new value
     * @param oldValue The old value
     */
    void handle(T newValue, T oldValue);

}
