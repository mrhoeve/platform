/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

/**
 * Boolean property change listener.
 *
 * @author John Sirach
 * @since 1.0
 */
public interface FloatPropertyBindingListener extends ObjectPropertyBindingListener<Float> {

}
