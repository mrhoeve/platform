/**
 * Property bean utilities.
 * <p>
 * Provides interfaces and classes to be able to use listener beans, both
 * writable and read only bean properties are supportd.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.tools.properties;
