package org.pidome.tools.properties;

/**
 * A boolean property class.
 *
 * @author John Sirach
 * @since 1.0
 */
public class BooleanPropertyBinding extends ObjectPropertyBinding<Boolean> {

    /**
     * Property constructor.
     *
     * Defaults to <code>Boolean.FALSE</code>.
     *
     */
    public BooleanPropertyBinding() {
        super(Boolean.FALSE);
    }

    /**
     * Property constructor.
     *
     * @param initialValue The initial value for the property.
     */
    public BooleanPropertyBinding(final Boolean initialValue) {
        super(initialValue);
    }

}
