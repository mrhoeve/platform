/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import java.util.Objects;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;

/**
 * Test boolean property binding.
 *
 * @author johns
 */
public class DoublePropertyBindingTest {

    public DoublePropertyBindingTest() {
    }

    @Test
    public void testDoubleBindingWithDefaultValueAndUpdate() {
        DoublePropertyBinding binding = new DoublePropertyBinding();
        if (!Objects.equals(binding.getValue(), null)) {
            fail("Double property default to null fails.");
        }
        binding.setValue(Double.MAX_VALUE);
    }

    @Test
    public void testDoubleBindingWithInitialValueAndUpdate() {
        DoublePropertyBinding binding = new DoublePropertyBinding(Double.MIN_VALUE);
        if (!Objects.equals(binding.getValue(), Double.MIN_VALUE)) {
            fail("Boolean property with initial value fails.");
        }
        binding.addListener((newValue, oldValue) -> {
            if (newValue == oldValue) {
                fail("Binding update with same value should not trigger listener");
            }
        });
        binding.setValue(Double.MAX_VALUE);
        if (!Objects.equals(binding.getValue(), Double.MAX_VALUE)) {
            fail("Double property value update fails as new value.");
        }
        binding.setValue(Double.MAX_VALUE);
    }

}
