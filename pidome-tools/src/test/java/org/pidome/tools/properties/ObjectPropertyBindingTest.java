/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import java.util.Objects;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;

/**
 * Test boolean property binding.
 *
 * @author johns
 */
public class ObjectPropertyBindingTest {

    /**
     * Constructor.
     */
    public ObjectPropertyBindingTest() {
    }

    /**
     * Test unchecked property changes.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testObjectPropertySetGetSupportUnchecked() {
        final PlainObject test1 = new PlainObject();
        final PlainObject test2 = new PlainObject();
        ObjectProperty emptyObject = new ObjectProperty(test1);
        assertThat(emptyObject.getValue(), is(equalTo(test1)));
        emptyObject.setValue(test2);
        assertThat(emptyObject.getValue(), is(equalTo(test2)));
    }

    /**
     * Test property change support.
     */
    @Test
    public void testObjectPropertyChangeSupport() {
        final PlainObject test1 = new PlainObject();
        ObjectProperty<PlainObject> emptyObject = new ObjectProperty<PlainObject>();
        emptyObject.getProperty().addPropertyChangeListener(change -> {
            assertThat(change.getOldValue(), is(equalTo(null)));
            assertThat(change.getNewValue(), is(equalTo(test1)));
        });
        emptyObject.setValue(test1);
    }

    /**
     * Test binding with generics.
     */
    @Test
    public void testObjectBindingWithGenericsValue() {
        final PlainObject test1 = new PlainObject();
        final PlainObject test2 = new PlainObject();

        ObjectPropertyBinding<PlainObject> binding = new ObjectPropertyBinding<PlainObject>(test1);
        if (!Objects.equals(binding.getValue(), test1)) {
            fail("Object property default to false fails.");
        }
        binding.addListener((newValue, oldValue) -> {
            if (Objects.equals(newValue, oldValue)) {
                fail("Object update should not trigger listener");
            }
        });
        binding.set(test2);
        if (!binding.getValue().equals(binding.get())) {
            fail("Object property get and getValue are not equal.");
        }
        if (!Objects.equals(binding.getValue(), test2)) {
            fail("Object property value update fails as new value.");
        }
    }

    /**
     * Pointer to test listener method.
     */
    private ObjectPropertyBindingListener testListener = this::testListener;

    /**
     * Test listener.
     *
     * Empty method.
     *
     * @param oldValue The old value.
     * @param newValue The new value.
     */
    private void testListener(Object oldValue, Object newValue) {

    }

    /**
     * Test the listeners count on the underlying property.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testObjectBindingListenersCount() {
        ObjectPropertyBinding binding = new ObjectPropertyBinding();
        binding.addListener(testListener);
        if (binding.getProperty().getProperty().getPropertyChangeListeners().length != 1) {
            fail("Property listener not registered.");
        }
        binding.removeListener(testListener);
        if (binding.getProperty().getProperty().getPropertyChangeListeners().length != 0) {
            fail("Property listener not removed.");
        }
    }

    /**
     * Test class used for change support.
     */
    private static class PlainObject extends Object {
    }

}
