/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import java.util.Objects;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;

/**
 * Test boolean property binding.
 *
 * @author johns
 */
public class BooleanPropertyBindingTest {

    public BooleanPropertyBindingTest() {
    }

    @Test
    public void testBooleanBindingWithDefaultValue() {
        BooleanPropertyBinding binding = new BooleanPropertyBinding();
        if (!Objects.equals(binding.getValue(), Boolean.FALSE)) {
            fail("Boolean property default to false fails.");
        }
    }

    @Test
    public void testBooleanBindingWithInitialValueAndUpdate() {
        BooleanPropertyBinding binding = new BooleanPropertyBinding(Boolean.TRUE);
        if (!Objects.equals(binding.getValue(), Boolean.TRUE)) {
            fail("Boolean property with initial value fails.");
        }
        binding.addListener((newValue, oldValue) -> {
            if (newValue.booleanValue() == oldValue.booleanValue()) {
                fail("Binding update should not trigger listener");
            }
        });
        binding.setValue(Boolean.FALSE);
        if (!Objects.equals(binding.getValue(), Boolean.FALSE)) {
            fail("Boolean property value update fails as new value.");
        }
        binding.setValue(Boolean.FALSE);
    }

}
