/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;

/**
 * Test boolean property binding.
 *
 * @author johns
 */
public class StringPropertyBindingTest {

    public StringPropertyBindingTest() {
    }

    @Test
    public void testStringBindingWithDefaultValue() {
        StringPropertyBinding binding = new StringPropertyBinding();
        assertThat(binding.getValue(), is(equalTo(null)));
        assertThat(binding.getValueSafe(), is(equalTo("")));
    }

    @Test
    public void testStringBindingWithInitialValueAndUpdate() {
        final String testValue = "Test value";
        final String newTestValue = "New test value";
        StringPropertyBinding binding = new StringPropertyBinding(testValue);

        assertThat(binding.getValue(), is(equalTo(testValue)));
        assertThat(binding.getValueSafe(), is(equalTo(testValue)));

        binding.addListener((newValue, oldValue) -> {
            assertThat(oldValue, is(equalTo(testValue)));
            assertThat(newValue, is(equalTo(newTestValue)));
        });
        binding.setValue(newTestValue);

        assertThat(binding.getValue(), is(equalTo(newTestValue)));
        assertThat(binding.getValueSafe(), is(equalTo(newTestValue)));
    }

}
