/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import java.util.Objects;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;

/**
 * Test boolean property binding.
 *
 * @author johns
 */
public class IntegerPropertyBindingTest {

    public IntegerPropertyBindingTest() {
    }

    @Test
    public void testIntegerBindingWithDefaultValueAndUpdate() {
        IntegerPropertyBinding binding = new IntegerPropertyBinding();
        if (!Objects.equals(binding.getValue(), null)) {
            fail("Integer property default to null fails.");
        }
        binding.setValue(Integer.MAX_VALUE);
    }

    @Test
    public void testIntegerBindingWithInitialValueAndUpdate() {
        IntegerPropertyBinding binding = new IntegerPropertyBinding(Integer.MIN_VALUE);
        if (!Objects.equals(binding.getValue(), Integer.MIN_VALUE)) {
            fail("Boolean property with initial value fails.");
        }
        binding.addListener((newValue, oldValue) -> {
            if (newValue == oldValue) {
                fail("Binding update with same value should not trigger listener");
            }
        });
        binding.setValue(Integer.MAX_VALUE);
        if (!Objects.equals(binding.getValue(), Integer.MAX_VALUE)) {
            fail("Integer property value update fails as new value.");
        }
        binding.setValue(Integer.MAX_VALUE);
    }

}
