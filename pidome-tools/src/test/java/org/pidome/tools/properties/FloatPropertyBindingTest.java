/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.properties;

import java.util.Objects;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;

/**
 * Test boolean property binding.
 *
 * @author johns
 */
public class FloatPropertyBindingTest {

    public FloatPropertyBindingTest() {
    }

    @Test
    public void testFloatBindingWithDefaultValueAndUpdate() {
        FloatPropertyBinding binding = new FloatPropertyBinding();
        if (!Objects.equals(binding.getValue(), null)) {
            fail("Float property default to null fails.");
        }
        binding.setValue(Float.MAX_VALUE);
    }

    @Test
    public void testFloatBindingWithInitialValueAndUpdate() {
        FloatPropertyBinding binding = new FloatPropertyBinding(Float.MIN_VALUE);
        if (!Objects.equals(binding.getValue(), Float.MIN_VALUE)) {
            fail("Boolean property with initial value fails.");
        }
        binding.addListener((newValue, oldValue) -> {
            if (newValue == oldValue) {
                fail("Binding update with same value should not trigger listener");
            }
        });
        binding.setValue(Float.MAX_VALUE);
        if (!Objects.equals(binding.getValue(), Float.MAX_VALUE)) {
            fail("Float property value update fails as new value.");
        }
        binding.setValue(Float.MAX_VALUE);
    }

}
