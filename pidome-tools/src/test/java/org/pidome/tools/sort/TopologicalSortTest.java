/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.sort;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author johns
 */
public class TopologicalSortTest {

    /**
     * Topo instance.
     */
    private TopologicalSort<SortTestObject> instance;

    /**
     * Object 1
     */
    private final SortTestObject first = new SortTestObject("1");

    /**
     * Object 2
     */
    private final SortTestObject second = new SortTestObject("2");

    /**
     * Object 3
     */
    private final SortTestObject third = new SortTestObject("3");

    /**
     * Object 4
     */
    private final SortTestObject fourth = new SortTestObject("4");

    /**
     * constructor.
     */
    public TopologicalSortTest() {
    }

    /**
     * Setup before each test.
     */
    @BeforeEach
    public void setUp() {
        instance = new TopologicalSort<>();
    }

    /**
     * Test of topo method, of class TopologicalSort.
     */
    @Test
    public void testTopo() throws Exception {
        /// Add unordered.
        instance.addVertex(first);
        instance.addVertex(fourth);
        instance.addVertex(third);
        instance.addVertex(second);

        /// Set dependency edges non chronological
        instance.addEdge(third, second);
        instance.addEdge(fourth, third);
        instance.addEdge(second, first);

        List<SortTestObject> expResult = new ArrayList<>() {
            {
                add(first);
                add(second);
                add(third);
                add(fourth);
            }
        };
        /// Sort dependencies
        List<SortTestObject> result = instance.sort();
        assertEquals(expResult, result);
    }

    /**
     * Test of sort method, of class TopologicalSort.
     */
    @Test
    public void testTopoInfiniteCycli() throws Exception {
        /// Add unordered.
        instance.addVertexSet(new HashSet<SortTestObject>() {
            {
                add(second);
                add(first);
            }
        });
        instance.addVertex(first);

        /// Create cycli
        /// Set dependency edges non chronological
        instance.addEdge(first, second);
        instance.addEdge(second, first);

        /// Sort.
        assertThrows(TopologicalSortCycleException.class, () -> {
            instance.sort();
        });

    }

    /**
     * Sort test object.
     */
    private static class SortTestObject {

        /**
         * the count bound to the object.
         */
        private final String count;

        /**
         * Constructor.
         *
         * @param count The count number.
         */
        private SortTestObject(String count) {
            this.count = count;
        }

        /**
         * Returns the object as string.
         *
         * @return The count text.
         */
        @Override
        public final String toString() {
            return count;
        }

    }

}
