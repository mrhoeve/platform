/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author johns
 */
public class HashUtilsTest {

    public HashUtilsTest() {
    }

    /**
     * Test of createSha256Hash method, of class HashUtils.
     */
    @Test
    public void testCreateSha256Hash() throws Exception {
        System.out.println("createSha256Hash");
        String toHash = "The Sha256 test hash must return a predicted result to comply";
        String expResult = "8579796ffa3b7a2207d2099d2ec15a5aed5c46724fae0926e243014c44a74804";
        byte[] result = HashUtils.createSha256Hash(toHash);
        StringBuilder sb = new StringBuilder();
        for (byte b : result) {
            sb.append(String.format("%02x", b));
        }
        assertEquals(sb.toString(), expResult);
    }

}
