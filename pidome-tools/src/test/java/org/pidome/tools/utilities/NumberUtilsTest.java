/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test the number utils class.
 *
 * @author johns
 */
public class NumberUtilsTest {

    public NumberUtilsTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testConstantsIntegers() {
        assertThat(0, is(equalTo(NumberUtils.INTEGER_ZERO)));
        assertThat(1, is(equalTo(NumberUtils.INTEGER_ONE)));
        assertThat(2, is(equalTo(NumberUtils.INTEGER_TWO)));
    }

    @Test
    public void testConstantsLongs() {
        assertThat(0L, is(equalTo(NumberUtils.LONG_ZERO)));
        assertThat(1L, is(equalTo(NumberUtils.LONG_ONE)));
        assertThat(2L, is(equalTo(NumberUtils.LONG_TWO)));
    }

}
