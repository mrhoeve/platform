/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import java.util.Locale;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author johns
 */
public class LocaleUtilsTest {

    /**
     * Test of getLocaleFromString method, of class LocaleUtils.
     */
    @Test
    public void testGetLocaleFromString() {
        assertEquals(Locale.US, LocaleUtils.getLocaleFromString("en_US"));
    }

    /**
     * Test of getLocaleFromString method, of class LocaleUtils.
     */
    @Test
    public void testGetLocaleFromLanguageString() {
        assertEquals(Locale.ENGLISH, LocaleUtils.getLocaleFromString("en"));
    }

    /**
     * Test of getLocaleFromString method, of class LocaleUtils.
     */
    @Test
    public void testGetLocaleFromStringWhenNull() {
        assertEquals(null, LocaleUtils.getLocaleFromString(null));
    }

    /**
     * Test of getLocaleFromString method, of class LocaleUtils.
     */
    @Test
    public void testGetDefaultLocaleFromString() {
        assertEquals(Locale.getDefault(), LocaleUtils.getLocaleFromString("default"));
    }

    /**
     * Test of getLocaleFromString method, of class LocaleUtils.
     */
    @Test
    public void testGetDefaultLocaleFromStringWithVariant() {
        assertEquals(
                new Locale.Builder()
                        .setLanguage("es")
                        .setRegion("ES")
                        .setVariant("tradnl")
                        .build(),
                LocaleUtils.getLocaleFromString("es_ES_tradnl")
        );
    }
}
