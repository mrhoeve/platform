/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.pidome.tools.enums.ColorType;

/**
 *
 * @author johns
 */
public class ColorUtilTest {

    /**
     * Test of tempToHsb method, of class ColorUtil.
     */
    @Test
    public void testTempToHsb() {
        float[] expResult = {0.6135531f, 0.35686275f, 1.0f};
        assertArrayEquals(expResult, ColorUtil.tempToHsb("26"));
    }

    /**
     * Test of tempToRgb method, of class ColorUtil.
     */
    @Test
    public void testTempToRgb() {
        float[] expResult = {164.0f, 193.0f, 255.0f};
        assertArrayEquals(expResult, ColorUtil.tempToRgb("26"));
    }

    /**
     * Test of getKelvinFromTemp method, of class ColorUtil.
     */
    @Test
    public void testGetKelvinFromTemp_String() {
        assertEquals(24975L, ColorUtil.getKelvinFromTemp("26"));
    }

    /**
     * Test of getKelvinFromTemp method, of class ColorUtil.
     */
    @Test
    public void testGetKelvinFromTemp_double() {
        assertEquals(24975L, ColorUtil.getKelvinFromTemp(26));
    }

    /**
     * Test of kelvinToRgb method, of class ColorUtil.
     */
    @Test
    public void testKelvinToRgb() {
        float[] expResult = {255.0f, 67.0f, 0.0f};
        assertArrayEquals(expResult, ColorUtil.kelvinToRgb(26));
    }

    /**
     * Test of kelvinToHsb method, of class ColorUtil.
     */
    @Test
    public void testKelvinToHsb() {
        float[] expResult = {0.043790847f, 1.0f, 1.0f};
        assertArrayEquals(expResult, ColorUtil.kelvinToHsb(26));
    }

    /**
     * Test of rgbToHsb method, of class ColorUtil.
     */
    @Test
    public void testRgbToHsb() {
        float[] expResult = {0.043790847f, 1.0f, 1.0f};
        assertArrayEquals(expResult, ColorUtil.rgbToHsb(255, 67, 0));
    }

    /**
     * Test of kelvinToColor method, of class ColorUtil.
     */
    @Test
    public void testKelvinToColorRGB() {
        float[] expResult = {164.0f, 193.0f, 255.0f};
        assertArrayEquals(expResult, ColorUtil.kelvinToColor(24975L, ColorType.RGB));
    }

    /**
     * Test of kelvinToColor method, of class ColorUtil.
     */
    @Test
    public void testKelvinToColorHSB() {
        float[] expResult = {0.6135531f, 0.35686275f, 1.0f};
        assertArrayEquals(expResult, ColorUtil.kelvinToColor(24975L, ColorType.HSB));
    }

    /**
     * Test of kelvinToColor method, of class ColorUtil.
     */
    @Test
    public void testKelvinToColorHEX() {
        float[] expResult = {164.0f, 193.0f, 255.0f};
        assertArrayEquals(expResult, ColorUtil.kelvinToColor(24975L, ColorType.HEX));
    }

    /**
     * Test of hexToRgb method, of class ColorUtil.
     */
    @Test
    public void testHexToRgb() {
        int[] expResult = {255, 255, 255};
        assertArrayEquals(expResult, ColorUtil.hexToRgb("#ffffff"));
    }

    /**
     * Test of rgbToHex method, of class ColorUtil.
     */
    @Test
    public void testRgbToHex_String() {
        assertEquals("#ffffff", ColorUtil.rgbToHex("255,255,255"));
    }

    /**
     * Test of rgbToHex method, of class ColorUtil.
     */
    @Test
    public void testRgbToHex_intArr() {
        assertEquals("#ffffff", ColorUtil.rgbToHex(255, 255, 255));
    }

    /**
     * Test of hsbToRgb method, of class ColorUtil.
     */
    @Test
    public void testHsbToRgb() {
        int[] expResult = {64, 128, 128};
        assertArrayEquals(expResult, ColorUtil.hsbToRgb(.5f, .5f, .5f));
    }

}
