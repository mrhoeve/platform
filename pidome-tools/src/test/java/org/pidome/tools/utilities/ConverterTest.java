/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author johns
 */
public class ConverterTest {

    /**
     * Test of setZeroLeading method, of class Converter.
     */
    @Test
    public void testSetZeroLeading() {
        String number = "245";
        int lengthWithLead = 5;
        String expResult = "00245";
        String result = Converter.setZeroLeading(number, lengthWithLead);
        assertEquals(expResult, result);
    }

    /**
     * Test of byteArrayToFloat method, of class Converter.
     */
    @Test
    public void testByteArrayToFloat() {
        byte[] b = {63, -116, -52, -51};
        assertEquals(1.1f, Converter.byteArrayToFloat(b), 0.0);
    }

    /**
     * Test of byteArrayToDouble method, of class Converter.
     */
    @Test
    public void testByteArrayToDouble() {
        byte[] b = {64, -124, 12, 0, 0, 0, 0, 0};
        assertEquals(641.5, Converter.byteArrayToDouble(b), 0.0);
    }

    /**
     * Test of byteArrayToHexString method, of class Converter.
     */
    @Test
    public void testByteArrayToHexString() {
        byte[] array = {0, -116, -52, -51};
        assertEquals("008CCCCD", Converter.byteArrayToHexString(array));
    }

}
