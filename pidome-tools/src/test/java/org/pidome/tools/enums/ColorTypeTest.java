/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.enums;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Color types.
 *
 * @author johns
 */
@DisplayName("Test color types availability")
public class ColorTypeTest {

    /**
     * Test of values method, of class ColorType.
     */
    @Test
    @DisplayName("Test available")
    public void testValues() {
        ColorType[] expResult = {ColorType.RGB, ColorType.HSB, ColorType.HEX};
        ColorType[] result = ColorType.values();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class ColorType.
     */
    @Test
    @DisplayName("Test discovry by string")
    public void testValueOf() {
        ColorType expResult = ColorType.RGB;
        ColorType result = ColorType.valueOf("RGB");
        assertEquals(expResult, result);
    }

}
