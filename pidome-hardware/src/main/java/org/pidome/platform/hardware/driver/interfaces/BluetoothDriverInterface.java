/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.interfaces;

import org.pidome.platform.hardware.driver.config.DummyConfigInterface;
import org.pidome.platform.hardware.driver.config.HardwareConfiguration;

/**
 * The interface to use for bluetooth.
 *
 * @author John Sirach
 */
public interface BluetoothDriverInterface extends HardwareDriverInterface, HardwareConfiguration<DummyConfigInterface> {

}
