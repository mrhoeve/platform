/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver;

import io.vertx.core.Future;
import org.pidome.platform.presentation.input.InputForm;

/**
 * The base for the hardware driver.
 *
 * <p>
 * This is a live object. This means that you are capable of keeping state of
 * your implementation. Use the provided <code>Future</code>'s in the options,
 * start and stop methods.
 * <p>
 * the <code>Future</code> implementation used is not the default java built in,
 * it is provided by the Eclipse Vert.X project. Please check https://vertx.io/
 * for more information.
 *
 * @author John Sirach
 */
public abstract class HardwareDriver {

    /**
     * The configuration presentation of the driver.
     */
    private final InputForm configuration = new InputForm("Configuration");

    /**
     * Returns the configuration form of this driver.
     *
     * This method returns the form used to configure this driver. When you want
     * to use the configuration you need to create your form in the constructor
     * of your driver.
     *
     * To create the configuration you use
     * {@link org.pidome.platform.presentation.input.InputSection} for each
     * configuration section. Inside these sections you place
     * {@link org.pidome.platform.presentation.input.InputField} which will be
     * shown to the end user. Refer to the pidome-presentation package for more
     * information on how to create a configuration shown to the end user.
     * <p>
     * To set a configuration call <code>getconfiguration().addSection();</code>
     * or <code>getConfiguration().addSections</code> when you have multiple
     * sections.
     * <p>
     * The configuration is live and when {@link #setOpts} is called you can get
     * the configuration with <code>getConfiguration()</code>. Within the
     * sections your input fields will contain the selected options the end user
     * has chosen to configure the driver. Refer to
     * {@link org.pidome.platform.presentation.input.InputField} on how to get
     * the value.
     *
     * @return the configuration object.
     * @see org.pidome.platform.presentation.input.InputForm
     */
    public final InputForm getConfiguration() {
        return configuration;
    }

    /**
     * Called when the user provides the configuration.
     *
     * This method is called when a user has entered the configuration needed,
     * and if a configuration has been created.
     *
     * @param future The future to notify PiDome the configuration has been
     * completed.
     */
    public abstract void setOpts(Future<Void> future);

    /**
     * Starts the driver implementation.
     *
     * This method is called after <code>setOpts()</code> when the future has
     * been completed.
     *
     * The future is used as an indicator. It does not prevend from
     * <code>stopDriver()</code> being called.
     *
     * @param future The future to complete or fail when a driver wants to start
     * or not.
     */
    public abstract void startDriver(Future<Void> future);

    /**
     * Stops the driver implementation.
     *
     * @param future The future to let know if the driver is successfully
     * stopped.
     */
    public abstract void stopDriver(Future<Void> future);

}
