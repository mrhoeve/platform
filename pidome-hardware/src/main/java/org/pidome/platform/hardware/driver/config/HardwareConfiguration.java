/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.config;

import io.vertx.core.Future;

/**
 * Interface required to be able to supply a hardware configuration to an
 * implementation.
 *
 * @author John Sirach
 * @param <T> The configuration object type.
 */
public interface HardwareConfiguration<T> {

    /**
     * Supplies the configuration of a peripheral attached to the system.
     *
     * @param configuration The configuration.
     * @param future The future to let the supplier know configurtion has been
     * applied/created.
     */
    void configure(T configuration, Future<Void> future);

}
