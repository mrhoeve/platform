/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.interfaces;

import org.pidome.platform.hardware.driver.config.HardwareConfiguration;
import org.pidome.platform.hardware.driver.config.SerialDeviceConfiguration;

/**
 * The interface for hardware drivers to implement.
 *
 * @author John Sirach
 */
public interface SerialDriverInterface extends HardwareDriverInterface, HardwareConfiguration<SerialDeviceConfiguration> {

}
