/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.config;

/**
 * A configuration object which will be supplied to a serial driver.
 *
 * @author John Sirach
 */
public class SerialDeviceConfiguration {

    /**
     * The port to a serial interface.
     *
     * A port is always supplied, and depending on the serial device available
     * to connect or not.
     *
     */
    private String port;

    /**
     * The path to a serial interface.
     *
     * A path is a best effort method and based on the os maybe available to
     * connect to.
     */
    private String path;

    /**
     * The name of the peripheral attached, if available.
     */
    private String name;

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param devicePort the port to set
     */
    public void setPort(final String devicePort) {
        this.port = devicePort;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param devicePath the path to set
     */
    public void setPath(final String devicePath) {
        this.path = devicePath;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param deviceName the name to set
     */
    public void setName(final String deviceName) {
        this.name = deviceName;
    }

}
