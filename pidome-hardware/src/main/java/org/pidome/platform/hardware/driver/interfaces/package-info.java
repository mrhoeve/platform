/**
 * Hardware driver interfaces.
 * <p>
 * Provides supported hardware interfaces to develop against.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.hardware.driver.interfaces;
