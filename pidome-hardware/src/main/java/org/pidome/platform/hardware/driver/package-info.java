/**
 * Hardware driver.
 * <p>
 * Provides abstract and interface classes to implement hardware drivers.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.hardware.driver;
