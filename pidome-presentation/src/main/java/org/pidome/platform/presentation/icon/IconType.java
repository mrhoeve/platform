/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.icon;

/**
 * Supported icon types.
 *
 * @author John Sirach
 */
public enum IconType {
    /**
     * Font awesome icon set.
     *
     * Version 5 is supported, set available is:
     * https://fontawesome.com/icons?d=gallery&amp;m=free
     */
    FONT_AWESOME("fa"),
    /**
     * Material icon set.
     *
     * Set supported is: https://material.io/tools/icons/?style=baseline
     */
    MATERIAL("mat");

    /**
     * The icon type identifier.
     */
    private final String identifier;

    /**
     * Type constructor.
     *
     * @param iconIdentifier The identifier for the type.
     */
    IconType(final String iconIdentifier) {
        this.identifier = iconIdentifier;
    }

    /**
     * Retuns the identifier for the type.
     *
     * @return The icon identifier.
     */
    public final String getIdentifier() {
        return this.identifier;
    }

}
