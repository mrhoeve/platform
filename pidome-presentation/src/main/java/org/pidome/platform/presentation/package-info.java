/**
 * Root package for presentations.
 * <p>
 * Presentations provide a programmatic way of providing an end user inputs and
 * visual elements.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.presentation;
