/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

/**
 * Exception thrown when there is an invalid value set by the end user.
 *
 * @author John Sirach
 */
public final class InputValueException extends Exception {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>WebInputInvalidValueException</code>
     * without detail message.
     */
    public InputValueException() {
    }

    /**
     * Constructs an instance of <code>WebInputInvalidValueException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public InputValueException(final String msg) {
        super(msg);
    }
}
