/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Interface to be used by Enums which are being used by presentations.
 *
 * @author John Sirach
 * @param <T> The type the enum is.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public interface InputEnum<T extends Object> {

    /**
     * Returns the label of the enum.
     *
     * @return The label used for to display the enum.
     */
    @JsonProperty("label")
    String getLabel();

    /**
     * Returns the value belonging to the enum.
     *
     * @return The value
     */
    @JsonProperty("value")
    T getValue();

    /**
     * Returns the enum name as described by the <code>Enum</code> type.
     *
     * @return The enum name.
     */
    @JsonProperty("name")
    String name();

}
