/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A set of sections.
 *
 * @author John Sirach
 */
public final class InputForm {

    /**
     * List of sections in the form.
     */
    private List<InputSection> sectionList = new ArrayList<>();

    /**
     * The name of the form.
     */
    private final String name;

    /**
     * The description of the form.
     */
    private final String description;

    /**
     * Constructor of a web form.
     */
    public InputForm() {
        this(null, null);
    }

    /**
     * Constructor of a web form.
     *
     * @param formName The name of the form.
     */
    public InputForm(final String formName) {
        this(formName, null);
    }

    /**
     * Constructor of a web form.
     *
     * @param formName The name of the form.
     * @param formDescription The description of the form.
     */
    public InputForm(final String formName, final String formDescription) {
        this.name = formName;
        this.description = formDescription;
    }

    /**
     * @return the sectionList
     */
    public List<InputSection> getSectionList() {
        return sectionList;
    }

    /**
     * @param sections the sectionList to set
     */
    public void setSectionList(final List<InputSection> sections) {
        this.sectionList = sections;
    }

    /**
     * @param section the sectionList to set.
     * @return This object.
     */
    public InputForm addSection(final InputSection section) {
        this.sectionList.add(section);
        return this;
    }

    /**
     * @param sections the sectionList to set.
     * @return This object.
     */
    public InputForm addSections(final List<InputSection> sections) {
        this.sectionList.addAll(sections);
        return this;
    }

    /**
     * @param sections the sectionList to set.
     * @return This object.
     */
    public InputForm addSections(final InputSection... sections) {
        return addSections(Arrays.asList(sections));
    }

    /**
     * @param section the sectionList to set.
     * @return This object.
     */
    public InputForm removeSection(final InputSection section) {
        this.sectionList.remove(section);
        return this;
    }

    /**
     * @param sections the sectionList to set.
     * @return This object.
     */
    public InputForm removeSections(final List<InputSection> sections) {
        this.sectionList.removeAll(sections);
        return this;
    }

    /**
     * @param sections the sectionList to set.
     * @return This object.
     */
    public InputForm removeSections(final InputSection... sections) {
        return removeSections(Arrays.asList(sections));
    }

    /**
     * @return the name of the form.
     */
    public String getName() {
        return name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
}
