/**
 * Input fields for displaying on the web interface.
 * <p>
 * Provides web input fields which an end user is presented and will be returned
 * with the given end user input.
 *
 * It is possible to extend each input field type and write your own validation
 * method where needed.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.presentation.input.fields;
