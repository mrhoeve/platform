/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import org.pidome.platform.presentation.input.InputValueException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.presentation.input.InputField;

/**
 * Single line text input field.
 *
 * @author John Sirach
 */
public final class StringSingleLineInput extends InputField<String> {

    /**
     * Constructor setting id, name and description.
     *
     * @param id The id of the field.
     * @param label The label of the field.
     * @param description The description of the field.
     */
    @JsonCreator
    public StringSingleLineInput(@JsonProperty("id") final String id, @JsonProperty("label") final String label, @JsonProperty("description") final String description) {
        super(id, label, description, InputFieldType.STRING);
    }

    /**
     * Validates the input of the given text.
     *
     * This method only checks if there are newlines present.
     *
     * @throws InputValueException When validation fails.
     */
    @Override
    public void validateInput() throws InputValueException {
        if (this.getValue().contains("\n")) {
            throw new InputValueException(this.getLabel() + " can not contain multiple lines.");
        }
    }

}
