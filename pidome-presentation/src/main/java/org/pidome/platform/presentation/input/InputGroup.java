/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import java.util.ArrayList;
import java.util.List;

/**
 * A group is used to group presentation sections together.
 *
 * @author John Sirach
 */
public class InputGroup {

    /**
     * Section in this group.
     */
    private List<InputSection> sections = new ArrayList<>();

    /**
     * @return the sections
     */
    public final List<InputSection> getSections() {
        return sections;
    }

    /**
     * @param presentationSection the sections to set
     */
    public final void setSections(final List<InputSection> presentationSection) {
        this.sections = presentationSection;
    }

}
