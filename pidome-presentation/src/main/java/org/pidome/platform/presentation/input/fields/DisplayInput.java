/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import org.pidome.platform.presentation.input.InputValueException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.presentation.input.InputField;

/**
 * A display input is an unmodifiable text field.
 *
 * Use this "input" to provide the user with information which is unmodifiable.
 * it consists of a label and a text.
 *
 * @author John Sirach
 */
public final class DisplayInput extends InputField<String> {

    /**
     * The content to show.
     */
    private String content;

    /**
     * Constructor.
     *
     * @param fieldId The field id.
     * @param fieldLabel The field label.
     */
    @JsonCreator
    public DisplayInput(@JsonProperty("id") final String fieldId, @JsonProperty("label") final String fieldLabel) {
        super(fieldId, fieldLabel, null, InputFieldType.TEXT);
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param textContent the content to set
     */
    public void setContent(final String textContent) {
        this.content = textContent;
    }

    @Override
    public void validateInput() throws InputValueException {
        /// Not available on read only items.
    }

}
