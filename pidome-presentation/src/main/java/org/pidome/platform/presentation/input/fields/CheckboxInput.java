/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.presentation.input.InputFieldMultiValues;

/**
 * A list of checkboxes implementing the WebInputField types for displaying.
 *
 * The select list only accepts unique objects. If you want to add same values
 * you must initialize a new object to add. Use the
 * {@link CheckboxInput#setListValues setListValues} and
 * {@link CheckboxInput#addItem addItem} methods to fill the list for presenting
 * to the user.
 *
 * @author John Sirach
 * @param <T> The type to use for the list of checkboxes. Must be a
 * <code>WebInputFieldType</code> type.
 */
public final class CheckboxInput<T extends InputFieldMultiValues> extends InputFieldMultiValues<T> {

    /**
     * Constructor setting id, name and description.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label of the field.
     * @param fieldDescription The description of the field.
     */
    @JsonCreator
    public CheckboxInput(@JsonProperty("id") final String fieldId, @JsonProperty("label") final String fieldLabel, @JsonProperty("description") final String fieldDescription) {
        super(fieldId, fieldLabel, fieldDescription, InputFieldType.CHECKBOX);
    }

}
