/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import org.pidome.platform.presentation.input.InputValueException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.presentation.input.InputField;

/**
 * Password text input field.
 *
 * @author John Sirach
 */
public final class PasswordInput extends InputField<String> {

    /**
     * Constructor setting id, name and description.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label of the field.
     * @param fieldDescription The description of the field.
     */
    @JsonCreator
    public PasswordInput(@JsonProperty("id") final String fieldId,
            @JsonProperty("label") final String fieldLabel,
            @JsonProperty("description") final String fieldDescription) {
        super(fieldId, fieldLabel, fieldDescription, InputFieldType.PASSWORD);
    }

    @Override
    public void validateInput() throws InputValueException {
    }

}
