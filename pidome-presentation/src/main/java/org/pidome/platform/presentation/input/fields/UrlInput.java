/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import org.pidome.platform.presentation.input.InputValueException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.net.URL;
import org.pidome.platform.presentation.input.InputField;

/**
 * An input field expecting an URL.
 *
 * URL as described as {@link java.net.URL java.net.URL}
 *
 * @author John Sirach
 */
public final class UrlInput extends InputField<URL> {

    /**
     * Constructor setting id, name and description.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label of the field.
     * @param fieldDescription The description of the field.
     */
    @JsonCreator
    public UrlInput(@JsonProperty("id") final String fieldId,
            @JsonProperty("label") final String fieldLabel,
            @JsonProperty("description") final String fieldDescription) {
        super(fieldId, fieldLabel, fieldDescription, InputFieldType.URL);
    }

    @Override
    public void validateInput() throws InputValueException {
    }

}
