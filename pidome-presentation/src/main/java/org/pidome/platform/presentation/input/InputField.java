/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.pidome.platform.presentation.input.fields.BooleanInput;
import org.pidome.platform.presentation.input.fields.CheckboxEnumInput;
import org.pidome.platform.presentation.input.fields.CheckboxInput;
import org.pidome.platform.presentation.input.fields.DisplayInput;
import org.pidome.platform.presentation.input.fields.DoubleInput;
import org.pidome.platform.presentation.input.fields.IntegerInput;
import org.pidome.platform.presentation.input.fields.IpAddressInput;
import org.pidome.platform.presentation.input.fields.PasswordInput;
import org.pidome.platform.presentation.input.fields.SelectEnumInput;
import org.pidome.platform.presentation.input.fields.SelectInput;
import org.pidome.platform.presentation.input.fields.StringMultiLineInput;
import org.pidome.platform.presentation.input.fields.StringSingleLineInput;
import org.pidome.platform.presentation.input.fields.UrlInput;

/**
 * Web component inputs fields.
 *
 * @param <T> The type implemented for the input field.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({
    @Type(value = BooleanInput.class),
    @Type(value = CheckboxEnumInput.class),
    @Type(value = CheckboxInput.class),
    @Type(value = DisplayInput.class),
    @Type(value = DoubleInput.class),
    @Type(value = IntegerInput.class),
    @Type(value = IpAddressInput.class),
    @Type(value = PasswordInput.class),
    @Type(value = SelectEnumInput.class),
    @Type(value = SelectInput.class),
    @Type(value = StringMultiLineInput.class),
    @Type(value = StringSingleLineInput.class),
    @Type(value = UrlInput.class)
})
public abstract class InputField<T> implements InputElement<T> {

    /**
     * The id of the input field.
     */
    private final String id;
    /**
     * The label of the input field.
     */
    private final String label;
    /**
     * The description of the input field.
     */
    private final String description;
    /**
     * The input field type.
     */
    private final InputElement.InputFieldType inputFieldType;
    /**
     * If the input is required to return with a value or not.
     */
    private boolean required = false;
    /**
     * The default value of the input field.
     */
    private T defaultValue;
    /**
     * The value entered by the user.
     */
    private T value;

    /**
     * Constructor setting id, name and description.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label of the field.
     * @param fieldDescription The description of the field.
     * @param fieldInputType The input type of the field.
     */
    protected InputField(final String fieldId, final String fieldLabel, final String fieldDescription, final InputElement.InputFieldType fieldInputType) {
        this.id = fieldId;
        this.label = fieldLabel;
        this.description = fieldDescription;
        this.inputFieldType = fieldInputType;
    }

    /**
     * Gets the input id.
     *
     * @return The id of the input.
     */
    @Override
    public final String getId() {
        return this.id;
    }

    /**
     * Gets the input label.
     *
     * @return The label of the input.
     */
    @Override
    public final String getLabel() {
        return this.label;
    }

    /**
     * Get the input description.
     *
     * @return The description of the input.
     */
    @Override
    public final String getDescription() {
        return this.description;
    }

    /**
     * @return the inputFieldType
     */
    @Override
    public final InputElement.InputFieldType getInputFieldType() {
        return inputFieldType;
    }

    /**
     * Set's an options default value.
     *
     * @param asDefault The defaultValue to set.
     */
    @Override
    public final void setDefaultValue(final T asDefault) {
        this.defaultValue = asDefault;
    }

    /**
     * Returns the default value. Defaults to empty string.
     *
     * @return The default value set by the user.
     */
    @Override
    public final T getDefaultValue() {
        return this.defaultValue;
    }

    /**
     * Sets the input value.
     *
     * @param userValue The value entered by the user.
     */
    @Override
    public final void setValue(final T userValue) {
        this.value = userValue;
    }

    /**
     * Returns the option value.
     *
     * @return The value set by the user.
     */
    @Override
    public final T getValue() {
        return this.value;
    }

    /**
     * Checks of the requirements are met.
     *
     * @return true when the requirements are met.
     * @throws InputValueException Throw when the requirements are not
     * met.
     */
    @Override
    public final boolean checkRequirements() throws InputValueException {
        if (this.isRequired() && this.getValue() == null) {
            throw new InputValueException("Missing value");
        }
        this.validateInput();
        return true;
    }

    /**
     * Returns if the value of the input is required.
     *
     * @return If required or not.
     */
    @Override
    public final boolean isRequired() {
        return required;
    }

    /**
     * Set the input field required or not.
     *
     * By default the value is false meaning not required.
     *
     * @param setRequired Set to true to set this field required.
     */
    @Override
    public final void setRequired(final boolean setRequired) {
        this.required = setRequired;
    }

    /**
     * Validates the end user input.
     *
     * This only checks for input validity, not value validity. This method
     * throws the <code>WebInputInvalidValueException</code> when the input is
     * invalid and before it reaches any code requesting the input value
     *
     * @throws InputValueException When the input is invalid.
     */
    @Override
    public abstract void validateInput() throws InputValueException;

}
