/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A section of controls.
 *
 * @author John Sirach
 */
public class InputSection {

    /**
     * The name of the section.
     */
    private final String name;

    /**
     * The description of the section.
     */
    private final String description;

    /**
     * The list of the section elements.
     */
    private List<InputField> elements = new ArrayList<>();

    /**
     * Constructor.
     */
    public InputSection() {
        this(null, null);
    }

    /**
     * Constructor.
     *
     * @param sectionName The name of the section.
     */
    public InputSection(final String sectionName) {
        this(sectionName, null);
    }

    /**
     * Constructor.
     *
     * @param sectionName The name of the section.
     * @param sectionDescription The description of the section.
     */
    public InputSection(final String sectionName, final String sectionDescription) {
        this.name = sectionName;
        this.description = sectionDescription;
    }

    /**
     * Returns the name of the section.
     *
     * @return Section name.
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Returns the description.
     *
     * @return the description.
     */
    public final String getDescription() {
        return this.description;
    }

    /**
     * @return the elements
     */
    public final List<InputField> getElements() {
        return elements;
    }

    /**
     * @param selements the elements to set
     */
    public final void setElements(final List<InputField> selements) {
        this.elements = selements;
    }

    /**
     * @param selements the elements to set
     */
    public final void setElements(final InputField... selements) {
        this.elements = Arrays.asList(selements);
    }

    /**
     * @param selements the elements to add
     */
    public final void addElements(final List<InputField> selements) {
        this.elements.addAll(selements);
    }

    /**
     * @param selements the elements to add
     */
    public final void addElements(final InputField selements) {
        this.elements.addAll(Arrays.asList(selements));
    }

}
