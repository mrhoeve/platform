/**
 * Package to identify and use icon sets between different presenters.
 * <p>
 * Icons delivered from this package provide a coherent identifier and methods
 * for presenters to present icons from icon sets.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.presentation.icon;
