/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation;

/**
 * Enum describing the presentation targets.
 *
 * @author John Sirach
 */
public enum PresentationPlatform {

    /**
     * Generic desktop.
     */
    DESKTOP("desktop"),
    /**
     * Mobile device.
     */
    MOBILE("mobile"),
    /**
     * A tablet.
     */
    TABLET("tablet"),
    /**
     * Like a magic mirror.
     */
    MIRROR("mirror"),
    /**
     * Like an home automation controller.
     */
    CONTROLLER("controller"),
    /**
     * A gaming console.
     */
    GAMING("gaming"),
    /**
     * A watch.
     */
    WATCH("watch"),
    /**
     * A television.
     */
    TELEVISION("television"),
    /**
     * Generic monitor.
     *
     * This will be used as a default if no specific can be identified.
     */
    MONITOR("monitor"),
    /**
     * A generic appliance (mostly household).
     */
    APPLIANCE("appliance"),
    /**
     * A car interface.
     */
    CAR("car");

    /**
     * String representation of the identifier.
     */
    private final String identifier;

    /**
     * Presentation target constructor.
     *
     * @param id The string representation of the identifier.
     */
    PresentationPlatform(final String id) {
        this.identifier = id;
    }

    /**
     * Returns the string representation.
     *
     * @return The string representation.
     */
    public final String getIdentifier() {
        return this.identifier;
    }

    /**
     * Returns the enum from the given representation.
     *
     * @param id The string version.
     * @return The representation.
     */
    public final PresentationPlatform fromIdentifier(final String id) {
        for (PresentationPlatform platform : PresentationPlatform.values()) {
            if (platform.getIdentifier().equals(id)) {
                return platform;
            }
        }
        return MONITOR;
    }

}
