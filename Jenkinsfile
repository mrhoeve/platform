pipeline {
    agent any
    tools {
        gradle 'Gradle-5.5'
        jdk "JDK-11"
    }
    environment {
        JAVA_HOME = "${jdk}"
    }
    stages {
        stage('Initialize') {
            steps {
                script{
                    GRADLE_BUILD_DEFAULT='gradle --configure-on-demand -PBRANCH_NAME=${BRANCH_NAME} -PENV_VENDOR=${ENV_VENDOR} -PENV_BUILD_NUMBER=${BUILD_NUMBER}'
                }
                sh 'echo "JDK path = $JAVA_HOME"'
                sh 'gradle --configure-on-demand clean'
            }
        }
        stage('Code Analysis') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} pidome-tools:fullAnalysisAndCheck"
                sh "${GRADLE_BUILD_DEFAULT} pidome-presentation:fullAnalysisAndCheck"
                sh "${GRADLE_BUILD_DEFAULT} pidome-hardware:fullAnalysisAndCheck"
                sh "${GRADLE_BUILD_DEFAULT} pidome-server:fullAnalysisAndCheck"
            }
        }
        stage('Build dependencies') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} -x check -x javadoc :pidome-hardware:build :pidome-presentation:build :pidome-tools:build"
                archiveArtifacts artifacts: '*/build/libs/**/*.jar', fingerprint: true, onlyIfSuccessful: true
            }
        }
        stage('Publish dependencies and BOM'){
            steps {
                withCredentials([usernamePassword(credentialsId: 'nexus_publish', passwordVariable: 'archpass', usernameVariable: 'archuser')]) {
                    sh "${GRADLE_BUILD_DEFAULT} -Parchuser=$archuser -Parchpass=$archpass :pidome-hardware:uploadArchives :pidome-presentation:uploadArchives :pidome-tools:uploadArchives"
                    sh "${GRADLE_BUILD_DEFAULT} -Parchuser=$archuser -Parchpass=$archpass :pidome-development-bom:publish"
                }
            }
        }
        stage('Build PiDome') {
            steps {
                dir('pidome-server'){ 
                    sh "${GRADLE_BUILD_DEFAULT} -x check -x javadoc distZip distTar"
                    archiveArtifacts artifacts: 'build/libs/**/*.jar, build/distributions/**/*.zip, build/distributions/**/*.tar', fingerprint: true, onlyIfSuccessful: true
                }
            }
        }
        stage('Publish PiDome') {
            when { 
                anyOf { 
                    branch 'master'; branch 'development' 
                } 
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'bitbucket-cloud', passwordVariable: 'scmpass', usernameVariable: 'scmuser')]) {
                    dir('pidome-server'){ 
                        script {
                            SERVER_BUILD_VERSION = sh (
                                script: 'gradle --configure-on-demand -q properties | grep "version:" | awk \'{print $2}\'',
                                returnStdout: true
                            ).trim()
                        }
                        sh "echo Publishing version: ${SERVER_BUILD_VERSION}"
                        sh "curl --fail-early -u ${scmuser}:${scmpass} -X POST https://api.bitbucket.org/2.0/repositories/pidome/platform/downloads -F files=@build/distributions/pidome-server-${SERVER_BUILD_VERSION}.zip"
                        sh "curl --fail-early -u ${scmuser}:${scmpass} -X POST https://api.bitbucket.org/2.0/repositories/pidome/platform/downloads -F files=@build/distributions/pidome-server-${SERVER_BUILD_VERSION}.tar"
                    }
                }
            }
        }
        stage('Create documentation') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} :pidome-hardware:javadoc :pidome-presentation:javadoc :pidome-tools:javadoc :pidome-server:javadoc :pidome-server:enunciate"
            }
        }
        stage('Publish documentation'){
            when { 
                anyOf { 
                    branch 'master'; branch 'development'
                } 
            }
            steps {
                configFileProvider([configFile(fileId: '0c74ea6f-c956-4950-af04-835b2a8ca28a', variable: 'FTP_SETTINGS')]) {
                    sh "${GRADLE_BUILD_DEFAULT} -PDOCS_PUB_FILE=${FTP_SETTINGS} publicizeDocs"
                }
            }
        }
    }
    post { 
        always {
            jacoco(execPattern: '**/*.exec')
            junit '**/test-results/test/*.xml'
            recordIssues enabledForFailure: true, tools: [java(), javaDoc()]
            recordIssues enabledForFailure: true, tool: cpd(pattern: 'build/analysis/**/cpd/main.xml')
            recordIssues enabledForFailure: true, tool: checkStyle(pattern: 'build/analysis/**/checkstyle/main.xml')
            recordIssues enabledForFailure: true, tool: spotBugs(pattern: 'build/analysis/**/spotbugs/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: pmdParser(pattern: 'build/analysis/**/pmd/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: taskScanner(includePattern:'**/*.java', excludePattern:'target/**/*', highTags:'FIXME', normalTags:'TODO,@todo', lowTags:'@deprecated'), qualityGates: [[threshold: 1, type: 'TOTAL_HIGH', unstable: true]]
        }
        cleanup { 
            cleanWs() 
        }
    }
}